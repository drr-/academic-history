package dreyesmod1;

import java.awt.FileDialog;
import java.awt.Frame;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.NoSuchElementException;
import java.util.Scanner;

public class Array2D {
	public static Scanner scan = new Scanner(System.in);
	public static Scanner data = null;
	
	public static void openFile() {
		try {
			File file = new File(pickLocRead());
			data = new Scanner(file);
			
			//data.useDelimiter(" - ");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	public static String pickLocRead() {
		Frame f = new Frame();
		// decide from where to read the file
		FileDialog fiBox = new FileDialog(f, "Pick location for reading your file", FileDialog.LOAD);
		System.out.println("The dialog box might appear behind Eclipse.  " + 
			      "\n   Choose where you would like to read for your data.");
		fiBox.setVisible(true);
		// get the absolute path to the file
		String fiName = fiBox.getFile();
		String dirPath = fiBox.getDirectory();

		// create a file instance for the absolute path
		String name = dirPath + fiName;
		return name;
	}
	
	
	
	
	private static void highValues(int[][] x) {
		int TLa = -99999;
		
		
		System.out.println("The highest values in each row: ");
		
		for (int i = 0; i <= x.length-1; i++) {
			int CL = -99999;
			
			for (int g = 0; g <= x[i].length-1; g++) {
				//System.out.print(x[i][g]);
				if (x[i][g] > CL) {
					CL = x[i][g];
				}
			}
			
			if (CL > TLa) {
				TLa = CL;
			}
			
			System.out.println("\tRow " + i + ":" + CL);
		}
		
		System.out.println("The highest value in the array is " + TLa);
	}

	private static int[][] readFile(int[][] z) {
		openFile();
    	
    	int[][] tempArray = z;
    	
    	int count = 0;
    	try {
    		
			while (data.hasNextLine()) {
				
				String[] tempArr = null;
				
				tempArr = data.nextLine().split("\\|");
				
				for (int i = 0; i <= tempArr.length-1; i++) {
					tempArray[count][i] = Integer.parseInt(tempArr[i]);
					//System.out.print(tempArr[i] + "-");
				}
				
				//System.out.println("\n");
				
				count += 1;
			}
			
			
			
		} catch (NoSuchElementException elementException) {
			System.err.println("File improperly formed. Terminating.");
		} catch (IllegalStateException stateException) {
			System.err.println("Error reading from file. Terminating.");
		}
    	
        return tempArray;
	}
	
	
	
	public static void main(String[] args) {
		System.out.println("How many rows?");
		int numRows = scan.nextInt();
		
		System.out.println("How many columns?");
		int numCols = scan.nextInt();
		
		int[][] values = new int[numRows][numCols];
		
		values = readFile(values);		
		highValues(values);
		System.exit(0);
	}


}
