
package dreyesmod1;
import java.text.DecimalFormat;
import java.text.NumberFormat;

public class Candy extends Dessert {
    
    //fields should be weight and pricePerPound;
	private double weight;
	private double pricePerPound;
	
	//no argument and full constructors
	
	public Candy() {
		super();
		
	}



	public Candy(String n, double x, double y) {
		super(n);
		weight = x;
		pricePerPound = y;
	}
    
	
    //toString method
    public String toString(){
        NumberFormat nf = NumberFormat.getCurrencyInstance();
        DecimalFormat df = new DecimalFormat("#.00");
        String ans = super.toString();
        return ans + " -- " + weight + "lbs " + " @ " + " $" + df.format(pricePerPound) 
                   + "  Cost:" + " $" + df.format(getCost());
    }
    
    

	public double getCost() {
        return weight * pricePerPound/12;
    }
    
	public double getWeight() {
		return weight;
	}

	public void setWeight(double weight) {
		this.weight = weight;
	}

	public double getPricePerPound() {
		return pricePerPound;
	}

	public void setPricePerPound(double pricePerPound) {
		this.pricePerPound = pricePerPound;
	}
    
    
    
    
    
    
    
    
}//end class
