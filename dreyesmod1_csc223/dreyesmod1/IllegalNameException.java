package dreyesmod1;

public class IllegalNameException extends Exception {
	
	public IllegalNameException() {
		super();
	}
	
	public IllegalNameException(String e) {
		super(e);
	}
	
}
