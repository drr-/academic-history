
package dreyesmod1;

import java.text.NumberFormat;

public class Employee {
    
	// very rich people so we are using int for salaries
    private String name;
    private int salary;

    public Employee() {
    }

    public Employee(String name, int salary) {
        this.name = name;
        this.salary = salary;
    }

    @Override
    public String toString() {
    	NumberFormat nf = NumberFormat.getCurrencyInstance();
        return name + " has a salary of " + nf.format(salary);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }
    
    
    
}
