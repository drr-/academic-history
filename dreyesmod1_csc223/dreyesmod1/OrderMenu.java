package dreyesmod1;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;





public class OrderMenu {
	
	private Scanner scan = new Scanner (System.in);
	
	public  void printCustomers(ArrayList<Customer> cust) {
		for (Customer c : cust)
			System.out.println(c.toString());
	}
	
	public void loadCustomers(ArrayList<Customer> c) {
		// create some fake customers (used for testing the program)
				c.add(new Customer("Duck", "Donald"));
				c.add(new Customer("Mouse", "Minnie"));
				c.add(new Customer("Mouse", "Mickey"));
				c.add(new Customer("Brown", "Charlie"));
	}
	
	 public  void loadDesserts(ArrayList<Dessert> d) {
	    	d.add(new Candy("Peanut Butter Fudge", 2.25, 3.99));
	    	d.add(new IceCream("Vanilla Ice Cream", 1.05, 3));
	    	d.add(new Sundae("Choc. Chip Ice Cream", 1.45, 3, "Hot Fudge", .50));        
	        d.add(new IceCream("Strawberry Ice Cream", 1.45, 2));
	        d.add(new Sundae("Vanilla Ice Cream", 1.05, 3, "Caramel", .50));
	        d.add(new Candy("Gummy Worms", 1.33, .89));
	        d.add(new Cookie("Chocolate Chip Cookies", 4, 3.99));
	        d.add(new Candy("Salt Water Taffy", 1.5, 2.09));
	        d.add(new Candy("Candy Corn", 3.0, 1.09));
	        d.add(new Cookie("Oatmeal Raisin Cookies", 4, 3.99));	        
	        d.add(new Cake ("Chocolate Cake", 2, 3.99));
	        d.add(new Cake("Carrot Cake", 1, 4.99));      
	       
	    }
	    
	    public void printDesserts(ArrayList<Dessert> d) {
	    	for (int i=0; i < d.size(); i++)
	    		System.out.println(d.get(i).toString());
	    }
	    
	    public void printClass(ArrayList<Dessert> d) {
	    	System.out.println("Which class of dessert are you looking for?"
	    			+ "\n - 1. Cake"
	    			+ "\n - 2. Candy"
	    			+ "\n - 3. Cookie"
	    			+ "\n - 4. Ice Cream"
	    			+ "\n - 5. Sundae");
	    	
	    	int eChoice = 0;
	    	eChoice = scan.nextInt();
	    	
	    	
	    	for (int i=0; i < d.size(); i++) {
	    		//System.out.println(d.get(i).getClass().getSimpleName());
	    		
	    		if (d.get(i).getClass().getSimpleName().equals("Cake") && eChoice == 1)
	    			System.out.println(d.get(i).toString());

	    		if (d.get(i).getClass().getSimpleName().equals("Candy") && eChoice == 2)
	    			System.out.println(d.get(i).toString());
	    		
	    		if (d.get(i).getClass().getSimpleName().equals("Cookie") && eChoice == 3)
	    			System.out.println(d.get(i).toString());
	    		
	    		if (d.get(i).getClass().getSimpleName().equals("IceCream") && eChoice == 4)
	    			System.out.println(d.get(i).toString());
	    		
	    		if (d.get(i).getClass().getSimpleName().equals("Sundae") && eChoice == 5)
	    			System.out.println(d.get(i).toString());
	    		
	    	}
	    	
	    		
	    		
	    	
	    }

	    public  void printPartName(ArrayList<Dessert> d) {
	    	boolean found = false;
	    	
	    	System.out.println("What dessert are you looking for: ");
	    	scan.nextLine();
	    	String check = scan.nextLine();
	    	
	    	for (int i=0; i < d.size(); i++) {
	    		//System.out.println(d.get(i).getName());
	    		
	    		if (d.get(i).getName().toLowerCase().contains(check)) {
	    			System.out.println(d.get(i).toString());
	    			found = true;
	    		}
	    	}

	    	if (!found)
	    		System.out.println("Sorry - we do not have a dessert with that name");
	    }
	    
	    // use the Comparable to sort by name
	    public void sortDessertNames(ArrayList<Dessert> d) {
	    	Collections.sort(d);
	    	
	    	System.out.println("Now sorted by name.");
	    }
	    
	    //use a bubble sort or equivalent to sort by cost
	    
	    public void sortDessertCosts(ArrayList<Dessert> d) {
	    	
	    	for (int out = d.size() - 1; out > 1; out--) {
	            for (int in = 0; in < out; in++) {
	                Dessert first = d.get(in);
	                Dessert second = d.get(in + 1);
	                double fc = first.getCost();
	                double sc = second.getCost();
	                if (fc > sc) {
	                    d.set(in, second);
	                    d.set(in + 1, first);
	                }
	            }
	        }
	    	
	    	System.out.println("Now sorted by cost");
	    }
	    
	    public  void createOrder(ArrayList<Customer> cust, ArrayList<Dessert> d, ArrayList<Order> orders) {
	    	for (Customer c : cust)
				System.out.println(c.toString());
	    	
	    	int minNum = 1;
	    	int maxNum = d.size();
	    	
	    	int custLoc = 0;
	    	
	    	NumberFormat nf = NumberFormat.getCurrencyInstance();
	    	
	    	System.out.println("Is the customer part of the list? Y/N");
	    	char bChoice = scan.next().charAt(0);
	    	
	    	if (bChoice == 'Y') {
	    		System.out.println("What is the ID of the customer buying?");
	    		int cID = scan.nextInt();
	    		
	    		boolean found = false;
	    		for (int i=0; i < cust.size(); i++)
	    			if (cust.get(i).getId() == cID) {
	    				custLoc = i;
	    				found = true;
	    			}
	    		
	    		if (!found)
	    			System.out.println("Sorry that user ID was not found!");
	    		
	    	} else if (bChoice == 'N') {
	    		scan.nextLine();
	    		System.out.println("What is the new customer's first name:");
	    		String fName = scan.nextLine();
	    		
	    		System.out.println("What is the new customer's last name:");
	    		String lName = scan.nextLine();
	    		
	    		
	    		cust.add(new Customer(lName, fName));
	    		custLoc = cust.size()-1;
	    	}
	    	
	    	boolean done = false;
	    	ArrayList<Dessert> tmpe = new ArrayList<Dessert>();
    		while (!done) {
    			for (int i=0; i < d.size(); i++) {
    				System.out.println((i+1) + ". " + d.get(i).getName() + " Cost: " + nf.format(d.get(i).getCost()));
    			}
    		
    			System.out.println("What dessert would you like to order (" + minNum + "-" + maxNum + ")");
    		
    			int nChoice = scan.nextInt();
    			
    			
    			
    			tmpe.add(d.get(nChoice-1));
    			
    			System.out.println(d.get(nChoice-1).getName() + " has been added to the order, would you like to add another dessert? (Y/N)");
    			char bDone = scan.next().charAt(0);
    			if (bDone == 'N') {
    				orders.add(new Order(tmpe, cust.get(custLoc)));
    				done = true;
    				System.out.println("Your order has been added");
    			}
    		}
	    }
	     
	    public  void printOrders(ArrayList<Order> orders) {
	    	for (Order o : orders)
				System.out.println(o.toString());
	    }
	    
		
}
