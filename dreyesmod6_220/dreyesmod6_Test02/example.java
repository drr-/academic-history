package dreyesmod6_Test02;

import java.util.LinkedList;

public class example {

	public static void main(String[] args) {
		LinkedList<String> animals = new LinkedList<>();
		
		animals.add("Dog");
	    animals.add("Cat");
	    animals.add("Crow");
	    animals.add("Cow");
	    
	    System.out.println("LinkedList: " + animals);
	    
	    animals.add(3, "Bacon");
	    
	    System.out.println("New LinkedList: " + animals + " - second Item " + animals.get(1) );
	}

}
