package dreyesmod6;

import java.util.ArrayList;
import java.util.Scanner;

public class GolfGameDriver {
	
	public static int menu() {
    	Scanner scan = new Scanner(System.in);
    	System.out.println("\n1.  Load the data");
    	System.out.println("2.  Print the names and scores in ascending order");
    	System.out.println("3.  Print the names and scores in descending order");
    	System.out.println("4.  Given the name of a player, find that player’s score and list all other players with the \n"
    			+ "same score");
    	System.out.println("5.  Add a player");
    	System.out.println("6.  Delete a player");
    	System.out.println("7.  Clear the doubly linked list");
    	System.out.println("999. exit the program  ");
    	System.out.println("Choice:");
    	int choice = scan.nextInt();
    	return choice;
    
    }
	
	public  static void load(GolfLinkedList ll) {
		ll.add(new Player("Jordan Spieth", 13));
        ll.add(new Player("Bubba Watson", 9));
        ll.add(new Player("Rury McIlory", 9));
        ll.add(new Player("Tiger Woods", 1));
        ll.add(new Player("Jason Day", 2));
        ll.add(new Player("Justin Rose", 9));
        
        /*
        ll.matches();
        ll.insert();
        */
	}
	
	

	public static void main(String[] args) {
		GolfLinkedList ll = new GolfLinkedList();
		int value = 0;
        while(value !=-999) {
        	value=menu();
        	if (value==1)
        		load(ll);
        	else if (value ==2) {
        		System.out.println("List printed in ascending order:");
        		ll.toStringA();
        	}
        	else if (value ==3) {
        		System.out.println("List printed in decending order:");
        		ll.toStringD();
        	}
        	else if (value ==4) {
        		//Team t = ll.mostWins();
        		ArrayList<Player> e = ll.matches();
        		
        		if (e != null) {
        			System.out.println("Players with the same score: \n");
        			for (int i=e.size()-1; i>=0 ; i--) {
        				System.out.println(e.get(i));
        			}
        		} else {
        			System.out.println("The player/score was not found");
        		}
        		//System.out.println(t.toString());        
        	}
        	else if (value==5) {
        		Scanner scan = new Scanner(System.in);
        		System.out.println("Name of the player you want to insert? ");
        		String name = scan.nextLine();
        		System.out.println("Score of the player you want to insert");
        		int sc1 = scan.nextInt();
        		
        		ll.add(new Player(name, sc1));
        	}
        	else if (value ==6) {
        		ll.locate();
        	}
        	else if (value == 7) {
        		ll.clear();
        		System.out.println("Cleared Linked List");
        	}
        	else if (value==999) {
        		System.out.println("Thanks for using my program");
        		System.exit(0);        
        	}       
        }
	}

}
