package dreyesmod4_csc223;

public class QueueTest {
	static Queue<Integer> takeoff = loadTakeOff();
	static Queue<Integer> land = loadLand();
	
	public static void main(String[] args) {
		boolean coco = true;
		while (coco) {
			String choice = Queue.menu();
			
			String[] cA = choice.split(" ");
			
			if (choice.equalsIgnoreCase("PRINT"))
				printInfo();
			else if (cA[0].equalsIgnoreCase("TAKEOFF")) {
				takeOff(takeoff, cleanInput(cA));
			}
			else if (cA[0].equalsIgnoreCase("LAND"))
				land(land, cleanInput(cA));
			else if (cA[0].equalsIgnoreCase("NEXT"))
				next(takeoff, land);
			else {
				coco = false;
				Queue.cleanup();
			}
		}
	}
	

	private static Queue<Integer> loadTakeOff() {
		Queue<Integer> ttakeoff = new Queue<>();
		
		ttakeoff.enqueue("AA 123");
		ttakeoff.enqueue("Delta 877");
		ttakeoff.enqueue("United 344");
		ttakeoff.enqueue("AA 98");
		
		return ttakeoff;
	}
	
	private static Queue<Integer> loadLand() {
		Queue<Integer> tland = new Queue<>();
		
		tland.enqueue("AA 576");
		tland.enqueue("Delta 83");
		
		return tland;
	}
	
	public static String cleanInput(String[] x) {
		String e = "";
		for (int i = 1; i < x.length; i++)
			e += x[i] + " ";
		//System.out.println("'" + e.strip() + "'");
		
		return e;
	}

	public static void printInfo() {
		System.out.println("Planes ready to take off: ");
		takeoff.print();
		
		System.out.println("\nPlanes ready to land: ");
		land.print();
		
	}

	public static void takeOff(Queue<Integer> takeoff, String name) {
		takeoff.enqueue(name);
		
	}

	public static void land(Queue<Integer> land, String name) {
		land.enqueue(name);
	}
	
	public static void next(Queue<Integer> takeoff, Queue<Integer> land) {
		
		if (!land.isEmpty()) {
			String Next = land.upNext(0);
			System.out.println(Next + " is now landing");
			//takeoff.enqueue(Next);
			land.dequeue();
		} else if (!takeoff.isEmpty() && land.isEmpty()) {
			String Next = takeoff.upNext(0);
			takeoff.dequeue();
			System.out.println("No planes to land so " + Next + " took off");
		} else if ((takeoff.isEmpty()) && (land.isEmpty())) {
			System.out.println("There are no planes in either queue at the current time");
		}
		
	}
}



