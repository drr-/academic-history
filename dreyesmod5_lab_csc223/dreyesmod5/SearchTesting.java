package dreyesmod5;

public class SearchTesting {
	
	public static boolean search(int[] array, int key) {
		for (int i=0; i<array.length; i++) {
			if(array[i]==key)
				return true;
		}
		return false;
	}
	
	public static void main(String[] args) {
		// Super sofistikated tests o-o
		int[] wrongArray = {0,1,2,3,4,5};
		
		System.out.println("Fail: " + search(wrongArray, 99) + "\nPass: " + search(wrongArray, 4));
		
	}
}
