/**
 * 
 */
package dreyesmod5;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * @author SuperSecretVM_User
 *
 */
class TestSearch {
	
	int[] a;

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeEach
	void setUp() throws Exception {
		a = new int[] {1, 2, 3, 4, 6};
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterEach
	void tearDown() throws Exception {
	}

	/**
	 * Test method for {@link dreyesmod5.SearchTesting#search(int[], int)}.
	 */
	@Test
	/*
	final void testSearch() {
		fail("Not yet implemented");
	}
	*/
	
	void testSearchNoItem() {
		Scanner scan = new Scanner(System.in);
		System.out.println("What value do you want to test?");
		int key = scan.nextInt();
		boolean val = (new SearchTesting()).search(a, key);
		assertTrue(val);
	}
	
	
	@Test
	public void testAddAndGet1() {
		//ArrayIntList list = new ArrayIntList();
		ArrayList<Object> list = new ArrayList<>();
		list.add(42);
		list.add(-3);
		list.add(17);
		list.add(99);
		assertEquals(42, list.get(0));
		assertEquals(-3, list.get(1));
		assertEquals(17, list.get(2));
		assertEquals(99, list.get(3));
		
		assertEquals("second attempt", 42, list.get(0));
		assertEquals("second attempt", 99, list.get(3));
	}
	
	@Test
	public void testSize1() {
		ArrayList<Object> list = new ArrayList<>();
		assertEquals(0, list.size());
		list.add(42);
		assertEquals(1, list.size());
		list.add(-3);
		assertEquals(2, list.size());
		list.add(17);
		assertEquals(3, list.size());
		list.add(99);
		assertEquals(4, list.size());
		assertEquals("second attempt", 4, list.size());
	}

}
