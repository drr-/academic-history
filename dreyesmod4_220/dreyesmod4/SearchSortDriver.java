package dreyesmod4;

import java.awt.FileDialog;
import java.awt.Frame;
import java.io.File;
import java.io.FileNotFoundException;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class SearchSortDriver {
	public static Scanner fileData = null;

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
				
		//int[] data = new int[10]; // create array size 10
		ArrayList<String> data = new ArrayList<String>();
		boolean more = true;
		while(more) {
			menu();
			System.out.println("Choice:");
			int choice = scan.nextInt();
			
			if (choice ==1) {
				data = prime(data);
			}
			else if (choice ==2) {
				print(data);
			}
			else if (choice ==3) {
				System.out.println("What value are you searching for?");
				String num = scan.next();
				int loc = LinearSearch.linearSearch(data, num);
				//int loc = 999;
				if (loc>=0)
					System.out.println("The value was found at location " + loc);
				else 
					System.out.println("The value is not in the array");
			}
			else if (choice==4) {
				System.out.println("What value are you searching for?");
				int num = scan.nextInt();
				//int loc = BinarySearch.binarySearch(data, num) ;
				int loc = 999;
				if (loc>=0)
					System.out.println("The value was found at location " + loc);
				else 
					System.out.println("The value is not in the array");
			}
			else if (choice ==5) {
				//MergeSort.mergeSort(data);
				String e = "e";
			}
			
			else if (choice == 6) {
				InsertionSort.insertionSort(data);
				String e = "e";
			}
			else if (choice == 7) {
				//SelectionSort.selectionSort(data);
				String e = "e";
			}
			else if (choice == 8) {
				/*ArrayList<Integer> list = new ArrayList<Integer>();
				for (int i=0;i<data.length;i++)
					list.add(data[i]);
				Collections.shuffle(list);
				for (int i=0;i<data.length;i++)
					data[i] = list.get(i); */
				Collections.shuffle(data);
				
			}
			else {
				System.out.println("BYE!!");
				System.exit(0);
			}

		}
	}
	
	
	public static ArrayList<String> openFile() {
		
		try {
			//File file = new File(pickLocRead());
			File file = new File("/home/user/QubesIncoming/College/BoyNames.txt");
			fileData = new Scanner(file);
			
			ArrayList<String> tempString = new ArrayList<String>();
			
			while (fileData.hasNext()) {
				tempString.add(fileData.next());
			}
			
			return tempString;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return null;
	}
    
    public static String pickLocRead() {
		Frame f = new Frame();
		// decide from where to read the file
		FileDialog fiBox = new FileDialog(f, "Pick location for reading your file", FileDialog.LOAD);
		System.out.println("The dialog box might appear behind Eclipse.  ");
		fiBox.setVisible(true);
		// get the absolute path to the file
		String fiName = fiBox.getFile();
		String dirPath = fiBox.getDirectory();

		// create a file instance for the absolute path
		String name = dirPath + fiName;
		return name;
	}
	
	
	
	public static ArrayList<String> prime(ArrayList<String> values) {
		SecureRandom generator = new SecureRandom();
		/* for (int i = 0; i < values.length; i++) // populate array
	         values[i] = 10 + generator.nextInt(90); */
		
		return openFile();
		
	}
	
	public static void print(ArrayList<String> data) {
		for (int i = 0; i < data.size(); i++) {
			System.out.println(data.get(i));
		}
	}
	
	public static void menu()
    {
        System.out.println("\n1.  Prime the array with new data");
        System.out.println("2.  Print out the array");
        System.out.println("3.  Do a linear search for a number");
        System.out.println("4.  Do a based binary search for a number - note there is also a recursive version for you!");
        System.out.println("    NOTE!! Data must be sorted first before choosing 4");
        System.out.println("5.  Merge sort the integers");
        System.out.println("6.  Insertion sort the integers");
        System.out.println("7.  Selection sort the integers");
        System.out.println("8.  Randomly shuffle the data in the list");
        System.out.println("9.  exit\n"); 
    }

}
