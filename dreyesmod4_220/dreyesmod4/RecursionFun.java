package dreyesmod4;

import java.util.Scanner;

public class RecursionFun {
	
	public static int nomNom = 0;
	public static boolean fFound = false;

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		System.out.println("Give me a string:");
		String value = scan.nextLine();
		//String value = "I love Java!";
		char[] chars = value.toCharArray();
		int choice = 0;
		while (choice !=9) {
			choice = menu();
			
			if (choice==1)
				reverse(chars);
			else if (choice ==2)
				findSubString(value);
			else if (choice==3)
				countChar(chars);
			else {
				System.out.println("Thanks for using my program");
				System.exit(0);
			}				
		}
	}
	
	public static int menu() {
		Scanner scan = new Scanner(System.in);
		System.out.println("MENU:  (must use recursion for the solutions!)");
		System.out.println("1.  Reverse the String");
		System.out.println("2.  Look for a substring in the string");
		System.out.println("3.  Count how many times a character occurs in the string");
		System.out.println("9.  End");
		System.out.println("CHOICE:");
		int ans = scan.nextInt();
		return ans;
	}
	
	public static void reverse(char[] chars) {

		  stringReverseHelper(chars, chars.length-1);
		      System.out.println();
	  }
	
	private static void stringReverseHelper(char[] array, int index)
	   {
		
			if (index != 0) {
				System.out.print(array[index]);
				stringReverseHelper(array, index-1);
			} else {
				System.out.print(array[index] + "\n");
			}
			
	     
	   } 	   

	
	public static boolean find(String text, String[] target, int pos)
	{
		fFound = false;
		
		if (pos != 0) {
			if (target[pos].contains(text)) {
				fFound = true;
				return true;
			} else {
				find(text, target, pos-1);
				return false;
			}
		} else {
			if (target[pos].contains(text)) {
				fFound = true;
				return true;
			} else {
				return false;
			}
		}
	}
	
	public static void findSubString(String s) {
		Scanner scan = new Scanner(System.in);		
		System.out.println("What substring are you looking for?");
		String sub = scan.nextLine();
		
		String[] place = s.split(" ");
		
		boolean found = find(sub,place, place.length-1);
		if (fFound)
			System.out.println("It was found!");
		else
			System.out.println("It was not found");
		
		
	}
	
	public static void countChar(char[] chars) {
		Scanner scan = new Scanner(System.in);
		
		nomNom = 0;
		
		System.out.println("What character are you looking for?");
		char LookieLookie = scan.next().charAt(0);
		
		charCount(chars, chars.length-1, LookieLookie);
		
		
		System.out.println("The character " + LookieLookie + " appears " + nomNom + " times.");
		
	}
	
	public static int charCount(char[] array, int start, char ch)
	{
		if (start != 0) {
			//System.out.println(start + " : " + array[start] + " - " + ch);
			if (array[start] == ch) {
				nomNom++;
			}
			charCount(array, start-1, ch);
			return -1;
		} else {
			if (array[start] == ch) {
				nomNom++;
			}
			return nomNom;
		}
	}

	
}
