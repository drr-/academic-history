package dreyesmod3;

import java.util.ArrayList;

public class Book {
	
	private String title;
	private int copyright;
	private int edition;
	private String isbn;
	private ArrayList<Author> authors;
	

	public Book() {
		super();
	}

	public Book(String title, int copyright, int edition, String isbn, ArrayList<Author> authors) {
		super();
		this.title = title;
		this.copyright = copyright;
		this.edition = edition;
		this.isbn = isbn;
		this.authors = authors;
	}
	
	public String toString() {
		
		String ans="";
		ans = ans + title + " copyright:" + copyright +
				" isbn:" + isbn + " edition: " + edition +
				"\n  written by ";
		for (int i=0;i<authors.size();i++)
			ans = ans + authors.get(i).toString() + " ";
		return ans;
				
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getCopyright() {
		return copyright;
	}

	public void setCopyright(int copyright) {
		this.copyright = copyright;
	}

	public int getEdition() {
		return edition;
	}

	public void setEdition(int edition) {
		this.edition = edition;
	}

	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	public ArrayList<Author> getAuthors() {
		return authors;
	}

	public void setAuthors(ArrayList<Author> authors) {
		this.authors = authors;
	}
	
	
	
	
	

}
