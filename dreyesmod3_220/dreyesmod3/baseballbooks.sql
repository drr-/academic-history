
Create database if not exists bbooks220;

use bbooks220;

CREATE TABLE authors (
   authorID INT NOT NULL  AUTO_INCREMENT,
   firstName varchar (20) NOT NULL,
   lastName varchar (30) NOT NULL,
   PRIMARY KEY (authorID)
);

CREATE TABLE titles (
   isbn varchar (20) NOT NULL,
   title varchar (100) NOT NULL,
   editionNumber INT NOT NULL,
   copyright varchar (4) NOT NULL,
   PRIMARY KEY (isbn)
);

CREATE TABLE isbn (
   authorID INT NOT NULL,
   isbn varchar (20) NOT NULL,
   FOREIGN KEY (authorID) REFERENCES authors (authorID), 
   FOREIGN KEY (isbn) REFERENCES titles (isbn)
);

INSERT INTO authors (firstName, lastName)
VALUES 
    ('Bryce', 'Harper'),
	('Paul ', 'Goldschmidt'),
	('Jose', 'Altuve'),
	('Max', 'Scherzer'),
	('Justin', 'Verlander');

INSERT INTO titles (isbn,title,editionNumber,copyright)
VALUES
    ('0132121360', 'Hall of Fame Inductees', 1, '2019'),
	('0132151006', 'How to Throw a Curveball', 5, '2018'),
	('0132575655', 'Rules for Sliding', 10, '2019'),
	('0133378713', 'Baseball Stadiums That I Love', 9, '2017'),
	('0133379337', 'How to Become an All Star', 5, '2019'),
	('0133406954', 'Baseball Managers That I Hated', 6, '2018'),
	('0133570924', 'Ugly Baseball Uniforms Throughout History', 2, '2018'),
	('0133807800', 'Baseball Score Keeping for Dummies', 10, '2015');
	

INSERT INTO isbn (authorID, isbn) VALUES
(1, '0132151006'),
(4, '0133807800'),
(2, '0133807800'),
(2, '0132575655'),
(3, '0133406954'),
(1, '0133379337'),
(2, '0133379337'),
(2, '0133378713'),
(4, '0133570924'),
(5, '0132121360');