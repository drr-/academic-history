package dreyesmod3;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.regex.Pattern;

public class BaseballMethods {
	public static Scanner scan = new Scanner(System.in);
    
        public Connection createConnection() {
            return null;
        }
	
	public void printBooks(ArrayList<Book> books) {
		for (int i=0;i<books.size();i++)
			System.out.println(books.get(i).toString());
	}
	
	public void printAuthors(ArrayList<Author> authors) {
		for (int i=0;i<authors.size();i++)
			System.out.println(authors.get(i).toString());
	}
	public void getPaul(ArrayList<Book> books) {
		Pattern pattern = Pattern.compile("[^a-zA-Z ]", Pattern.DOTALL);
		
		for (int x=0; x<books.size(); x++) {
			String beepBeep = books.get(x).getAuthors().toString();
			String New = pattern.matcher(beepBeep).replaceAll("");
			
			if (New.contains("Paul Goldschmidt")) {
				System.out.println(books.get(x));
			}
		}
	}
	
	public void getBaseball(ArrayList<Book> books) {
		for (int x=0; x<books.size(); x++) {
			if (books.get(x).getTitle().toLowerCase().contains("baseball")) {
				System.out.println(books.get(x));
			}
			
			//System.out.println(books.get(x).getTitle());
		}
	}
	
	public void getWord(ArrayList<Book> books) {
		
		
		System.out.println("What word are you looking for?");
		String SearchTerm = scan.nextLine();
		
		
		
		for (int x=0; x<books.size(); x++) {
			if (books.get(x).getTitle().toLowerCase().contains(SearchTerm)) {
				System.out.println(books.get(x));
			}
			
			//System.out.println(books.get(x).getTitle());
		}
	}
	
	public void addBook(ArrayList<Book> books, ArrayList<Author> aut) {
		
		boolean moreBooks = true;
		
		while(moreBooks) {
			ArrayList<Integer> authLoc = new ArrayList<>();
			
			ArrayList<Author> tempAuth = new ArrayList<Author>();
			
			System.out.println("Title of book?");
			String titleBook = scan.nextLine();
			
			System.out.println("Copyright year?");
			int copyrightYear = scan.nextInt();
			
			System.out.println("Edition number?");
			int editionNumber = scan.nextInt();
						
			boolean moreAuthors = true;
			while (moreAuthors) {
				System.out.println("Author's first name?");
				scan.next();
				String firstName = scan.nextLine();
				
				System.out.println("Author's last name?");
				String lastName = scan.nextLine();
				
				authLoc.add(aut.size());
				
				aut.add(new Author(firstName, lastName));
				
				System.out.println("More authors? (true/false)");
				moreAuthors = scan.nextBoolean();
			}
			System.out.println("Lastly, ISBN?");
			scan.next();
			String isbn = scan.nextLine();
			
			for (int i = 0; i<authLoc.size(); i++) {
				tempAuth.add(aut.get(authLoc.get(i)));
			}
			
			books.add(new Book(titleBook,copyrightYear,editionNumber,isbn,tempAuth ));
			
			System.out.println("Add more books? (true/false)");
			moreBooks = scan.nextBoolean();
		}
	}

}
