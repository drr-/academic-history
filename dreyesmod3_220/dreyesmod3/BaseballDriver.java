package dreyesmod3;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Scanner;

public class BaseballDriver {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		BaseballMethods bm = new BaseballMethods();
		
		ArrayList<Author> authors = new ArrayList<Author>();
		authors=loadAuthors();
		ArrayList<Book> books = new ArrayList<Book>();
		books = loadBooks(authors);
                Connection conn;
		
		boolean more = true;
		while(more) {
			menu();
			System.out.println("Choice:");
			int ans = scan.nextInt();
                        if (ans==0)
                            conn = bm.createConnection();
			if (ans == 1)
				bm.printBooks(books);
			else if (ans == 2)
				bm.printAuthors(authors);
			else if (ans==3)
				bm.getPaul(books);
			else if (ans == 4)
				bm.getBaseball(books);
			else if (ans == 5)
				bm.getWord(books);
			else if (ans == 6)
				bm.addBook(books,authors);
			else {
				System.out.println("BYE!!!");
				more = false;
				System.exit(0);
			}
				
			
		}
		
	}
	
	public static void menu() {
		System.out.println("Choose one:");
		System.out.println("   1. Print out all of the book information");
		System.out.println("   2. Print out all of the author information");
		System.out.println("   3. Select all of the books written by Paul Goldschmidt.");
		System.out.println("   4. Find all of the books that have the word 'baseball' in the name.");
		System.out.println("   5. Look for a book that has a word --user supplied -- in the title");
		System.out.println("   6. Add a new book");
		System.out.println("   7. Exit");
	}
	
	public static ArrayList<Author> loadAuthors() {
		ArrayList<Author> a = new ArrayList<Author>();
		a.add(new Author("Bryce","Harper"));
		a.add(new Author("Paul","Goldschmidt"));
		a.add(new Author("Jose", "Altuve"));
		a.add(new Author("Max", "Scherzer"));
		a.add(new Author("Justin", "Verlander"));		
		return a;
	}
	
	public static ArrayList<Book> loadBooks( ArrayList<Author> a) {
		ArrayList<Book> b = new ArrayList<Book>();
		
		ArrayList<Author> au = new ArrayList<Author>();
		au.add(a.get(4));
		b.add (new Book("Hall of Fame Inductees",2019,1,"0132121360",au ));
		
		au=new ArrayList<Author>();
		au.add(a.get(0));
		b.add (new Book("How To Throw A Curverball",2018,5,"0132151006",au ));
		
		au=new ArrayList<Author>();
		au.add(a.get(1));
		b.add (new Book("Rules for Sliding",2019,10,"0132575655",au ));
		
		au=new ArrayList<Author>();
		au.add(a.get(1));
		b.add (new Book("Baseball Stadiums I Have Loved",2017,9,"0133378723",au ));
		
		au=new ArrayList<Author>();
		au.add(a.get(0));   au.add(a.get(1));
		b.add (new Book("How to Become an All Star",2019,5,"0133379337",au ));
		
		au=new ArrayList<Author>();
		au.add(a.get(2));   
		b.add (new Book("Baseball Managers that I Have Hated",2018,6,"0133406954",au ));
		
		au=new ArrayList<Author>();
		au.add(a.get(3));   
		b.add (new Book("Ugly Baseball Uniforms Throughout History",2018,2,"0133570924",au ));
		
		au=new ArrayList<Author>();
		au.add(a.get(1));  au.add(a.get(3)); 
		b.add (new Book("Baseball Score Keeping for Dummies",2015,10,"0133807800",au ));
		
		return b;
	}

}

