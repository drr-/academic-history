package dreyesmod3;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Scanner;

public class BaseballDriverDB {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		
		//BaseballMethodDB bm = new BaseballMethodDB();
		
		
		boolean more = true;
		while(more) {
			menu();
			System.out.println("Choice:");
			int ans = scan.nextInt();
			if (ans == 1)
				BaseballMethodDB.printBooks();
			else if (ans == 2)
				BaseballMethodDB.printAuthors();
			else if (ans==3)
				BaseballMethodDB.getPaul();
			else if (ans == 4)
				BaseballMethodDB.getBaseball();
			else if (ans == 5)
				BaseballMethodDB.getWord();
			else if (ans == 6)
				BaseballMethodDB.addBook();
			else {
				System.out.println("BYE!!!");
				more = false;
				System.exit(0);
			}
				
			
		}
		
	}
	
	public static void menu() {
		System.out.println("Choose one:");
		System.out.println("   1. Print out all of the book information");
		System.out.println("   2. Print out all of the author information");
		System.out.println("   3. Select all of the books written by Paul Goldschmidt.");
		System.out.println("   4. Find all of the books that have the word 'baseball' in the name.");
		System.out.println("   5. Look for a book that has a word --user supplied -- in the title");
		System.out.println("   6. Add a new book");
		System.out.println("   7. Exit");
	}
	

}

