# Quick readme

This project and all previous projects have been on Linux
<br>While anyone else using Linux most likely would not need a tutorial, in the spirit of READMEs here's a tutorial


## Initial Setup
## XAMPP
* Download XAMPP from <https://www.apachefriends.org/download.html>
* Go to downloads: ` cd ~/Downloads `
* Run the installer as sudo(or doas): `sudo ./xampp-linux-x64-8.1.6-0-installer.run`
* Go through the installer

> if running through a vm
>>* GUI Location can be found at `./opt/lampp/manager-linux-x64.run`

* Run GUI Manager
* Start `MySQL Database`
* Start `Apache Web Server`


## MYSQL
* Install Mysql-client: `sudo apt-get install mariadb-client -y`
* Login via cli: `mariadb --host=127.0.0.1 -P 3306 -u root`

> '127.0.0.1' as "--host" (and NOT 'localhost') is very critical as mysql is not "directly" installed

* To install sql files (while in mysql-cli): "source location/of/sql/baseballbooks.sql"

## phpMyAdmin
* URL: `http://localhost/phpmyadmin`


# Post
Everything else should work like normal