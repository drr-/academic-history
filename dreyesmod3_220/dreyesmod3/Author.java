package dreyesmod3;

public class Author {
	
	private int id;
	private String first;
	private String last;
	private static int nextNum=1;
	
	public Author() {
		id = nextNum;
		nextNum++;
		
	}
	
	public Author (String f, String l) {
		id = nextNum;
		first = f;
		last = l;
		nextNum++;
	}
	
	public String toString() {
		return id + ":" +first + " " + last;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFirst() {
		return first;
	}

	public void setFirst(String first) {
		this.first = first;
	}

	public String getLast() {
		return last;
	}

	public void setLast(String last) {
		this.last = last;
	}
	
	


}
