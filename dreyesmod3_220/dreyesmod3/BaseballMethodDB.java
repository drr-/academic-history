package dreyesmod3;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.regex.Pattern;

public class BaseballMethodDB {
	public static Scanner scan = new Scanner(System.in);
	static Connection conn = null;
	static Statement stmt = null;
    
	public static Connection createConnection() {

		String user = "itp220";
		String pass = "itp220";
		String name = "bbooks220";
		String driver = "com.mysql.jdbc.Driver";
		String url = "jdbc:mysql://localhost:3306/" + name;

		//System.out.println(driver);
		//System.out.println(url);

		try {
			Class.forName(driver).newInstance();
			conn = DriverManager.getConnection(url, user, pass);
			//System.out.println("Connection really is from : " + conn.getClass().getName());
			//System.out.println("Connection successful!");

		} catch (Exception e) {
			e.printStackTrace();
		}
		return conn;
	}
	
	public static void closeConnection() {
		if (conn != null) {
			try {
				conn.close();
				conn = null;
				// stmt.close();
				System.out.println("The connection was successfully closed");
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	
	
	public static void checkConnect() {
		if (conn == null) {
			conn = createConnection();
		}
		if (stmt == null) {
			try {
				stmt = conn.createStatement();
			} catch (SQLException e) {
				System.out.println("Cannot create the statement");
			}

		}
	}
	
	
	public static void printBooks() {
		
		checkConnect();
				
		try {
			String query = "SELECT * FROM titles,isbn,authors WHERE titles.isbn = isbn.isbn AND authors.authorID = isbn.authorID ORDER BY isbn.isbn";
			
			stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			
			while (rs.next()) {
				System.out.println(rs.getString(2) + " copyright: " + rs.getInt(4) 
					+ " isbn:" + rs.getString(1) + " edition: " + rs.getInt(3) 
					+ "\n  written by " + rs.getInt(5) + ":" + rs.getString(8) + " " + rs.getString(9) );
			}
		  } catch (SQLException e) {
			  System.out.println("Cannot create the statement" + e); 
		}
	}
	
	public static void printAuthors() {
		checkConnect();
		
		try {
			String query = "SELECT * FROM authors";
			
			stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			
			while (rs.next()) {
				System.out.println(rs.getInt(1) + ":" + rs.getString(2) + " " +rs.getString(3) );
			}
		  } catch (SQLException e) {
			  System.out.println("Cannot create the statement"); 
		  }
	}
	public static void getPaul() {
		checkConnect();
		
		try {
			String query = "SELECT * FROM titles,isbn,authors WHERE titles.isbn = isbn.isbn AND authors.authorID = isbn.authorID AND authors.lastName LIKE '%Goldschmidt%' AND authors.firstName LIKE '%Paul%' ORDER BY isbn.isbn";
			
			stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			
			while (rs.next()) {
				System.out.println(rs.getString(2) + " copyright: " + rs.getInt(4) 
				+ " isbn:" + rs.getString(1) + " edition: " + rs.getInt(3) 
				+ "\n  written by " + rs.getInt(5) + ":" + rs.getString(8) + " " + rs.getString(9) );
			}
		  } catch (SQLException e) {
			  System.out.println("Cannot create the statement"); 
		  }
	}
	
	public static void getBaseball() {
		checkConnect();
		
		try {
			String query = "SELECT * FROM titles,isbn,authors WHERE titles.isbn = isbn.isbn AND authors.authorID = isbn.authorID AND titles.title LIKE '%Baseball%' ORDER BY isbn.isbn;";
			
			stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			
			while (rs.next()) {
				System.out.println(rs.getString(2) + " copyright: " + rs.getInt(4) 
				+ " isbn:" + rs.getString(1) + " edition: " + rs.getInt(3) 
				+ "\n  written by " + rs.getInt(5) + ":" + rs.getString(8) + " " + rs.getString(9) );
			}
		  } catch (SQLException e) {
			  System.out.println("Cannot create the statement"); 
		  }
	}
	
	public static void getWord() {
		checkConnect();
		
		System.out.println("What word are you looking for?");
		String SearchTerm = scan.nextLine();
		
		
		
		if (conn == null) {
			System.out.println("A connection does not exist");
		}
		
		try {
			String query = "SELECT * FROM titles,isbn,authors WHERE titles.isbn = isbn.isbn AND authors.authorID = isbn.authorID AND titles.title LIKE '%" + SearchTerm + "%' ORDER BY isbn.isbn;";
			
			stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			
			while (rs.next()) {
				System.out.println(rs.getString(2) + " copyright: " + rs.getInt(4) 
				+ " isbn:" + rs.getString(1) + " edition: " + rs.getInt(3) 
				+ "\n  written by " + rs.getInt(5) + ":" + rs.getString(8) + " " + rs.getString(9) );
			}
		  } catch (SQLException e) {
			  System.out.println("Cannot create the statement"); 
		  }
	}
	
	public static void addBook() {
		
		checkConnect();
		
		
		boolean moreBooks = true;
		
		while(moreBooks) {
			ArrayList<Integer> authLoc = new ArrayList<>();
			ArrayList<Author> tempAuth = new ArrayList<Author>();
			
			
			
			System.out.println("Title of book?");
			String titleBook = scan.nextLine();
			
			System.out.println("Copyright year?");
			int copyrightYear = scan.nextInt();
			
			System.out.println("Edition number?");
			int editionNumber = scan.nextInt();
						
			boolean moreAuthors = true;
			while (moreAuthors) {
				System.out.println("Author's first name?");
				String firstName = scan.next();
				
				System.out.println("Author's last name?");
				String lastName = scan.next();
				
				
				try {
					String query = "INSERT IGNORE INTO authors (firstName, lastName) VALUES ('" + firstName + "', '" + lastName + "')";
					
					stmt = conn.createStatement();
					int rs = stmt.executeUpdate(query);
					
					
				  } catch (SQLException e) {
					  System.out.println("Cannot create the statement"); 
				  }
				
				
				try {
					String query = "SELECT * FROM authors ORDER BY authorID DESC LIMIT 1";
					
					stmt = conn.createStatement();
					ResultSet rs = stmt.executeQuery(query);
					
					while (rs.next()) {
						authLoc.add(rs.getInt(1));
						rs.getString(2);
						rs.getString(3);
						System.out.println( );
					}
				  } catch (SQLException e) {
					  System.out.println("Cannot create the statement");  
				  } 
				
				
				
				System.out.println("More authors? (true/false)");
				moreAuthors = scan.nextBoolean();
			}
			System.out.println("Lastly, ISBN?");
			String isbnA = scan.next();
			
			
			
			try {
				String query = "INSERT INTO titles (isbn,title,editionNumber,copyright) VALUES ('" + isbnA + "', '" + titleBook + "', " + editionNumber + ", '" + copyrightYear +"')";
				
				stmt = conn.createStatement();
				int rs = stmt.executeUpdate(query);
				
				
			  } catch (SQLException e) {
				  e.printStackTrace();
				  System.out.println("Cannot create the statement" + e); 
			  }
			
			for (int i = 0; i<authLoc.size(); i++) {
				try {
					String query = "INSERT INTO isbn (authorID, isbn) VALUES (" + authLoc.get(i) + ", '" + isbnA + "')";
					
					stmt = conn.createStatement();
					int rs = stmt.executeUpdate(query);
					
					
				  } catch (SQLException e) {
					  System.out.println("Cannot create the statement"); 
				  }
			}
			
			System.out.println("Add more books? (true/false)");
			moreBooks = scan.nextBoolean();
		}
	}

}
