# Academic Timeline
- Virginia Western CC -> Radford
- TLDR: This shows most (90%) the projects that were done during my Coding related Classes.
- Structure:
```
# School
## Class
### Assignment
```

# Virginia Western CC-
## Java I
### dreyesmod1_220
> Array-based food/employee checksums
- Array2D.java
- Cake.java
- Candy.java
- Cookie.java
- Customer.java
- Dessert.java
- DessertsSale.java
- EmployeeDriver.java
- Employee.java
- IceCream.java
- IllegalNameException.java
- IllegalSalaryException.java
- Array2D.java
- Order.java
- OrderMenu.java
- Sundae.java



### dreyesmod2_220
> Employee monetary + Capital Mapping + Tickets Java Project
- EmployeeDriver.java
- Employee.java
- IllegalSalaryException.java
- SentencePlay.java
- StateCapitals.java
- TicketDriver.java
- Ticket.java



### dreyesmod3_220
> Database storage and retrieval for Baseball and Books
- Author.java
- BaseballDriverDB.java
- BaseballDriver.java
- BaseballMethodDB.java
- BaseballMethods.java
- Book.java



### dreyesmod4_220
> Sorting algorithms implementations
- BinarySearch.java
- InsertionSort.java
- LinearSearch.java
- MergeSort.java
- RecursionFun.java
- RecursiveBinarySearch.java
- SearchSortDriver.java
- SelectionSort.java
- Test.java


### dreyesmod6_220
> Sports examples
- FootballLL.java
- GolfGameDriver.java
- GolfLinkedList.java
- Player.java
- Team.java
- example.java
- FootballLL.java
- Player.java
- Team.java
- example.java
- FootballLL.java
- Player.java
- Team.java
- example.java
- FootballLL.java
- Player.java
- Team.java
- example.java
- FootballLL.java
- GolfLinkedList.java
- Player.java
- Team.java



### dreyesmod7_220
> Queues Project
- List.java
- NestedDelimeters.java
- QueueDriver.java
- Queue.java




















## Java II
### dreyesmod1_223
> Webpage project
- WebPage.java



### dreyesmod2_223
> Algorithm Project
- ArrayRecursion.java
- ArrayRecursionTest.java
- ConvertBaseVersionOne.java
- ConvertBaseVersionTwo.java
- HighLow.java
- HighLowTest.java
- MyList.java
- MyListTest.java



### dreyesmod3_wedding_quiz
> Quiz response for assignment
- LotteryList.java
- WeddingLotteryList.java



### dreyesmod4_quiz
> Another quiz response
- List.java
- Portfolio.java
- Stack.java
- Stock.java
- StockPurchase.java



### Eagles_Aeroplane_Project
> Semester-long project
- Customer.java
- Flight.java
- LoadData.java
- Person.java
- Pilot.java
- Reservation.java
- Seat.java



### Eagles_Submitted
> Semester-long project (final)
- Customer.java
- EADriverDB.java
- EAMethodsDB.java
- Flight.java
- JDBCConnection.java
- Person.java
- Pilot.java
- Reservation.java
- Seat.java



### Eagles_Remade
> Semester-long project (redo)
- EADriverDB.java
- EAMethodDB.java



### Eagles_New
> Semester-long project (reset)
- EADriverDB.java
- EAMethodsDB.java
- JDBCConnection.java








## Java III
### dreyesmod1_csc223
> Full drivers for multiple projects
- AccountDriver.java
- AccountIDComparator.java
- Account.java
- CheckingAccount.java
- Customer.java
- NameComparator.java
- PlayWithDates.java
- SavingsAccount.java
- SuperSavings.java
- Cake.java
- Candy.java
- Cookie.java
- Customer.java
- Dessert.java
- DessertsSale.java
- IceCream.java
- Order.java
- OrderMenu.java
- Sundae.java


### dreyesmod2_wk1
> Puzzle Assignment
- puzzlePuzzlePuzzle.java



### dreyesmod3_csc223
> Sports project
- FootballLL.java
- GolfGameDriver.java
- GolfLinkedList.java
- Player.java
- Team.java



### dreyesmod4_csc223
> Queues project
- List.java
- NestedDelimeters.java
- Queue.java
- QueueTest.java



### dreyesmod5_lab_csc223
> Searching project
- SearchTesting.java
- TestSearch.java



### dreyesmod6_csc223
> Algorithms implementation
- BinarySearch.java
- InsertionSort.java
- LinearSearch.java
- MergeSort.java
- RecursionFun.java
- SearchSortDriver.java
- SelectionSort.java
- SubstringRec.java





### dreyesmod7_csc223
> State capitals mapping
- StateCapitals.java



### dreyesmod7_lab_csc223
> Encoding project
- War_Peace.java



### dreyesmod8_csc223
> Decision Tree
- DecisionTreeApp.java
- DecisionTree.java



-----

## Radford: ITEC
### radford_itech_340_02
> Queues Types
- ListDriver.java
- QueueDriver.java
- StackDriver.java



### radford_itech340_02__liveData
> API Grabbing project
- LiveDataDriver.java



### radford_itech340_02__liveData_bkup
> PoC examples
- merge_two.java
- OWM_org.java
- slim.java
- tet.java


### Resubmit Game
> Extracted game (from gradle)
- Board.java
- Driver.java
- Shape.java
- Snek.java
- Tetras.java
- TikTakToe.java

