package dreyesmod2_220;

import java.awt.FileDialog;
import java.awt.Frame;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;


public class EmployeeDriver {

	public static void main(String[] args) {
		
		int choice = 0;
                ArrayList<Employee> emp = new ArrayList<Employee>();
		
		int valid =0;
		
		while (choice >=0) {
			
			try {
				choice = menu();
			} catch (InputMismatchException e) {
				System.out.println("Error: That is not an option!");
			}
        	
        	
        	if (choice ==1)
        		emp=inputData();
        	else if (choice ==2)
        		emp= readData();
        	else if (choice ==3) {
        		printData(emp);        		
        	}
        	else if (choice ==4) {
        		saveData(emp);
        	}
        	
        	else if (choice ==5) {
        		System.out.println("Bye!!!!!");
        		choice=-1;
        		System.exit(0);
        	}
        }  // end while
  	}

	
	public static int menu() {
		Scanner keyboard = new Scanner (System.in);
		System.out.println("\nChoice:");
		System.out.println("   1.  Input employees from keyboard");
		System.out.println("   2.  Read employees from file");
		System.out.println("   3.  Print employees");
		System.out.println("   4.  Save employee data");
		System.out.println("   5.  Quit!! ");
		System.out.println("\nCHOICE:");
		int value = keyboard.nextInt();
		return value;
		
	}
	
	public static ArrayList<Employee> inputData(){
		Scanner keyboard = new Scanner(System.in);
                ArrayList<Employee> emp = new ArrayList<Employee>();
		boolean more = true;
		while (more) {
			System.out.println("Employee name: " );
			String who= keyboard.nextLine();
			System.out.println("Salary: ");
			int amt = 0;
			try {
				amt = keyboard.nextInt();
				
				if (amt < 0) {
					throw new IllegalSalaryException("ILLEAGAL SALARY");
				}
				
			} catch (IllegalSalaryException e) {
				e.printStackTrace();
			}
			Employee em = new Employee(who,amt);
            emp.add(em);
                        
			System.out.println("More (true/false)");
			more = keyboard.nextBoolean();			
			keyboard.nextLine();
		}
		return emp;
	}
	
	public static void printData(ArrayList<Employee> e){
		System.out.println("Employees:");
		for (int i=0;i<e.size();i++)
			System.out.println(e.get(i).toString());
	}
                   // First, read the name.
	
	public static ArrayList<Employee> readData() {
            ArrayList<Employee> emp;
            Frame f = new Frame();
    		// decide from where to read the file
    		FileDialog foBox = new FileDialog(f, "Pick location for reading your file", FileDialog.LOAD);
    		System.out.println("The dialog box will appear behind Eclipse.  " + 
    		      "\n   Choose where you would like to read from.");
    		foBox.setVisible(true);
    		// get the absolute path to the file
    		String foName = foBox.getFile();
    		String dirPath = foBox.getDirectory();

    		// create a file instance for the absolute path
    		File inFile = new File(dirPath + foName);
    		if (!inFile.exists()) {
    			System.out.println("That file does not exist");
    			System.exit(0);
    		}
            Scanner employeeFile = null;
			try {
				employeeFile = new Scanner(inFile);
			} catch (FileNotFoundException e) {
				
				e.printStackTrace();
			}
                emp = new ArrayList<Employee>();
                while (employeeFile.hasNext())
                {
                   // Read a record from the file.
                   String who = employeeFile.nextLine();
                   
                   // Next read the salary.
                   int amt= employeeFile.nextInt();
                   // Read the newline that was left by nextInt...                 
                   employeeFile.nextLine();
                   Employee em = new Employee(who,amt);
                   emp.add(em);
                }
           
	      System.out.println("The data was read from the file into the array list");
          employeeFile.close();    
	      return emp;
	}
	
	public static void saveData(ArrayList<Employee> e) {
		// Open a file named employees.dat.
		
		Frame f = new Frame();
		// decide from where to read the file
		FileDialog foBox = new FileDialog(f, "Pick location for saving your file", FileDialog.SAVE);
		System.out.println("The dialog box will appear behind Eclipse.  " + 
		      "\n   Choose where you would like to read from.");
		foBox.setVisible(true);
		// get the absolute path to the file
		String foName = foBox.getFile();
		String dirPath = foBox.getDirectory();

		// create a file instance for the absolute path
		File outFile = new File(dirPath + foName);
		
	      PrintWriter employeeFile = null;
		try {
			employeeFile = new PrintWriter(outFile);
		} catch (FileNotFoundException e1) {
	
			e1.printStackTrace();
		}
	      
	      // Get each employee's data and write it to the file.
	      for (int counter = 0; counter < e.size(); counter++){
                   Employee emp =e.get(counter); 
	    	  employeeFile.println(emp.getName());
	    	  employeeFile.println(emp.getSalary());	      	   
	      }  
	      employeeFile.close();
	      System.out.println("The data was saved to a file");
	}
	
	
	
		
	
	}

