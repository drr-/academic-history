package dreyesmod2_220;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Ticket {
	
	public static Scanner scan = new Scanner(System.in);
    
    // constructor
    public Ticket() {
    }
    
    // adds up the cost of each seat
    public static int getTotal(int[][] array){
    	int total = 0;
    	for (int x = 0; x < array.length; x++) {
    		for (int y = 0; y < array[x].length; y++) {
    			total += array[x][y];
    		}
    	}
    	
    	return total;        
    }
    
    // adds up the number of available seats (precluding white spaces)
    public static int getElementCount(int[][] array) {
    	return (array.length * array[0].length);
    }
    
    // for the whole array, calculates the average cost per seat
    public static double getAverage (int[][] array) {
    	int total = 0;
    	for (int x = 0; x < array.length; x++) {
    		for (int y = 0; y < array[x].length; y++) {
    			total += array[x][y];
    		}
    	}
    	
    	int seats = (array.length * array[0].length);
    	
        return ((double)total/seats);
    }
    
    // for the selected row, calculates the average cost per seat
    public static double getRowAverage(int[][] array, int row) {
    	
    	int total = 0;
    	
    	for (int x = 0; x < array[row].length; x++) {
    		total += array[row][x];
    	}
    	
       return ((double)total/array[row].length*1);
    }  
    
    // prints the physical layout of each seat
    public static void printArray(int[][] array) {
        System.out.println("The theatre: (any seat with a zero is sold)");
        
        for (int x = 0; x < array.length; x++) {
    		for (int y = 0; y < array[x].length; y++) {
    			System.out.print(array[x][y] + "\t");
    		}
    		System.out.println("");
    	}
        
    }
    
    // for the selected row/seat, calculates the sale price
    public static int sellTicket(int[][] array, int row, int seat) {
    	return array[row][seat];
    }   
}
