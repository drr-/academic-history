package dreyesmod2_220;
import java.text.NumberFormat;
import java.util.Scanner;
import java.awt.FileDialog;
import java.awt.Frame;
import java.io.*;
import java.util.StringTokenizer;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.NoSuchElementException;


public class TicketDriver {
	public static int amtSold = 0;
	public static Scanner data = null;
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
	NumberFormat nf = NumberFormat.getCurrencyInstance();
        int [][] tickets = {};
        
        
    
        // prompt user to select menu option
        while (true) {
            menu();
            System.out.println("Choice:");
            String choice = scan.nextLine();
            
            if (choice.equalsIgnoreCase("a"))
            	tickets = open();
            //tickets = fakeLoad();
            
            if (choice.equalsIgnoreCase("b")) {
                System.out.println("The total value if you sell all seats is: " +     
                nf.format(Ticket.getTotal(tickets)));
            }
            
            if (choice.equalsIgnoreCase("c")) {
                System.out.println("The total number of seats in our theater is : " +     
                Ticket.getElementCount(tickets));
            }
            
            if (choice.equalsIgnoreCase("d")) {
                System.out.println("The average cost of a ticket in our theater is: " +
                nf.format(Ticket.getAverage(tickets)));
                System.out.println("Row summary:");
                for (int row=0; row<tickets.length; row++) {
                    System.out.println("   Average cost of a ticket in " + row + ": " +
                        nf.format(Ticket.getRowAverage(tickets, row)));
                }
            }
            
            if (choice.equalsIgnoreCase("e")) {
            	Ticket.printArray(tickets);
            	
            	System.out.println("Which ticket would you like to buy? ");
                
                boolean again = true;
                while (again) {
                try {
                	System.out.println("Select a row # (first row is 0)");
                	int sRow = scan.nextInt();
                	
                	System.out.println("Select a seat # (first seat is 0)");
                	int sSeat = scan.nextInt();
                	
                	if (!(Ticket.sellTicket(tickets, sRow, sSeat) == 0)) {
                		System.out.println(tickets[sRow][sSeat]);
                		
                		System.out.println("SOLD for " + nf.format(tickets[sRow][sSeat]));
                		
                		amtSold += tickets[sRow][sSeat];
                		
                		tickets[sRow][sSeat] = 0;
                		
                		System.out.println("More tickets to buy? (y/n)");
                		char mTickets = scan.next().toLowerCase().charAt(0);
                		
                		switch (mTickets) {
                		case 'y':
                			Ticket.printArray(tickets);
                			continue;
                		case 'n':
                			again = false;
                			break;
                		default:
                			again = false;
                			break;
                		}
                	} else {
                		System.out.println("That seat is unavailable\nMore tickets to buy? (y/n)");
                		char mTickets = scan.next().toLowerCase().charAt(0);
                		
                		switch (mTickets) {
                		case 'y':
                			Ticket.printArray(tickets);
                			continue;
                		case 'n':
                			again = false;
                			break;
                		default:
                			again = false;
                			break;
                		}
                	}
                	
                	
                } catch (InputMismatchException e) {
                	System.err.println("Nurp");
                }
                }
                
                System.out.println("After ticket sales of " + nf.format(amtSold) + " the theater looks like");
                
                Ticket.printArray(tickets);
            	
            }
            
            if (choice.equalsIgnoreCase("f"))
                writeFile(tickets);
            
            if (choice.equalsIgnoreCase("g")) {
            	System.out.println("Enjoy the show!");
            	System.exit(0);
            }
        }
    }

    public static void menu() {
        System.out.println("\n----------------------------\n");    
        System.out.println("a.  Read the theater layout data from the file");
        System.out.println("b.  Print the total value if you sell all of the seats");
        System.out.println("c.  Print the total number of seats in the theater");
        System.out.println("d.  Print the average ticket cost for the event");
        System.out.println("e.  Sell a seat");            
        System.out.println("f.  Store the current theater event back to a text file");
        System.out.println("g.  Exit\n");
        System.out.println("\n----------------------------\n");
    }
    
    public static int[][] open() {            	
    	openFile();
    	
    	
    	// Create a 2d arraylist since a normal array would need a "known" number of rows
    	ArrayList<ArrayList<Integer>> array_list = new ArrayList<ArrayList<Integer>>();
    	
    	// initialize a temp array
    	int[][] temp_array = null;
    	
    	int count = 0;
    	try {
    		// start reading the file
			while (data.hasNextLine()) {
				array_list.add(new ArrayList<Integer>()); // each row builds up the 2d arraylist
				
				String[] tempArr = null;
				
				tempArr = data.nextLine().split(","); // split the entire row into a string array
				
				for (int i = 0; i <= tempArr.length-1; i++) {
					array_list.get(count).add(i, Integer.parseInt(tempArr[i])); // convert the string to int
				}
				
				count += 1; // for the next line
			}
			
			
			temp_array = new int[array_list.size()][]; // Now the full size is known
			for (int x = 0; x < array_list.size(); x++) {
				ArrayList<Integer> row = array_list.get(x); // start converting the 2d arraylist to array
				
				int[] step_one = new int[row.size()];
				for (int y = 0; y < row.size(); y++) {
					step_one[y] = row.get(y); // black magic
				}
				temp_array[x] = step_one;
			}
			
		} catch (NoSuchElementException elementException) {
			System.err.println("File improperly formed. Terminating.");
		} catch (IllegalStateException stateException) {
			System.err.println("Error reading from file. Terminating.");
		}
    	
        return temp_array;
    }
    
    public static void openFile() {
		try {
			File file = new File(pickLocRead());
			data = new Scanner(file);
			//data.useDelimiter(" - ");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
    
    public static String pickLocRead() {
		Frame f = new Frame();
		// decide from where to read the file
		FileDialog fiBox = new FileDialog(f, "Pick location for reading your file", FileDialog.LOAD);
		System.out.println("The dialog box might appear behind Eclipse.  " + 
			      "\n   Choose where you would like to read for your data.");
		fiBox.setVisible(true);
		// get the absolute path to the file
		String fiName = fiBox.getFile();
		String dirPath = fiBox.getDirectory();

		// create a file instance for the absolute path
		String name = dirPath + fiName;
		return name;
	}
    
    public static int[][] fakeLoad() {
    	int[][] seats = {{0,0,90,120,150,150,150,150,150,150,120,90,0,0},
    			{0,0,60,90,90,120,150,150,120,90,90,60,0,0},
    			{0,0,60,60,90,90,120,120,90,90,60,30,0,0},
    			{0,0,30,30,60,60,60,60,60,60,30,30,0,0},
    			{0,0,30,30,60,60,60,60,60,60,30,30,0,0},
    			{0,0,30,30,30,30,30,30,30,30,30,30,0,0},
    			{0,0,30,30,30,30,30,30,30,30,30,30,0,0},
    			{20,20,30,30,30,30,30,30,30,30,30,30,20,20},
    			{20,20,20,20,20,20,20,20,20,20,20,20,20,20}};
    	return seats;
    }
    
    // breaks each line into tokens, and loads into 2-D array
    public static int[][] composeList(BufferedReader br) {
    	
    	
        return new int[0][0];    //place holder
        
        /*
         * My apologies, but I genuinely couldn't wrap my head around this
         * So plan B was a go 
         */
    }
    
    public static void writeFile(int[][] tickets) {
    	
    	Frame f = new Frame();
		// decide from where to read the file
		FileDialog foBox = new FileDialog(f, "Pick location for writing your file", FileDialog.SAVE);
		System.out.println("The dialog box might appear behind Eclipse.  " + 
			      "\n   Choose where you would like to write your data.");
		
		
		foBox.setVisible(true);
		foBox.toFront();
		foBox.setAlwaysOnTop(true);
		// get the absolute path to the file
		String foName = foBox.getFile();
		String dirPath = foBox.getDirectory();
		String name = dirPath + foName;
		
		
		PrintWriter outputFile = null;
		try {
			outputFile = new PrintWriter(name);

			// Get data and write it to the file.
			for (int x = 0; x < tickets.length; x++) {
	    		for (int y = 0; y < tickets[x].length; y++) {
	    			//System.out.print(tickets[x][y] + "\t");
	    			if (y == tickets[x].length-1) {
	    				outputFile.print(tickets[x][y]);
	    			} else {
	    				outputFile.print(tickets[x][y] + ",");
	    			}
	    		}
	    		outputFile.print("\n");
	    	}

		} catch (IOException ioe) {
			System.out.println("IO Exception.  Need to exit");
			System.exit(0);

		} finally {
			// Close the file.
			outputFile.close();
		}
    }
}