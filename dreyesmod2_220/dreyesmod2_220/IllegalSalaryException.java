package dreyesmod2_220;

public class IllegalSalaryException extends Exception {
	
	public IllegalSalaryException() {
		super();
	}
	
	public IllegalSalaryException(String e) {
		super(e);
	}
	
}
