package dreyesmod2_220;

import java.awt.FileDialog;
import java.awt.Frame;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Scanner;
import java.util.regex.Pattern;

public class StateCapitals {

	public static Scanner data = null;
	static Map< String, String > mapMap = new HashMap< String, String >(); 
	public static Scanner scan = new Scanner(System.in);
	public static int total = 0;
	public static int correct = 0;
	public static int wrong = 0;
	
	
	public static String pickLocRead() {
		Frame f = new Frame();
		// decide from where to read the file
		FileDialog fiBox = new FileDialog(f, "Pick location for reading your file", FileDialog.LOAD);
		System.out.println("The dialog box might appear behind Eclipse.  " + 
			      "\n   Choose where you would like to read for your data.");
		fiBox.setVisible(true);
		// get the absolute path to the file
		String fiName = fiBox.getFile();
		String dirPath = fiBox.getDirectory();

		// create a file instance for the absolute path
		String name = dirPath + fiName;
		return name;
	}
	
	public static void openFile() {
		try {
			File file = new File(pickLocRead());
			//File file = new File("/home/user/QubesIncoming/College/statecapitals.txt");
			data = new Scanner(file);
			data.useDelimiter(" - ");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	public static void readRecords() {
		try {
			while (data.hasNextLine()) {
				String State = data.next();
				String City = data.nextLine().replaceAll(" - ", "");
				
				mapMap.put(State, City);
			}
		} catch (NoSuchElementException elementException) {
			System.err.println("File improperly formed. Terminating.");
		} catch (IllegalStateException stateException) {
			System.err.println("Error reading from file. Terminating.");
		}
	   }
	
	public static void closeFile() {
		if (data != null)
			data.close();
	}
	
	public static boolean question(String x, String y) {
		boolean again = true;
		while (again) {
			System.out.println( "The capital of " + x + " is??");
			String answer = scan.nextLine();
			
			if (answer.toLowerCase().equals(y.toLowerCase())) {
				correct += 1;
				total += 1;
				System.out.println("Correct!! It's definitly: " + y);
				again = false;
			} else {
				total += 1;
				wrong += 1;
				System.out.println("Sorry, nope\n Try again? (y)es / (n)o / (e)xit");
				char tempagain = scan.nextLine().toLowerCase().charAt(0);
				
				switch (tempagain) {
				case 'y':
					again = true;
					break;
				case 'n':
					again = false;
					break;
				case 'e':
					again = false;
					return false;
				default:
					again = false;
					return false;
				}
			}
		}
		return true;
	}
	
	public static void Score() {
		System.out.println("Total questions answered: " + total
				+ "\nTotal correct: " + correct
				+ "\nTotal wrong: " + wrong);
	}
	
	
	public static void main(String[] args) {
		openFile();
		readRecords();
		
		List keys = new ArrayList(mapMap.keySet());
		Collections.shuffle(keys);
		for (int i=0;i<keys.size();i++) {
			String current = (String)keys.get(i);
			if (!question(current, mapMap.get(current))) {
				Score();
				System.exit(0);
			}
		}
	}

}
