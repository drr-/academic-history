package dreyesmod2_220;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SentencePlay {
	
	public static Scanner scan = new Scanner(System.in);
	
	
	public static boolean isCaseSensitive() {
		char iChoice = 'z';
		System.out.println("Now when I split it, do you want case sensitivity? (y/n)");
		
		iChoice = scan.nextLine().charAt(0);
		
		if (iChoice == 'y') {
			return true;
		} else {
			return false;
		}
	}
	
	public static String cRegex(String x, String y, boolean z) {
		if (z) { 
			Pattern pattern = Pattern.compile(y, Pattern.DOTALL);
			return pattern.matcher(x).replaceAll("");
		} else {
			Pattern pattern = Pattern.compile(y, Pattern.DOTALL);
			return pattern.matcher(x).replaceAll("").toLowerCase();
		}
		
	}
	
	
	public static char menu() {
		char mChoice = 'z';
		
		System.out.println("Pick pick: "
				+ "\n\ta) The sentence printed backwards"
				+ "\n\tb) The list of words in the sentence including duplicates"
				+ "\n\tc) A sorted list of the words (alphabetically)"
				+ "\n\td) A sorted list of words listed backwards (where z comes before a)"
				+ "\n\te) A randomly shuffled list of words"
				+ "\n\tf) The list of words in the sentence alphabetically removing duplicates");
		
		mChoice = scan.nextLine().charAt(0);
		
		return mChoice;
	}
	
	
	
	public static void sentenceBackwards(String x) {
		String[] sB = x.split("");
		
		for (int i = x.length()-1; i >= 0; i--) {
			System.out.print(sB[i]);
		}
		System.out.println("");
	}
	
	public static void listWords(String x, boolean y) {
		System.out.println(cRegex(x, "[^a-zA-Z0-9 ]", y));
	}
	
	public static void listSortedWords(String x, boolean y) {
		String[] newString = cRegex(x, "[^a-zA-Z0-9 ]", y).split(" ");
		
		Arrays.sort(newString, String.CASE_INSENSITIVE_ORDER);
		
		for (int i = 0; i <= newString.length-1; i++) {
			System.out.print(newString[i] + " ");
		}
		System.out.println("");
	}
	
	public static void listSortedWordsBackwards(String x, boolean y) {
		String[] newString = cRegex(x, "[^a-zA-Z0-9 ]", y).split(" ");
		
		Arrays.sort(newString, String.CASE_INSENSITIVE_ORDER);
		
		for (int i = newString.length-1; i >= 0; i--) {
			System.out.print(newString[i] + " ");
		}
		System.out.println("");
	}
	
	public static void rRoulette(String x, boolean y) {
		String[] initString = cRegex(x, "[^a-zA-Z0-9 ]", y).split(" ");
		
		List<String> finalString = Arrays.asList(initString);
		Collections.shuffle(finalString);
		
		for (String e : finalString.toArray(initString))
			System.out.print(e + " ");
		
		System.out.println("");
	}
	
	public static void cleanList(String x, boolean y) {
		String[] initString = cRegex(x, "[^a-zA-Z0-9 ]", y).split(" ");
		ArrayList<String> finalString = new ArrayList<String>();
		
		Arrays.sort(initString, String.CASE_INSENSITIVE_ORDER);
		
		for (int i = 0; i <= initString.length-1; i++) {
			
			if (!finalString.contains(initString[i])) {
				finalString.add(initString[i]);
			}
		}
		
		for (int i = 0; i <= finalString.size()-1; i++) {
			System.out.print(finalString.get(i) + " ");
		}
		
		System.out.println("");
		
	}



	public static void main(String[] args) {
		boolean inLoop = true;
		String sentence = null;
		
		System.out.println("Enter sentence to be modified: ");
		sentence = scan.nextLine();
		
		
		
		while (inLoop) {
			char choice = menu();
			
			switch(choice) {
			case 'a':
				sentenceBackwards(sentence);
				break;
			case 'b':
				listWords(sentence, isCaseSensitive());
				break;
			case 'c':
				listSortedWords(sentence, isCaseSensitive());
				break;
			case 'd':
				listSortedWordsBackwards(sentence, isCaseSensitive());
				break;
			case 'e':
				rRoulette(sentence, isCaseSensitive());
				break;
			case 'f':
				cleanList(sentence, isCaseSensitive());
				break;
			default:
				System.out.println("\nError: '" + choice + "' is not a valid option\n");
			}
		}
		
	}

}
