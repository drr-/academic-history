package checkPointThree;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.regex.Pattern;


/*
 * 
 * 
 * What I did (Damian):
 *  +- Fixed the mysql connection (Had issues connecting)
 *  +- Remade the entire base architecture (Based on my other submission)
 *  +  Created initial Mysql Script
 *  +- Updated Mysql Script/Procs
 *  +  Added the methods (plus procs)
 *  	#5	deleteReservation
 *  	#6	findReservationNumber
 *  	#7	findReservationCustID
 *  	#8	printGrossFlight
 *  	#9	printGrossAll
 *  	#10	findDeletedReservationNumber
 *  	#11	findDeletedReservationCustID
 *  	#15	printFlights
 *  
 *  +  Added try/catch errors for all
 *  
 *  +  Added README.txt
 *  
 *  +- Methods Remade
 *   	#1	findCustomer
 *   	#2	addCustomer
 *   	#3	printPilotSchedule
 *   	#13	printCustomers
 *   	#14	printPilots
 *
 */


public class EAMethodDB {
	public static Scanner scan = new Scanner(System.in);
	static Connection conn = null;
	static Statement stmt = null;
    
	public static Connection createConnection() {

		String user = "itp220";
		String pass = "itp220";
		String name = "Eagles";
		String driver = "com.mysql.jdbc.Driver";
		String url = "jdbc:mysql://localhost:3306/" + name;

		//System.out.println(driver);
		//System.out.println(url);

		try {
			Class.forName(driver).newInstance();
			conn = DriverManager.getConnection(url, user, pass);
			//System.out.println("Connection really is from : " + conn.getClass().getName());
			//System.out.println("Connection successful!");

		} catch (Exception e) {
			e.printStackTrace();
		}
		return conn;
	}
	
	public static void closeConnection() {
		if (conn != null) {
			try {
				conn.close();
				conn = null;
				// stmt.close();
				System.out.println("The connection was successfully closed");
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	
	
	public static void checkConnect() {
		if (conn == null) {
			conn = createConnection();
		}
		if (stmt == null) {
			try {
				stmt = conn.createStatement();
			} catch (SQLException e) {
				System.out.println("Cannot create the statement");
			}

		}
	}
	
	public static void TestTest() {
		checkConnect();
		try {
			String query = "SELECT * FROM customers";
			
			stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			
			while (rs.next()) {
				/*System.out.println(rs.getString(2) + " copyright: " + rs.getInt(4) 
					+ " isbn:" + rs.getString(1) + " edition: " + rs.getInt(3) 
					+ "\n  written by " + rs.getInt(5) + ":" + rs.getString(8) + " " + rs.getString(9) );
				*/
				System.out.println("Customer ID: " + rs.getInt(1) +
					" \n\tName: " + rs.getString(2) +
					" \n\tAddress: " + rs.getString(3) +
					" \n\tPhone Number: " + rs.getString(4) + "\n");
			}
		  } catch (SQLException e) {
			  System.out.println("Cannot create the statement" + e); 
		}
	}
	
	public static int TestInput(String x) {
		int var = -1;
		do {
			try {
				System.out.println(x);
				String tempAns = scan.nextLine();
				
				var = Integer.parseInt(tempAns);
			} catch (NumberFormatException e) {
				System.out.println("Wrong input try again. \n");
			}
		} while (var == -1);
		return var;
	}

	public static void findCustomer() {
		checkConnect();

		boolean found = false;
		
		int id = TestInput("Enter Customer ID: ");
		
		try {
			String query = "SELECT * FROM customers WHERE customers.custID = " + id;
			
			stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			
			while (rs.next()) {
				System.out.println("Customer Information: " +
					" \n\t Customer ID: "+ rs.getInt(1) +
					" \n\tName: " + rs.getString(2) +
					" \n\tAddress: " + rs.getString(3) +
					" \n\tPhone Number: " + rs.getString(4) + "\n");
				found = true;
			}
			
			if (!found)
				System.out.println("Customer was not found\n");
			
		  } catch (SQLException e) {
			  System.out.println("Cannot create the statement" + e); 
		}
		
	}

	public static void addCustomer() {
		checkConnect();
		
		boolean more = true;
		while(more) {
			System.out.println("Customer's name? ");
			String name = scan.nextLine();
			
			System.out.println("Customer's address? ");
			String address = scan.nextLine();
			
			System.out.println("Customer's phone number? ");
			String phone = scan.nextLine();
		
		
			try {
				String query = "INSERT INTO customers (name, address, phone) VALUES ('" + name + "', '" + address + "', '" + phone + "')";
			
				stmt = conn.createStatement();
				int rs = stmt.executeUpdate(query);
			
			
			} catch (SQLException e) {
				System.out.println("Cannot create the statement"); 
			}
			
			System.out.print("Add more? (y/n)  ");
			String sMore = scan.nextLine();
			if (! sMore.equals("y")) {
				more = false;
			}
		}
		
	}

	public static void printPilotSchedule() {
		checkConnect();
		boolean found = false;
		
		int pilotID = TestInput("Enter Pilot ID: ");
		
		try {
			String query = "SELECT * FROM flights,pilots,pilot_flights WHERE flights.flightID = pilot_flights.flightID AND pilot_flights.pilotID = pilots.pilotID AND pilot_flights.pilotID LIKE '" + pilotID + "' ORDER BY flights.flightID;";
			
			stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			
			while (rs.next()) {
				if (found == false)
					System.out.println("Flight Schedule: ");
				
				System.out.println("Flight ID: " + rs.getString(2) +
					" | " + rs.getString(3) +
					" | " + rs.getString(4) +
					" | " + rs.getString(5) +
					" | " + rs.getString(7));
				found = true;
			}
			
			if (!found)
				System.out.println("Pilot was not found\n");
			
		  } catch (SQLException e) {
			  System.out.println("Cannot create the statement" + e); 
		}
		
	}

	public static void createReservation() {
		// TODO Auto-generated method stub
		
	}

	public static void deleteReservation() {
		checkConnect();
		
		int reservationID = TestInput("Enter Reservation ID: ");
		
		try {
			String query = "DELETE FROM reservations WHERE reservations.reservationID =" + reservationID + ";";
		
			stmt = conn.createStatement();
			int rs = stmt.executeUpdate(query);
			
			if (rs == 0)
				System.out.println("Reservation Not Found");
			else
				System.out.println("Reservation was deleted");
		
		} catch (SQLException e) {
			System.out.println("Cannot create the statement"); 
		}
		
	}

	public static void findReservationNumber() {
		checkConnect();
		boolean found = false;
		
		int reservationID = TestInput("Enter Reservation ID: ");
		
		try {
			String query = "SELECT * FROM flights,reservations,reservation_seats,customers,customer_reservations WHERE flights.flightID = reservations.flightID AND reservations.reservationID = reservation_seats.reservationID AND reservations.reservationID = customer_reservations.reservationID AND customers.custID = customer_reservations.custID AND "
					+ "reservations.reservationID LIKE '" + reservationID + "' AND reservations.deleted LIKE '0' ORDER BY reservations.reservationID;";
			
			stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			
			while (rs.next()) {
				if (found == false)
					System.out.println("Reservation Information: ");
				
				System.out.println("Reservation ID: " + rs.getString(11) +
					" \n\t " +rs.getString(14) +
					" has a reservation for flight " + rs.getString(2) +
					" and thier seat numbers are " + rs.getString(12));
				found = true;
			}
			
			if (!found)
				System.out.println("Reservation was not found\n");
			
		  } catch (SQLException e) {
			  System.out.println("Cannot create the statement" + e); 
		}
		
	}

	public static void findReservationCustID() {
		checkConnect();
		boolean found = false;
		
		int customerID = TestInput("Enter Customer ID: ");
		
		try {
			String query = "SELECT * FROM flights,reservations,reservation_seats,customers,customer_reservations WHERE flights.flightID = reservations.flightID AND reservations.reservationID = reservation_seats.reservationID AND reservations.reservationID = customer_reservations.reservationID AND customers.custID = customer_reservations.custID AND "
					+ "customers.custID LIKE '" + customerID + "' AND reservations.deleted LIKE '0' ORDER BY reservations.reservationID;";
			
			stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			
			while (rs.next()) {
				if (found == false)
					System.out.println("Reservation Information: ");
				
				System.out.println("Reservation ID: " + rs.getString(11) +
					" \n\t " +rs.getString(14) +
					" has a reservation for flight " + rs.getString(2) +
					" and thier seat numbers are " + rs.getString(12));
				found = true;
			}
			
			if (!found)
				System.out.println("Reservation was not found\n");
			
		  } catch (SQLException e) {
			  System.out.println("Cannot create the statement" + e); 
		}
		
	}

	public static void printGrossFlight() {
		checkConnect();
		NumberFormat nf = NumberFormat.getCurrencyInstance();
		int Total = 0;
		
		int flightID = TestInput("Enter Flight ID: ");
		
		try {
			String query = "SELECT * FROM reservations WHERE reservations.deleted LIKE '0' AND reservations.flightID = '" + flightID + "' ORDER BY reservations.reservationID;";
			
			stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			
			while (rs.next()) {
				String SeatType = rs.getString(2);
				int Price = 0;
				
				if (SeatType.equalsIgnoreCase("F")) {
					Price = 850;
				} else {
					Price = 450;
				}
				
				Total += Price;
			}
			
			System.out.println("The total income for this flight is: " + nf.format(Total));
			
		  } catch (SQLException e) {
			  System.out.println("Cannot create the statement" + e); 
		}
		
	}

	public static void printGrossAll() {
		checkConnect();
		NumberFormat nf = NumberFormat.getCurrencyInstance();
		
		int Total = 0;
		
		try {
			String query = "SELECT * FROM reservations WHERE reservations.deleted LIKE '0' ORDER BY reservations.reservationID;";
			
			stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			
			while (rs.next()) {
				String SeatType = rs.getString(2);
				int Price = 0;
				
				if (SeatType.equalsIgnoreCase("F")) {
					Price = 850;
				} else {
					Price = 450;
				}
				
				Total += Price;
			}
			
			System.out.println("The total income for all flights is currently: " + nf.format(Total));
			
		  } catch (SQLException e) {
			  System.out.println("Cannot create the statement" + e); 
		}
	}

	public static void findDeletedReservationNumber() {
		checkConnect();
		boolean found = false;
		
		int reservationID = TestInput("Enter Reservation ID: ");
		
		try {
			String query = "SELECT * FROM flights,reservations,reservation_seats,customers,customer_reservations WHERE flights.flightID = reservations.flightID AND reservations.reservationID = reservation_seats.reservationID AND reservations.reservationID = customer_reservations.reservationID AND customers.custID = customer_reservations.custID AND "
					+ "reservations.reservationID LIKE '" + reservationID + "' AND reservations.deleted LIKE '1' ORDER BY reservations.reservationID;";
			
			stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			
			while (rs.next()) {
				if (found == false)
					System.out.println("Reservation Information: ");
				
				System.out.println("Reservation ID: " + rs.getString(11) +
					" \n\t " +rs.getString(14) +
					" has a reservation for flight " + rs.getString(2) +
					" and thier seat numbers are " + rs.getString(12));
				found = true;
			}
			
			if (!found)
				System.out.println("Reservation was not found\n");
			
		  } catch (SQLException e) {
			  System.out.println("Cannot create the statement" + e); 
		}
		
	}

	public static void findDeletedReservationCustID() {
		checkConnect();
		boolean found = false;
		
		int customerID = TestInput("Enter Customer ID: ");
		
		try {
			String query = "SELECT * FROM flights,reservations,reservation_seats,customers,customer_reservations WHERE flights.flightID = reservations.flightID AND reservations.reservationID = reservation_seats.reservationID AND reservations.reservationID = customer_reservations.reservationID AND customers.custID = customer_reservations.custID AND "
					+ "customers.custID LIKE '" + customerID + "' AND reservations.deleted LIKE '1' ORDER BY reservations.reservationID;";
			
			stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			
			while (rs.next()) {
				if (found == false)
					System.out.println("Reservation Information: ");
				
				System.out.println("Reservation ID: " + rs.getString(11) +
					" \n\t " +rs.getString(14) +
					" has a reservation for flight " + rs.getString(2) +
					" and thier seat numbers are " + rs.getString(12));
				found = true;
			}
			
			if (!found)
				System.out.println("Reservation was not found\n");
			
		  } catch (SQLException e) {
			  System.out.println("Cannot create the statement" + e); 
		}
		
	}

	public static void printSeats() {
		// TODO Auto-generated method stub
		
	}
	
	public static void printCustomers() {
		checkConnect();
		try {
			String query = "SELECT * FROM customers";
			
			stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			
			while (rs.next()) {
				System.out.println("Customer ID: " + rs.getInt(1) +
					" \n\tName: " + rs.getString(2) +
					" \n\tAddress: " + rs.getString(3) +
					" \n\tPhone Number: " + rs.getString(4) + "\n");
			}
		  } catch (SQLException e) {
			  System.out.println("Cannot create the statement" + e); 
		}
		
	}

	public static void printPilots() {
		checkConnect();
		try {
			String query = "SELECT * FROM pilots";
			
			stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			
			while (rs.next()) {
				System.out.println("Pilot ID: " + rs.getInt(1) +
					" \n\tName: " + rs.getString(2) +
					" \n\tAddress: " + rs.getString(3) +
					" \n\tPhone Number: " + rs.getString(4) + "\n");
			}
		  } catch (SQLException e) {
			  System.out.println("Cannot create the statement" + e); 
		}
		
	}

	public static void printFlights() {
		checkConnect();
		try {
			String query = "SELECT * FROM flights";
			
			stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			
			while (rs.next()) {
				System.out.println("Flight ID: " + rs.getInt(1) +
					" \n\tCode: " + rs.getString(2) +
					" \n\tDate: " + rs.getInt(3) +
					" \n\tRoute: " + rs.getString(3) +
					" \n\tTime: " + rs.getString(4) + "\n");
			}
		  } catch (SQLException e) {
			  System.out.println("Cannot create the statement" + e); 
		}
		
	}
	
	
	
	
	
	
	
	
	/*
	
	public static void printBooks() {
		
		checkConnect();
				
		try {
			String query = "SELECT * FROM titles,isbn,authors WHERE titles.isbn = isbn.isbn AND authors.authorID = isbn.authorID ORDER BY isbn.isbn";
			
			stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			
			while (rs.next()) {
				System.out.println(rs.getString(2) + " copyright: " + rs.getInt(4) 
					+ " isbn:" + rs.getString(1) + " edition: " + rs.getInt(3) 
					+ "\n  written by " + rs.getInt(5) + ":" + rs.getString(8) + " " + rs.getString(9) );
			}
		  } catch (SQLException e) {
			  System.out.println("Cannot create the statement" + e); 
		}
	}
	
	public static void printAuthors() {
		checkConnect();
		
		try {
			String query = "SELECT * FROM authors";
			
			stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			
			while (rs.next()) {
				System.out.println(rs.getInt(1) + ":" + rs.getString(2) + " " +rs.getString(3) );
			}
		  } catch (SQLException e) {
			  System.out.println("Cannot create the statement"); 
		  }
	}
	public static void getPaul() {
		checkConnect();
		
		try {
			String query = "SELECT * FROM titles,isbn,authors WHERE titles.isbn = isbn.isbn AND authors.authorID = isbn.authorID AND authors.lastName LIKE '%Goldschmidt%' AND authors.firstName LIKE '%Paul%' ORDER BY isbn.isbn";
			
			stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			
			while (rs.next()) {
				System.out.println(rs.getString(2) + " copyright: " + rs.getInt(4) 
				+ " isbn:" + rs.getString(1) + " edition: " + rs.getInt(3) 
				+ "\n  written by " + rs.getInt(5) + ":" + rs.getString(8) + " " + rs.getString(9) );
			}
		  } catch (SQLException e) {
			  System.out.println("Cannot create the statement"); 
		  }
	}
	
	public static void getBaseball() {
		checkConnect();
		
		try {
			String query = "SELECT * FROM titles,isbn,authors WHERE titles.isbn = isbn.isbn AND authors.authorID = isbn.authorID AND titles.title LIKE '%Baseball%' ORDER BY isbn.isbn;";
			
			stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			
			while (rs.next()) {
				System.out.println(rs.getString(2) + " copyright: " + rs.getInt(4) 
				+ " isbn:" + rs.getString(1) + " edition: " + rs.getInt(3) 
				+ "\n  written by " + rs.getInt(5) + ":" + rs.getString(8) + " " + rs.getString(9) );
			}
		  } catch (SQLException e) {
			  System.out.println("Cannot create the statement"); 
		  }
	}
	
	public static void getWord() {
		checkConnect();
		
		System.out.println("What word are you looking for?");
		String SearchTerm = scan.nextLine();
		
		
		
		if (conn == null) {
			System.out.println("A connection does not exist");
		}
		
		try {
			String query = "SELECT * FROM titles,isbn,authors WHERE titles.isbn = isbn.isbn AND authors.authorID = isbn.authorID AND titles.title LIKE '%" + SearchTerm + "%' ORDER BY isbn.isbn;";
			
			stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			
			while (rs.next()) {
				System.out.println(rs.getString(2) + " copyright: " + rs.getInt(4) 
				+ " isbn:" + rs.getString(1) + " edition: " + rs.getInt(3) 
				+ "\n  written by " + rs.getInt(5) + ":" + rs.getString(8) + " " + rs.getString(9) );
			}
		  } catch (SQLException e) {
			  System.out.println("Cannot create the statement"); 
		  }
	}
	
	public static void addBook() {
		
		checkConnect();
		
		
		boolean moreBooks = true;
		
		while(moreBooks) {
			ArrayList<Integer> authLoc = new ArrayList<>();
			//ArrayList<Author> tempAuth = new ArrayList<Author>();
			
			
			
			System.out.println("Title of book?");
			String titleBook = scan.nextLine();
			
			System.out.println("Copyright year?");
			int copyrightYear = scan.nextInt();
			
			System.out.println("Edition number?");
			int editionNumber = scan.nextInt();
						
			boolean moreAuthors = true;
			while (moreAuthors) {
				System.out.println("Author's first name?");
				String firstName = scan.next();
				
				System.out.println("Author's last name?");
				String lastName = scan.next();
				
				
				try {
					String query = "INSERT IGNORE INTO authors (firstName, lastName) VALUES ('" + firstName + "', '" + lastName + "')";
					
					stmt = conn.createStatement();
					int rs = stmt.executeUpdate(query);
					
					
				  } catch (SQLException e) {
					  System.out.println("Cannot create the statement"); 
				  }
				
				
				try {
					String query = "SELECT * FROM authors ORDER BY authorID DESC LIMIT 1";
					
					stmt = conn.createStatement();
					ResultSet rs = stmt.executeQuery(query);
					
					while (rs.next()) {
						authLoc.add(rs.getInt(1));
						rs.getString(2);
						rs.getString(3);
						System.out.println( );
					}
				  } catch (SQLException e) {
					  System.out.println("Cannot create the statement");  
				  } 
				
				
				
				System.out.println("More authors? (true/false)");
				moreAuthors = scan.nextBoolean();
			}
			System.out.println("Lastly, ISBN?");
			String isbnA = scan.next();
			
			
			
			try {
				String query = "INSERT INTO titles (isbn,title,editionNumber,copyright) VALUES ('" + isbnA + "', '" + titleBook + "', " + editionNumber + ", '" + copyrightYear +"')";
				
				stmt = conn.createStatement();
				int rs = stmt.executeUpdate(query);
				
				
			  } catch (SQLException e) {
				  e.printStackTrace();
				  System.out.println("Cannot create the statement" + e); 
			  }
			
			for (int i = 0; i<authLoc.size(); i++) {
				try {
					String query = "INSERT INTO isbn (authorID, isbn) VALUES (" + authLoc.get(i) + ", '" + isbnA + "')";
					
					stmt = conn.createStatement();
					int rs = stmt.executeUpdate(query);
					
					
				  } catch (SQLException e) {
					  System.out.println("Cannot create the statement"); 
				  }
			}
			
			System.out.println("Add more books? (true/false)");
			moreBooks = scan.nextBoolean();
		}
	}
	
	*/

}
