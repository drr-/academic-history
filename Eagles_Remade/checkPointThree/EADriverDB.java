package checkPointThree;

import java.awt.GraphicsEnvironment;
import java.awt.Rectangle;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Scanner;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;

public class EADriverDB extends JPanel {
	
	private JButton jcomp1;
    private JButton jcomp2;
    private JButton jcomp3;
    private JButton jcomp4;
    private JButton jcomp5;
	
	public EADriverDB() {
		
		GraphicsEnvironment environment = GraphicsEnvironment.getLocalGraphicsEnvironment();
	    Rectangle bounds = environment.getMaximumWindowBounds();
	    System.out.println("Screen Bounds = " + bounds);
	    
	    int newFrameHeight = (int) (bounds.height - (bounds.height * 0.20));
	    int newFrameWidth = (int) (bounds.width - (bounds.width * 0.20));
		
        //construct components
        jcomp1 = new JButton ("Center");
        jcomp2 = new JButton ("South");
        jcomp3 = new JButton ("North");
        jcomp4 = new JButton ("East");
        jcomp5 = new JButton ("West");
        
        jcomp1.setToolTipText ("ToolTip");
        jcomp1.action(null, bounds);
        
        //adjust size and set layout
        setPreferredSize (new Dimension (newFrameWidth, newFrameHeight));
        BorderLayout layout = new BorderLayout(0, 0);
        setLayout (layout);

        //add components
        add (jcomp1, BorderLayout.CENTER);
        add (jcomp2, BorderLayout.SOUTH);
        add (jcomp3, BorderLayout.NORTH);
        add (jcomp4, BorderLayout.EAST);
        add (jcomp5, BorderLayout.WEST);
        
    }

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		
		
		JFrame jframe = new JFrame("Main Screen");   //create JFrame object

	    jframe.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

	    //jframe.setSize(newFrameWidth,newFrameHeight);         //set size of GUI screen
	    
	    //JButton pressButton = new JButton("Press");  //create JButton object
        jframe.getContentPane().add(new EADriverDB());

        jframe.pack();
	    jframe.setVisible(true);
		
       // jframe.getContentPane().add(new EADriverDB());
      
		
		/*
		boolean more = true;
		while(more) {
			int ans = 0;
			do {
				try {
					menu();
					System.out.println("Choice: ");
					String tempAns = scan.nextLine();
					
					ans = Integer.parseInt(tempAns);
				} catch (NumberFormatException e) {
					System.out.println("Wrong input try again.\n");
				}
			} while (ans == 0);
			if (ans == 1)
				EAMethodDB.findCustomer();
			else if (ans == 2)
				EAMethodDB.addCustomer();
			else if (ans==3)
				EAMethodDB.printPilotSchedule();
			else if (ans == 4)
				EAMethodDB.createReservation();
			else if (ans == 5)
				EAMethodDB.deleteReservation();
			else if (ans == 6)
				EAMethodDB.findReservationNumber();
			else if (ans == 7)
				EAMethodDB.findReservationCustID();
			else if (ans == 8)
				EAMethodDB.printGrossFlight();
			else if (ans == 9)
				EAMethodDB.printGrossAll();
			else if (ans == 10)
				EAMethodDB.findDeletedReservationNumber();
			else if (ans == 11)
				EAMethodDB.findDeletedReservationCustID();
			else if (ans == 12)
				EAMethodDB.printSeats();
			else if (ans == 13)
				EAMethodDB.printCustomers();
			else if (ans == 14)
				EAMethodDB.printPilots();
			else if (ans == 15)
				EAMethodDB.printFlights();
			//else if (ans == 99)
				//BaseballMethodDB.TestTest();
			else {
				System.out.println("BYE!!!");
				more = false;
				System.exit(0);
			}
				
			
		}*/
		
	}
	
	public static void menu() {
		System.out.println("Choose one:" +
				"\n\t 1. Find Customer Information" +
				"\n\t 2. Add New Customer" +
				"\n\t 3. Print Pilot Schedule" +
				"\n\t 5. Delete Existing Reservation" +
				"\n\t 6. Find Existing Reservation By Reservation Number" +
				"\n\t 7. Find Existing Reservation By Customer ID" +
				"\n\t 8. Print Gross Income For Specific Flight" +
				"\n\t 9. Print Gross Income For All Scheduled Flights" +
				"\n\t 10. Find Deleted Reservation By Reservation Number" +
				"\n\t 11. Find Deleted Reservation By Customer ID" +
				"\n\t 13. Print out all of the Customer information" +
				"\n\t 14. Print out all of the Pilot information" +
				"\n\t 15. Print out all of the Flight information" +
				"\n\t 999. Exit");
	}
	

}

