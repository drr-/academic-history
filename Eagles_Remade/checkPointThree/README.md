
# Eagles Project - Checkpoint 3

## Basic instructions

```
Default Username:	itp220
Default Password:	itp220
Default Database:	Eagles

```

* Ensure L/XAMP is installed
* Ensure services are running
     * Add user/password to L/XAMPP
* Import submitted JAR
* Add mysqlconnctor to `{project_name} > Properties > 'Java Building Path' > Libraries > Class Path > 'Add External Jar'`
* Import SQL Procs/Script
    * From the webpage: `I have no idea. I did not do this`
    * From the CLI (**After Logging In**): `source {path_to_mysql_procs}`

* Begin program
     * Selecting any of the options will work
     * Connections are made automatically


-----
## Specialized instructions (just for my own fun)
> while very unlikely, here's a Linux checklist:

* Stuff from the other section
* Basic LAMPP Setup
     * Install LAMPP
     * Start GUI from `./opt/lamp/manager-linux-x64.run`
* Basic CLI Login

>> mysql -u itp220 -p -h 127.0.0.1