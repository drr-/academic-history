package com.ass.i3;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class TikTakToe extends JFrame {
    private static final int BOARD_SIZE = 3;
    private static final int TILE_SIZE = 100;

    private char[][] board;
    private char currentPlayer;
    
    
    public String serverurl = "";
    public String zeGame = "TicTacToe";
    public String user = "";
    public String key = "";
    
    public void setAuth (String s, String u, String k) {
    	serverurl = s;
    	user = u;
    	key = k;
    }
    
    private void jsonPost(int score, char eeeeeeeeeee) {
    	System.out.println("Game: "+zeGame+" - Score: "+score);
        //String jsonInputString = "{\"key1\": \"value1\", \"key2\": \"value2\"}";
        //String responseBody = sendPostRequest(serverurl, jsonInputString);
    	//System.out.println("Response Body:\n" + responseBody);
    	
    	String jsonInputString = "{\"Username\":\""+user+"\",\"APIKey\":\""+key+"\",\"Game\":\""+zeGame+"\",\"Player\":\""+eeeeeeeeeee+"\",\"Score\":\""+score+"\"}";
        
        URL apiUrl = null;
        HttpURLConnection connection = null; 

        // Set up the connection for a POST request
        try {
        	apiUrl = new URL(serverurl);
        	connection = (HttpURLConnection) apiUrl.openConnection();
			connection.setRequestMethod("POST");
	        connection.setRequestProperty("Content-Type", "application/json");
	        connection.setDoOutput(true);
		} catch (IOException e) {
			e.printStackTrace();
		}

        try (OutputStream os = connection.getOutputStream()) {
            byte[] input = jsonInputString.getBytes("utf-8");
            os.write(input, 0, input.length);
        } catch (IOException e) {
			e.printStackTrace();
		}

        try (BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream(), "utf-8"))) {
            StringBuilder response = new StringBuilder();
            String responseLine;
            while ((responseLine = br.readLine()) != null) {
                response.append(responseLine.trim());
            }
            System.out.println(response.toString());
        } catch (IOException e) {
			e.printStackTrace();
		} finally {
            connection.disconnect();
        }
    	
    }

    

    public TikTakToe() {
        setTitle("Tic Tac Toe");
        setSize(BOARD_SIZE * TILE_SIZE, BOARD_SIZE * TILE_SIZE);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setLocationRelativeTo(null);
        
        setAuth("http://93.95.228.40:8000/postStats/1", "itech_300", "totalyCoolApiKey");

        initializeBoard();
        addMouseListener(new TicTacToeMouseListener());

        repaint();
    }

    private void initializeBoard() {
        board = new char[BOARD_SIZE][BOARD_SIZE];
        for (int i = 0; i < BOARD_SIZE; i++) {
            for (int j = 0; j < BOARD_SIZE; j++) {
                board[i][j] = ' ';
            }
        }
        currentPlayer = 'X';
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);

        for (int row = 0; row < BOARD_SIZE; row++) {
            for (int col = 0; col < BOARD_SIZE; col++) {
                g.drawRect(col * TILE_SIZE, row * TILE_SIZE, TILE_SIZE, TILE_SIZE);

                char mark = board[row][col];
                if (mark != ' ') {
                    drawMark(g, mark, row, col);
                }
            }
        }
    }

    private void drawMark(Graphics g, char mark, int row, int col) {
        g.setColor(Color.BLACK);
        g.setFont(new Font("SansSerif", Font.PLAIN, TILE_SIZE));
        g.drawString(String.valueOf(mark), col * TILE_SIZE + TILE_SIZE / 4, row * TILE_SIZE + 3 * TILE_SIZE / 4);
    }

    private class TicTacToeMouseListener extends MouseAdapter {
        @Override
        public void mouseClicked(MouseEvent e) {
            int clickedRow = e.getY() / TILE_SIZE;
            int clickedCol = e.getX() / TILE_SIZE;

            if (board[clickedRow][clickedCol] == ' ') {
                board[clickedRow][clickedCol] = currentPlayer;
                checkWinner();
                switchPlayer();
            }

            repaint();
        }

        private void switchPlayer() {
            currentPlayer = (currentPlayer == 'X') ? 'O' : 'X';
        }

        private void checkWinner() {
            for (int i = 0; i < BOARD_SIZE; i++) {
                if (board[i][0] != ' ' && board[i][0] == board[i][1] && board[i][1] == board[i][2]) {
                    announceWinner(board[i][0]);
                    return;
                }
            }
            for (int i = 0; i < BOARD_SIZE; i++) {
                if (board[0][i] != ' ' && board[0][i] == board[1][i] && board[1][i] == board[2][i]) {
                    announceWinner(board[0][i]);
                    return;
                }
            }
            if (board[0][0] != ' ' && board[0][0] == board[1][1] && board[1][1] == board[2][2]) {
                announceWinner(board[0][0]);
                return;
            }

            if (board[0][2] != ' ' && board[0][2] == board[1][1] && board[1][1] == board[2][0]) {
                announceWinner(board[0][2]);
            }
        }

        private void announceWinner(char winner) {
        	jsonPost(1, TikTakToe.this.currentPlayer);
            JOptionPane.showMessageDialog(TikTakToe.this, winner + " wins!", "Game Over", JOptionPane.INFORMATION_MESSAGE);
            initializeBoard();
        }
    }
}
