package dreyesmod2_223;

import java.util.ArrayList;

class MyList<T> {
	T data;
	
	ArrayList<Integer>	disBeIt = new ArrayList<Integer>();
	ArrayList<Double>	disItBe = new ArrayList<Double>();

	public void add(int x) {
		disBeIt.add(x);
	}

	public void add(double x) {
		disItBe.add(x);
	}

	public String largest() {
		Double lOne = -999999.0;
		int lTwo = -999999;
		
		
		if (disBeIt.size() > disItBe.size()) {
			for (int counter = 0; counter < disBeIt.size(); counter++) {
				if (lTwo < disBeIt.get(counter)) {
					lTwo = disBeIt.get(counter);
					
				}
			}
			return String.valueOf(lTwo);
		} else {
			for (int counter = 0; counter < disItBe.size(); counter++) { 
				if (lOne < disItBe.get(counter)) {
					lOne = disItBe.get(counter);
				}
			}
			return String.valueOf(lOne);
		}
	}

	public String smallest() {
		Double lOne = 999999.0;
		int lTwo = 999999;
		
		
		if (disBeIt.size() > disItBe.size()) {
			for (int counter = 0; counter < disBeIt.size(); counter++) {
				if (lTwo > disBeIt.get(counter)) {
					lTwo = disBeIt.get(counter);
					
				}
			}
			return String.valueOf(lTwo);
		} else {
			for (int counter = 0; counter < disItBe.size(); counter++) { 
				if (lOne > disItBe.get(counter)) {
					lOne = disItBe.get(counter);
				}
			}
			return String.valueOf(lOne);
		}
	}
}
