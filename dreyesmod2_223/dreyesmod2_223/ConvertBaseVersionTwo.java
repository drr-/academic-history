package dreyesmod2_223;

import java.util.Scanner;

public class ConvertBaseVersionTwo {
	

	static Scanner scan = new Scanner(System.in);
	
	
	private static final char[] CHARS = "0123456789abcdefghijklmnopqrstuvwxyz".toCharArray();

	private static String convertIntToBase(int i, int base){
	    final StringBuilder builder = new StringBuilder();
	    do{
	        builder.append(CHARS[i % base]);
	        i /= base;
	    } while(i > 0);
	    return builder.reverse().toString();
	}
	
	
	public static int recursive(int x, int y, int z) {

		if (x == 0) {
			return Integer.parseInt(convertIntToBase(y, z));
		} else if (x != 0) {
			return recursive(x-x, (Integer.parseInt(convertIntToBase(x, y))), z);
			
		}
		
		return recursive(x-x, y, z);
	}
	
	
	public static int getInt(String x) {
		System.out.println(x);
		
		int var = 0;
		
		boolean momo = true;
		do {
			try {
				String tempAns = scan.nextLine();
				
				var = Integer.parseInt(tempAns);
				momo = false;
			} catch (NumberFormatException e) {
				System.out.println("Wrong input try again. \n");
			}
		} while (momo);
		
		return var;
	}

	public static void main(String[] args) {
		
		boolean momo = true;
		do {
			int og = getInt("Enter any number: ");
			int ob = getInt("Enter original Base: ");
			int nb = getInt("Enter new Base: ");
			
			System.out.println(og + " base " + ob + " converted to base " + nb + " is " + recursive(og, ob, nb));
			
			System.out.println("Do you want to enter another number? (y/n)");
			String e = scan.nextLine();
			if (!e.equalsIgnoreCase("y")) {
				momo = false;
			}
		} while (momo);
		
		
	}

}




/*

		System.out.println("Your value was " + value);
		System.out.println("In binary:  " + Integer.toBinaryString(value));
		System.out.println("In octal:  " + Integer.toOctalString(value));
		System.out.println("In hexadecimal:  " + Integer.toHexString(value));	


 */

