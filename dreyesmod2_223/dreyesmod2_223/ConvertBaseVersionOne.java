package dreyesmod2_223;

import java.util.Scanner;

public class ConvertBaseVersionOne {
	
	static int value = 0;
	static Scanner scan = new Scanner(System.in);
	
	
	public static int getInt(String x) {
		System.out.println(x);
		
		int var = 0;
		
		boolean momo = true;
		do {
			try {
				String tempAns = scan.nextLine();
				
				var = Integer.parseInt(tempAns);
				momo = false;
			} catch (NumberFormatException e) {
				System.out.println("Wrong input try again. \n");
			}
		} while (momo);
		
		return var;
	}
	
	public static String getString(String x) {
		System.out.println(x);
		
		String var = scan.next(); 
		
		return var;
	}
	
	public static long getLong(String x) {
		System.out.println(x);
		
		long var = 0;
		
		boolean momo = true;
		do {
			try {
				String tempAns = scan.nextLine();
				
				var = Long.valueOf(tempAns);
				momo = false;
			} catch (NumberFormatException e) {
				System.out.println("Wrong input try again. \n");
			}
		} while (momo);
		
		return var;
	}
	
	
	private static final char[] CHARS = "0123456789abcdefghijklmnopqrstuvwxyz".toCharArray();

	private static String convertIntToBase(int i, int base){
	    final StringBuilder builder = new StringBuilder();
	    do{
	        builder.append(CHARS[i % base]);
	        i /= base;
	    } while(i > 0);
	    return builder.reverse().toString();
	}
	
	
	public static int convertFromBinary(long num) {
		int decimalNumber = 0, i = 0;
		long remainder;

		while (num != 0) {
			remainder = num % 10;
			num /= 10;
			decimalNumber += remainder * Math.pow(2, i);
			++i;
		}

		return decimalNumber;
	}
	
	
	public static void twoBinaryNumber() {
		long disNoNo = getLong("Enter binary: ");
		int newBase = getInt("Enter New Base: ");
		
		convertFromBinary(disNoNo);
		System.out.println(disNoNo + " converted to base " + newBase + " is " + convertIntToBase(convertFromBinary(disNoNo), newBase));
	}
	
	
	public static void toOctalNumber() {
		String buns = getString("Enter any octal number: ");
		int nomNom = Integer.parseInt(buns, 8);
		
		int monMon = getInt("Enter the base you wish to go to: ");
		
		System.out.println(buns + " octal converted to base " + monMon + " is " + convertIntToBase(nomNom, monMon));
	}
	
	
	

	public static int hexadecimalToDecimal(String hexVal) {
		int len = hexVal.length();
		int base = 1;
		int dec_val = 0;
		
		for (int i = len - 1; i >= 0; i--) {
			if (hexVal.charAt(i) >= '0' && hexVal.charAt(i) <= '9') {
				dec_val += (hexVal.charAt(i) - 48) * base;
				base = base * 16;
			} else if (hexVal.charAt(i) >= 'A' && hexVal.charAt(i) <= 'F') {
				dec_val += (hexVal.charAt(i) - 55) * base;
				base = base * 16;
			}
		}
		return dec_val;
	}
	
	public static void toHexANumber() {
		String RateStarWarsFromOneToTen = getString("Enter any hexadecimal number: ");
		
		int beesAreCool = hexadecimalToDecimal(RateStarWarsFromOneToTen);
		int newBase = getInt("Enter the base you wish to go to: ");
		
		System.out.println(RateStarWarsFromOneToTen + " hexadeciaml converted to base " + newBase + " is " + convertIntToBase(beesAreCool, newBase));
	}
	
	public static int search(int x, int y, int z) {
		
		if (z==1) {
			twoBinaryNumber();
			return 0;
		} else if (z==2) {
			toOctalNumber();
			return 0;
		} else if (z==3) {
			int e = getInt("Enter int: ");
			int a = getInt("Enter new base: ");
			String sports = "- its in the game";
		
			System.out.println(e + " converted to base " + a + " is " + convertIntToBase(e, a));
			return 0;
		} else if (z==4) {
			toHexANumber();
			return 0;
		} else if (x > 5) {
			return 0;
		}
		
		return search(x+1, y, z);
	}
	
	
	

	public static int menu() {
		
		System.out.println("\nPick to which to convert:"
				+ "\n\t1) - Binary	to base"
				+ "\n\t2) - Octal	to base"
				+ "\n\t3) - Decimal	to base"
				+ "\n\t4) - Hexadecimal to base"
				+ "\n\t5) - Exit");
		
		int var = 0;
		
		boolean momo = true;
		do {
			try {
				String tempAns = scan.nextLine();
				
				var = Integer.parseInt(tempAns);
				
				if ((var > 0) && (var < 10)) {
					momo = false;
				}
				
				if (var == 5) {
					System.exit(0);
				}
				
			} catch (NumberFormatException e) {
				System.out.println("Wrong input try again. \n");
			}
		} while (momo);
		
		return var;
	}

	public static void main(String[] args) {
		
		while (true) {
			int z = menu();
		
			search(1, value, z);
		}
	}

}




/*

		System.out.println("Your value was " + value);
		System.out.println("In binary:  " + Integer.toBinaryString(value));
		System.out.println("In octal:  " + Integer.toOctalString(value));
		System.out.println("In hexadecimal:  " + Integer.toHexString(value));	


 */

