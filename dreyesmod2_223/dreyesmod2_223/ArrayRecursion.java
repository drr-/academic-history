package dreyesmod2_223;

import java.util.Arrays;

public class ArrayRecursion {
	
	
	
	int[] intArray = {1,2,3,4,5};
	static int max = -9999;

	public static int reco(int x, int y, int[] z) {
		if (x>y) {
			return -1;
		} else if (x==y) {
			return 0;
		} else {
			if ((x < z.length) && (max < z[x])) {
				max = z[x];
			}
			return z[x] + reco(x+1, z.length, z);
		}
	}
	
	
	
	
	public String toString() {
		System.out.println("Add:\t" + reco(0, intArray.length, intArray));
		System.out.println("Max:\t " + getMax());
		
		return "";
	}
	
	public int[] getIntArray() {
		return intArray;
	}
	
	public void setIntArray(int[] intArray) {
		this.intArray = intArray;
	}
	
	
	public int getMax() {
		return max;
	}
	
	public void setMax(int max) {
		this.max = max;
	}
	
	
}
