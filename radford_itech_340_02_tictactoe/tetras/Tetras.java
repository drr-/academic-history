package tetras;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JLabel;

public class Tetras extends JFrame {

    private JLabel statusbar;

    public Tetras() {
        initGUI();
    }

    private void initGUI() {
        statusbar = new JLabel("Game start!");
        add(statusbar, BorderLayout.SOUTH);

        var board = new Board(this);
        add(board);
        board.start();

        setTitle("Tetras - an OG remake");
        setSize(250, 450);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setLocationRelativeTo(null);
    }

    JLabel getStatusBar() {
        return statusbar;
    }

    public static void main(String[] args) {
        EventQueue.invokeLater(() -> {
            var game = new Tetras();
            game.setVisible(true);
        });
    }
}
