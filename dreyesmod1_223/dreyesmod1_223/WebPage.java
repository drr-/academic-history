package dreyesmod1_223;

import java.awt.FileDialog;
import java.awt.Frame;
import java.io.*;
import java.util.Scanner;

public class WebPage {

	public static Scanner sc = new Scanner(System.in);



	private static String CONE;
	private static String CTWO;
	
	
	
	public static final String RESET = "\033[0m";
	
	public static final String WHITE = "\033[0;37m";
	public static final String BLACK = "\033[0;30m";

	public static final String RED = "\033[0;31m";
	public static final String YELLOW = "\033[0;33m";

	public static final String GREEN = "\033[0;32m";
	public static final String BLUE = "\033[0;34m";

	public static final String PURPLE = "\033[0;35m";
	public static final String CYAN = "\033[0;36m";
	
	
	
	public static void writeFile(String x) {
		Frame f = new Frame();
		// decide from where to read the file
		FileDialog foBox = new FileDialog(f, "Pick location for writing your file", FileDialog.SAVE);
		System.out.println(RED + "Remember to end it in 'html'!!" + RESET + ":) The dialog box might appear behind Eclipse.  " + 
			      "\n   Choose where you would like to write your data.");
		
		
		foBox.setVisible(true);
		foBox.toFront();
		foBox.setAlwaysOnTop(true);
		// get the absolute path to the file
		String foName = foBox.getFile();
		String dirPath = foBox.getDirectory();
		String name = dirPath + foName;
		
		
		PrintWriter outputFile = null;
		try {
			outputFile = new PrintWriter(name);
			
			outputFile.print(x);

		} catch (IOException ioe) {
			System.out.println("IO Exception.  Need to exit");
			System.exit(0);

		} finally {
			// Close the file.
			outputFile.close();
		}
    }
	
	
	
	public static String chooseColor(String x) {
		System.out.println("\nEnter custom color for '" + x + "': ");
		String cColor = sc.nextLine();
		
		switch (cColor.toUpperCase()) {
		case "WHITE": return WHITE;
		case "BLACK": return BLACK;
		case "RED": return RED;
		case "YELLOW": return YELLOW;
		case "GREEN": return GREEN;
		case "BLUE": return BLUE;
		case "PURPLE": return PURPLE;
		case "CYAN": return CYAN;
		default: return RESET;
		}
	}
	
	public static String construct(String x, String y) {
		String top = "\n<html>\n\t<head>\n\t\t<title>Mod - 01</title>\n\t</head>\n<body>\n\t<h1>";
		String mid = "</h1>\n\t<p>";
		String bot = "</p>\n</body>\n</html>\n";
		
		CONE = chooseColor("Name");
		CTWO = chooseColor("Description");
		
		String z = top + x + mid + y + bot;
		writeFile(z);
		z = RED + top + RESET + CONE + x + RESET + BLUE + mid + RESET + CTWO + y + RESET + GREEN + bot + RESET;
		
		return z;
	}
	
	
	
	public static void main(String[] args) {
		
		System.out.println("Enter Name: ");
		String Name = sc.nextLine();
		
		System.out.println("Enter Description: ");
		String Desc = sc.nextLine();
		
		System.out.println("\nColor options: WHITE, BLACK, RED, YELLOW,\n GREEN, BLUE, PURPLE, CYAN, and/or NONE");
		System.out.println("\nOUT: " + construct(Name, Desc));
	}

}
