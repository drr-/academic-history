
package dreyesmod8_csc223;


import java.awt.FileDialog;
import java.awt.Frame;
import java.io.*;
import java.util.Scanner;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Random;

class DecisionTreeApp {
	public static Scanner data = null;
	public static Scanner scan = new Scanner(System.in);
	
	static Random rand = new Random();
	
	static ArrayList<Integer> list = new ArrayList<>();
	static List<String> adV = Arrays.asList("amusingly","angrily","apathetically","assertively","begrudgingly","blissfully","blithely","boisterously","boldly","chillingly","coyly","darkly","dazzlingly","deafeningly","dutifully","eagerly","facetiously","faintly","falteringly","frivolously","gloweringly","greedily","grimly","guiltily","hastily","hungrily","intelligently","kindly","lavishly","lazily","listlessly","masterfully","meagerly","methodically","naively","narrowly","neglectfully","nerve-wrackingly","numbly","offensively","passionately","pleasantly","pointlessly","quickly","rapidly","rashly","secretly","seriously","swiftly","tactfully","teasingly","tenderly","timorously","tragically","underhandedly","vacantly","vividly","weirdly","youthfully","zealously");
	static List<String> adJ = Arrays.asList("acrobatic","adorable","adventurous","bitter","boundless","bright","brilliant","brittle","delirious","diminutive","exultant","filthy","foolhardy","gregarious","intrepid","jocular","joyful","jubilant","keen","kooky","lanky","lazy","limp","lush","luxurious","macabre","magnanimous","mellow","miserable","nimble","nocturnal","opulent","ordinary","ornate","palatial","parsimonious","peevish","picturesque","potent","practical","precious","putrid","questionable","quirky","radiant","raspy","rustic","scornful","scrumptious","silky","sly","spectacular","spider-like","tense","tentacular","thorny","verdant","whimsical","woeful","zesty");
	static List<String> non = Arrays.asList("Aardvark","Addax","Adelie Penguin","African Buffalo","African Elephant","African Forest Elephant","Africanized Bees","African Lions","African Penguin","African Spurred Tortoise","African Wild Dog","Allen&#8217;s Swamp Monkeys","Alligator","Allis Shad","Alpine Ibex","Amazonian Manatee","Amazon River Dolphin","American Badger","American Kestrel","American Oystercatcher Bird","Anaconda Snake","Ants","Aquatic Warbler","Arctic Wolf","Armadillo","Asian Elephant","Asian Lion","Atlantic Puffin","Atlantic Spotted Dolphins","Atlas Beetle","Audubon&#8217;s Shearwater Bird","Australasian Grebe","Australian Dingo","Australian Pelican","Australian Swiftlet","Axolotl","Bactrian Camels","Badger","Baltimore Oriole","Bean Goose","Bees","Beetle","Bengal Tigers","Betta Fish (Siamese Fighting Fish)","Big Eyed Squirrel Fish","Bigfin Reef Squid","Bigfin Squid","Bison","Black Caimans","Black Footed Ferret","Black House Spider","Black Mamba Snakes","Black Necked Stilt","Black Rhinoceros","Blacktip Reef Shark","Blacktip Shark","Black Widow Spider","Blister Beetle","Blue and Yellow Macaw","Blue-footed Booby Bird","Blue Ringed Octopus","Blue Whale","Boa Constrictor Snake","Bottlenose Dolphins","Bowhead Whale","Brandling Worms","Brazilian Wandering Spider","British Mice","British Moles","British Water Vole","British Wild Cats","Broad-Snouted Caimans","Brown Recluse Spider","Brown Trout","Bull Shark","Bumble Bees","Burying Beetle","Butterflies","Cactus Wren","Camel Cricket","Camels","Camel Spider","Campbell&#8217;s Dwarf Hamster","Canada Goose","Canadian Marble Fox","Cape Gannet Bird","Capercaillie","Capuchin Monkeys","Capybara","Caracal","Cardinal Birds","Caribbean Reef Shark","Caribou (Reindeer)","Cassowary","Cats","Centipede","Cheetah","Chickaree","Chickens","Chimpanzee","Chinchilla","Chinese Hamsters","Chinstrap Penguin","Chipmunk","Christmas Beetle","Cicada Killer Wasps","Click Beetle","Colossal Squid","Commerson Dolphin","Common Buzzard","Common Dolphin","Common Frog","Common Hippopotamus","Common Kingfisher","Common Lizard","Common Newt","Common Octopus","Common Palm Civet","Common Seal","Common Toad","Common Wasps","Cone Snail","Cooper&#8217;s Hawk","Copepods","Copperhead Snakes","Corncrake","Cottonmouth Snakes","Cows","Coyote","Crab Plover","Crabs","Crocodile","Cuban Solenodon","Cuviers Dwarf Caimans","Darkling Beetle","De Brazza&#8217;s Monkeys","Deer","Degu","Dik Dik","Dog","Dolphins","Domestic Pig","Dragonfly","Dromedary Camels","Drywood Termites","Duck-billed Platypus","Ducks","Dung Beetle","Earthworms","Eastern Lowland Gorillas","Eastern Wolf","Echidna","Elephants","Elephant Seal","Elephant Shrew","Elk","Emperor Penguin","Emu","Erect-Crested Penguin","Ethiopian Wolf","Eurasian Beaver","Eurasian Eagle Owl","Eurasian Water Shrew","Eurasian Wolf","Eurasian Wryneck","European Hare","European Otter","False Black Widow Spider","Fennec Fox","Ferret","Field Vole","Fiji Blue Devil Damsel Fish","Fiordland Crested Penguin","Flatworms","Fungus Beetle","Funnel Web Spider","Galapagos Barn Owls","Galapagos Black Tipped Shark","Galapagos Brown Noddy Tern","Galapagos Brown Pelican Birds","Galapagos Brydes Whale","Galapagos Dark Billed Cuckoo","Galapagos Dove","Galapagos Eels","Galapagos Finches","Galapagos Fin Whales","Galapagos Flamingo","Galapagos Flightless Cormorant Sea Bird","Galapagos Flycatcher Bird","Galapagos Fur Seals","Galapagos Gecko","Galapagos Giant Tortoise","Galapagos Great Blue Heron Bird","Galapagos Great Frigate Bird","Galapagos Green Sea Turtle","Galapagos Hammerhead Shark","Galapagos Hawk","Galapagos Hoary Bat","Galapagos Humpback Whale","Galapagos Killer Whale &#8211; The Orca","Galapagos Land Iguana","Galapagos Lava Gull Bird","Galapagos Lava Heron Bird","Galapagos Lava Lizard","Galapagos Marine Iguana","Galapagos Masked Booby Bird","Galapagos Minke Whale","Galapagos Mockingbird","Galapagos Nazca Booby Bird","Galapagos Penguin","Galapagos Rail Bird","Galapagos Rays","Galapagos Red Bat","Galapagos Red Billed Tropic Bird","Galapagos Red Footed Booby Bird","Galapagos Rice Rat","Galapagos Sea Lions","Galapagos Short Eared Owl","Galapagos Short Finned Pilot Whale","Galapagos Silky Shark","Galapagos Smooth Billed Ani Bird","Galapagos Snakes","Galapagos Sperm Whale","Galapagos Storm Petrel Bird","Galapagos Swallow Tailed Gull","Galapagos Waved Albatross","Galapagos Whale Shark","Galapagos Whimbrel Birds","Galapagos White Cheeked Pintail Duck","Galapagos White Tipped Reef Shark","Galapagos Willet Bird","Galapagos Yellow-Crowned Night Heron","Galapagos Yellow Warbler Bird","Gemsbok","Gentoo Penguin","Gerenuk","Gharial","Giant Anteater","Giant Grouper","Giant Pacific Octopus","Giant Panda Bear","Giant Sable Antelope","Giant Squid","Giraffe","Goats","Golden Eagle","Golden Pheasant","Gold, Silver and Blue Monkeys","Gorillas","Grasshopper","Grass Spiders","Gray Fox","Great Barracuda","Great Bittern","Great Crested Newt","Greater Flamingo","Greater Horseshoe Bat","Greater Mouse-Eared Bat","Greater Spot-Nosed Monkeys","Great Green Macaw","Great Grey Owl","Great White African Pelican Birds","Great White Shark","Green Caterpillars","Green Cheeked Conure","Green Crabs","Green Scarab Beetle","Green Tree Python","Grevy&#8217;s Zebra","Grey Jungle Fowl Bird","Grey Loerie bird","Grey Partridge","Grey Seal","Grey Wolf","Grivet Monkeys","Grizzly Bear","Ground Beetle","Ground Centipede","Groundhog","Guinea Pigs","Hamerkop Bird","Hamsters","Harbour Porpoise","Harp Seal","Hazel Dormouse","Hectors Dolphin","Hedgehog","Hermit Crabs","Heron","Hippopotamus","Hobo Spider","Honey Bee","Honey Bee Characteristics","Hoopoe Birds","Hornets","Horses","Horseshoe Crabs","House Centipede","Howler Monkeys","Humboldt Penguin","Huntsman Spider","Hydrozoan Jellyfish","Ibisbill Bird","Impala","Indian Cuckoo Bird","Indian Rhinoceros","Indian Skimmer Bird","Italian Wolf","Jackal","Jaguar","Jambu Fruit Dove","Japanese Spider Crabs","Javan Rhinoceros","Jellyfish","Katipo Spider","Kea Parrot","Kestrel Bird","Killdeer","King Cobra Snakes","King Penguin","King Salmon","Kinkajou","Kittens","Koala","Komodo Dragon","Krill","Ladybird / Ladybug","Laughing Kookaburra","Lemmings","Lemur","Leopard Seal","Leopard Tortoise","Lion","Little Fairy Penguin","Little Owl  (Athene noctua) Facts","Lizards","Llama","Long Eared Owl","Longhorn Cowfish","Long Horned Beetle","Lynx","Macaque Monkeys","Macaroni Penguin","Magellanic Penguin","Magpie","Manatee","Maned Wolf","Mantis Shrimp","Manx Shearwater Bird","Marine Angelfish","Markhor","Masai Giraffe","Masked Finfoot Bird","Mealy Parrot","Meerkat","Mexican Wolf","Mice","Military Macaw Parrot","Milkweed Beetle","Millipedes","Mongoose","Monkeys","Moorhen","Moose","Moray Eel","Morpho Butterfly","Moth","Mottled Owl Facts","Mountain Goat","Mountain Gorillas","Mountain Zebra","Mourning Dove","Mouse Spider","Mud Dauber Wasp","Musk Ox","Natterjack Toad","New World Monkeys","New World Tarantula Spider","North American Beaver","Northern Mockingbird","Northern Short-tailed Shrew","Nurse Shark","Ocelot","Okapi","Old World Monkeys","Old World Tarantula Spiders","Opossums","Orangutan","Orb Weaver Spiders","Oryx (Antelope)","Osprey","Ostrich Birds","Pantropical Spotted Dolphins","Paper Wasp","Parasitic Bees","Peafowl Peacock","Peccary (Javelina)","Penguin","Peregrine Falcon","Peruvian Fox","Pigs","Pine Marten","Piranha Fish","Plains Zebra","Poison Arrow Frogs","Poisonous Caterpillars","Polar Bear","Polecat","Pollan Fish","Porcupine","Possum","Potter Wasps","Praying Mantis","Preuss&#8217;s Monkey","Pygmy Hippopotamus","Quagga Zebra","Quokka","Quolls","Rabbits","Raccoon","Raccoon Dog","Rainbow Bee Eater","Ratel","Rattlesnakes","Red-backed Shrike","Red Back Jumping Spiders","Red British Squirrel","Red Fox","Red Kangaroo","Red Kite","Red Knot","Red Panda","Red Rock Crabs","Red-tailed Hawk","Red Winged Blackbird","Red Wolf","Reticulated Giraffe","Rhinoceros","Rhinoceros Beetle","Right Whales","Risso&#8217;s Dolphin","Roborovski Hamsters","Rockhopper Penguin","Rough-Toothed Dolphins","Royal Penguin","Sacred Ibis Bird","Saki Monkeys","Sally Lightfoot Crab","Sand Lizard","Scarlet Macaw","Schneider&#8217;s Dwarf Caiman","Scottish Crossbill","Seagull","Seahorse","Sea Lion","Seals","Sea Snakes","Sea Urchin","Secretary Bird","Serval","Shark","Sheep","Shoveler","Silvery Gibbon","Six Eyed Sand Spider","Skipper Butterflies","Skylark","Slender Loris","Sloth","Slow Worm","Slug","Snakes","Snares Penguin","Snow Goose","Snow Leopard","Snowshoe Hare","Solitary Bees","Song Thrush","Southern Short-tailed Shrew","Spectacled Bear","Spectacled/Common Caiman","Spectacled Owl Facts","Spider Monkey","Spiders","Spinner Dolphins","Spiny Lobster","Spotted Beetle","Squirrel Monkeys","Stag Beetle","St Andrews Cross Spider","Stellers Sea Cow","Stingless Bees","Stink Bugs","Stoat","Stone Curlew","Stone Dwelling Centipede","Striped Dolphins","Striped Hyena","Sugar Maple Borer Beetle","Sumatran Rhinoceros","Syrian Hamster","Tamarin Monkeys","Tangle Web Spiders","Tapir","Tarantula Spider","Tawny Eagle","Tawny Frogmouth","Tawny Owl","Teira Batfish","The Atlantic Wolffish","The Bald Eagle","The Burrowing Owl","The Harpy Eagle","The Leopard","The Numbat","The Puma Cat","The Skunk","The Slipper Lobster","The Spotted Hyena","Thomson&#8217;s Gazelle","Tiger Shark","Timber Wolf","Titi Monkeys","Toco Toucan","Tortoise","Trapdoor Spider","Tuatara","Tundra Swan","Tundra Wolf","Turkey Vulture","Turtles","Vampire Bat","Vampire Bats","Vendace Fish","Vulture","Wallaby","Walrus","Warthog","Wasps","Water Beetle","Waterbuck","Weasel","Weevil Beetle","West African Manatee","Western Lowland Gorillas","West Indian Manatee","Whales","White Bengal Tiger","White-clawed Crayfish","White Rhino","White Stork Birds","Wild Boar","Wildebeest","Wild Rabbits","Willow Ptarmigan","Winter White Russian Hamster","Wolf Spider","Wolverine","Wolves","Wombat","Worms","X-Ray Fish","Yacare Caimans","Yellow-bellied marmot","Yellow-eyed Penguin","Yellowhammer","Yellow jacket Wasp","Yellow Sac Spider","Zebras");
	
	static public int eoooe = 0;
	static public int Limit = 9;
    
    static DecisionTree newTree;

    /* --------------------------------- */
    /*                                   */
    /*               METHODS             */
    /*                                   */
    /* --------------------------------- */
    
    public static String pickLocRead() {
		Frame f = new Frame();
		// decide from where to read the file
		FileDialog fiBox = new FileDialog(f, "Pick location for reading your file", FileDialog.LOAD);
		System.out.println("The dialog box might appear behind Eclipse.  " + 
			      "\n   Choose where you would like to read for your data.");
		fiBox.setVisible(true);
		// get the absolute path to the file
		String fiName = fiBox.getFile();
		String dirPath = fiBox.getDirectory();

		// create a file instance for the absolute path
		String name = dirPath + fiName;
		return name;
	}
	
	public static void openFile() {
		try {
			File file = new File(pickLocRead());
			//File file = new File("/home/user/eclipse-workspace/dreyesmod8_csc223/dreyesmod8_csc223/ExampleFile.txt");
			data = new Scanner(file);
			//data.useDelimiter(", ");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	public static void readRecords(DecisionTree tree) {
		try {
			while (data.hasNextLine()) {
				
				//disYoYo.add(data.nextLine());
				
				String E = data.nextLine();
				
				String[] tmpArray = E.split(",");
				
				if (tmpArray.length == 3) {
					tree.createRoot(Integer.valueOf(tmpArray[1]), tmpArray[2].replace("\"", ""));
					//System.out.println("R: '" + tmpArray[1] + "' -" + " '" + tmpArray[2].replace("\"", "") + "'");
				} else {
					if (tmpArray[0].toLowerCase().equals("y")) {
						newTree.addYesNode(Integer.valueOf(tmpArray[1]), Integer.valueOf(tmpArray[2]), tmpArray[3].replace("\"", ""));
						//System.out.println("Y: '" + tmpArray[1] + "' - '" + tmpArray[2] + "' - '" + tmpArray[3].replace("\"", "") + "'");
					} else {
						newTree.addNoNode(Integer.valueOf(tmpArray[1]), Integer.valueOf(tmpArray[2]), tmpArray[3].replace("\"", ""));
						//System.out.println("N: '" + tmpArray[1] + "' - '" + tmpArray[2] + "' - '" + tmpArray[3].replace("\"", "") + "'");
						
						if (tmpArray[3].replace("\"", "").equals("Answer: Hehe")) {
							addNoNo(Integer.valueOf(tmpArray[2]));
						}
					}
				}
				
				//System.out.println();
				
			}
			closeFile();
		} catch (NoSuchElementException elementException) {
			System.err.println("File improperly formed. Terminating.");
		} catch (IllegalStateException stateException) {
			System.err.println("Error reading from file. Terminating.");
		}
	   }
    
	public static void closeFile() {
		if (data != null)
			data.close();
	}

    /* MAIN */
    public static void main(String[] args) throws IOException {
    	
    	openFile();
    	
    	//System.out.println(ranran());
    	
    	
        // Create instance of class DecisionTree
        newTree = new DecisionTree();
        
        readRecords(newTree);
        
        //generateTree();
        

        // Output tree
        System.out.println("\nOUTPUT DECISION TREE");
        System.out.println("====================");
        newTree.outputBinTree();

        // Query tree
        queryTree();
    }

    /* GENERATE TREE */
    static void generateTree() {
        System.out.println("\nGENERATE DECISION TREE");
        System.out.println("======================");

        newTree.createRoot(1, "Is it an organism?");
        newTree.addYesNode(1, 11, "Is it tough?");
        newTree.addYesNode(11, 111, "Is it really really tough?");
        newTree.addYesNode(111, 1111, "Is it space tough?");
        newTree.addYesNode(1111, 11111, "Answer: Tardigrade!!!");
        newTree.addNoNode(1111, 11112, "Answer: E. Coli");
        newTree.addNoNode(111, 1112, "Is it in minecraft?");
        newTree.addYesNode(1112, 11121, "Answer: Mountain Goat!");
        newTree.addNoNode(1112, 11122, "Answer: Honey Badger!");
        newTree.addNoNode(11, 112, "Does it have a 'head cracking' rear?");
        newTree.addYesNode(112, 1121, "Answer: Wombat!");
        newTree.addNoNode(112, 1122, "Answer: Armadillo!");
        newTree.addNoNode(1, 12, "Is it a technical device?");
        newTree.addYesNode(12, 121, "Does it eat people?");
        newTree.addYesNode(121, 1211, "Does it have more than one finger?");
        newTree.addYesNode(1211, 12111, "Answer: an Iron Maiden");
        newTree.addNoNode(1211, 12112, "Answer: A catapult");
        newTree.addNoNode(121, 1212, "Was it used by the spartans?");
        newTree.addYesNode(1212, 12121, "Answer: their spears");
        newTree.addNoNode(1212, 121212, "Answer: bacon");
        newTree.addNoNode(12, 131, "Is it used for 'entertainment' purposes?");
        newTree.addYesNode(131, 11, "Answer: Its a phone! Haha, got cha");
        newTree.addNoNode(131, 1312, "WWZ?");
        newTree.addYesNode(1312, 13121, "I kinda am super tired by this point and forgot what im doing?");
        newTree.addYesNode(13121, 11, "Answer: I lied");
        newTree.addNoNode(13121, 12, "Answer: Or did i?");
        newTree.addNoNode(1312, 13122, "Will this End?");
        newTree.addYesNode(13122, 1413123, "Answer: Ok");
        newTree.addNoNode(13122, 1413122, "Answer: Hehe");
        
        //addMoMo(13122, 1413122, 0);
        //addNoNo(1413122, 12345, 54321);
        addNoNo(1413122);
    }
    
    static void addNoNo(int Orig) {
    	System.out.println(eoooe);
    	if (eoooe >= Limit) {
    		int A = 0;
    		
    		int dis = list.size();
    		for (int i = 0; i < dis ; i++) {
    			int yParent = rand.nextInt(100000);
        		int nParent = rand.nextInt(100000);
        		
        		
        		list.add(yParent);
        		list.add(nParent);
            	
            	newTree.addYesNode(list.get(0), yParent, ranran(1));
            	newTree.addNoNode(list.get(0), nParent, ranran(1));
            	
            	
            	list.remove(0);
    		}
        	
        	eoooe++;
    		
    	} else if (eoooe == 0){
    		int yParent = rand.nextInt(100000);
    		int nParent = rand.nextInt(100000);
    		
    		
    		list.add(yParent);
    		list.add(nParent);
        	
        	//System.out.println("P:" + Orig + "\n\tCY:" +yParent+ Orig + "\n\tCN:" +nParent);
        	newTree.addYesNode(Orig, yParent, ranran(0));
        	newTree.addNoNode(Orig, nParent, ranran(0));
        	
        	eoooe++;
        	addNoNo(Orig);
    	} else {
    		int dis = list.size();
    		for (int i = 0; i < dis ; i++) {
    			int yParent = rand.nextInt(100000);
        		int nParent = rand.nextInt(100000);
        		
        		
        		list.add(yParent);
        		list.add(nParent);
            	
            	newTree.addYesNode(list.get(0), yParent, ranran(0));
            	newTree.addNoNode(list.get(0), nParent, ranran(0));
            	
            	
            	list.remove(0);
    		}
    		
        	
        	//System.out.println("P:" + Orig + "\n\tCY:" +yParent+ Orig + "\n\tCN:" +nParent);
        	
        	eoooe++;
        	addNoNo(Orig);
    	}
    }
    
    
    
    static String ranran(int i) {
    	int rndOne = new Random().nextInt(adV.size());
    	int rndTwo = new Random().nextInt(adJ.size());
    	int rndThr = new Random().nextInt(non.size());
    	
    	if (i == 0) {
    		return adV.get(rndOne) + " " + adJ.get(rndTwo) + " " + non.get(rndThr) + "?";
    	} else {
    		return "Answer: " + adV.get(rndOne) + " " + adJ.get(rndTwo) + " " + non.get(rndThr) + "!";
    	}
    }

    /* QUERY TREE */
    static void queryTree() throws IOException {
        System.out.println("\nQUERY DECISION TREE");
        System.out.println("===================");
        newTree.queryBinTree();

        // Option to exit

        optionToExit();
    }

    /* OPTION TO EXIT PROGRAM */
    static void optionToExit() throws IOException {
    	Scanner scan = new Scanner(System.in);
        System.out.println("Exit? (enter \"Yes\" or \"No\")");
        String answer = scan.nextLine();
        if (answer.equals("Yes")) {
            return;
        } else {
            if (answer.equals("No")) {
                queryTree();
            } else {
                System.out.println("ERROR: Must answer \"Yes\" or \"No\"");
                optionToExit();
            }
        }
    }
}