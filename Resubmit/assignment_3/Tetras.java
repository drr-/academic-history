package assignment_3;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JLabel;

public class Tetras extends JFrame {

    private JLabel statusbar;

    public Tetras() {
        initGUI();
    }

    private void initGUI() {
        statusbar = new JLabel("Game start!");
        add(statusbar, BorderLayout.SOUTH);

        var board = new Board(this);
        add(board);
        board.start();
        
        
        board.setAuth("http://93.95.228.40:8000/postStats/1", "itech_300", "totalyCoolApiKey");


        setTitle("Tetras - an OG remake");
        setSize(250, 450);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        setLocationRelativeTo(null);
    }

    JLabel getStatusBar() {
        return statusbar;
    }
}
