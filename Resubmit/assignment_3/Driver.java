package assignment_3;



import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Driver extends JFrame {

    private String[] buttonNames = {"Tetras", "TikTakToe", "Snek"};
    private JTextArea[] textAreas = new JTextArea[3];
    
    public static String serverurl = "http://93.95.228.40:8000/getStats/1";
    public static String user = "itech_300";
    public static String key = "totalyCoolApiKey";
    
    // Game stuff
    //System.out.println("After formatting: " + formattedDate);
    
	private static String gTetras() {
		LocalDateTime myDateObj = LocalDateTime.now();
	    DateTimeFormatter myFormatObj = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
	    String formattedDate = myDateObj.format(myFormatObj);
		
		EventQueue.invokeLater(() -> {
            var game = new Tetras();
            game.setVisible(true);
        });
		
		
		String eee = "Stats:"+formattedDate+"\n"+getStats("Tetras");
		return eee;
	}
	
	private static String gTikTakToe() {
		LocalDateTime myDateObj = LocalDateTime.now();
	    DateTimeFormatter myFormatObj = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
	    String formattedDate = myDateObj.format(myFormatObj);
		
		SwingUtilities.invokeLater(() -> {
        	TikTakToe game = new TikTakToe();
            game.setVisible(true);
        });
		
		String eee = "Stats:"+formattedDate+"\n"+getStats("TicTacToe");
		
		return eee;
	}
	
	private static String gSnek() {
		LocalDateTime myDateObj = LocalDateTime.now();
	    DateTimeFormatter myFormatObj = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
	    String formattedDate = myDateObj.format(myFormatObj);
		
		SwingUtilities.invokeLater(() -> {
            Snek game = new Snek();
            game.setVisible(true);
        });
		
		String eee = "Stats:"+formattedDate+"\n"+getStats("Snek");
		
		return eee;
	}

    public Driver() {
        setTitle("ITECH 300");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        setSize((int) (screenSize.getWidth()-(screenSize.getWidth()*.2)), (int) (screenSize.getHeight()-(screenSize.getHeight()*.2)));

        // Create a panel with a GridLayout
        JPanel mainPanel = new JPanel(new GridLayout(2, 3, 10, 10));

        // Add buttons to the panel with names from the array
        for (int i = 0; i < 3; i++) {
            JButton button = new JButton(buttonNames[i]);
            button.setPreferredSize(new Dimension(mainPanel.getWidth() / 3, mainPanel.getHeight() / 3));
            button.addActionListener(new ButtonClickListener(i));
            mainPanel.add(button);
        }

        // Add text areas to the panel
        for (int i = 0; i < 3; i++) {
            JTextArea textArea = new JTextArea();
            textArea.setPreferredSize(new Dimension(mainPanel.getWidth() / 3, mainPanel.getHeight() / 3));
            textAreas[i] = textArea;
            textArea.setText("---");
            mainPanel.add(new JScrollPane(textArea));
        }

        // Set the layout for the content pane
        getContentPane().setLayout(new BorderLayout());

        // Add the main panel to the content pane
        getContentPane().add(mainPanel, BorderLayout.CENTER);
    }

    private class ButtonClickListener implements ActionListener {

        private int buttonIndex;

        public ButtonClickListener(int buttonIndex) {
            this.buttonIndex = buttonIndex;
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            handleButtonClick(buttonIndex);
        }
    }

    private void handleButtonClick(int buttonIndex) {
        //JOptionPane.showMessageDialog(this, "Button " + (buttonIndex + 1) + " Clicked!");
        

        if (buttonIndex == 0) {
        	updateTextArea(buttonIndex, "");
        	updateTextArea(buttonIndex, gTetras());
        } else if (buttonIndex == 1) {
        	updateTextArea(buttonIndex, "");
        	updateTextArea(buttonIndex, gTikTakToe());
        } else if (buttonIndex == 2) {
        	updateTextArea(buttonIndex, "");
        	updateTextArea(buttonIndex, gSnek());
        }
        
        //updateTextArea(2, "aaa");
        

        
    }

    private void updateTextArea(int textAreaIndex, String msg) {
    	
    	
    	if (msg.equalsIgnoreCase("")) {
    		textAreas[textAreaIndex].setText("");
            textAreas[textAreaIndex].selectAll();
    	} else {
    		textAreas[textAreaIndex].append(msg);
            textAreas[textAreaIndex].selectAll();
    	}
    	
        

//        if (textArea != null) {
//            if (clear) {
//                textArea.setText("");
//            } else {
////                textArea.append(msg); // Append text
//                textArea.setText(textArea.getText()+msg);
//            }
//        }
//        textArea.selectAll();
//        contentPane.repaint();
    }
    
    public static String getStats(String zeGame) {
    	String jsonInputString = "{\"Username\":\""+user+"\",\"APIKey\":\""+key+"\",\"Game\":\""+zeGame+"\",\"Player\":\"-\",\"Score\":\"-\"}";
        
    	String x = "";
    	
        URL apiUrl = null;
        HttpURLConnection connection = null; 

        // Set up the connection for a POST request
        try {
        	apiUrl = new URL(serverurl);
        	connection = (HttpURLConnection) apiUrl.openConnection();
			connection.setRequestMethod("POST");
	        connection.setRequestProperty("Content-Type", "application/json");
	        connection.setDoOutput(true);
		} catch (IOException e) {
			e.printStackTrace();
		}

        try (OutputStream os = connection.getOutputStream()) {
            byte[] input = jsonInputString.getBytes("utf-8");
            os.write(input, 0, input.length);
        } catch (IOException e) {
			e.printStackTrace();
		}

        try (BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream(), "utf-8"))) {
            StringBuilder response = new StringBuilder();
            String responseLine;
            while ((responseLine = br.readLine()) != null) {
                response.append(responseLine.trim());
            }
            String[] tmp = response.toString().split("}");
            
            for (var m = 0; m <= tmp.length-1; m++) {
            	//String[] tmpTwo = tmp[m].split('"');
            	
            	if (tmp.length == 0) { break; }
            	
            	String[] tmpTwo = tmp[m].split("\"");
            	
            	if (tmpTwo.length <= 3) { continue; }
            	
            	x += "Player:"+tmpTwo[15]+" Score:"+tmpTwo[19]+"\n";
            }
            
        } catch (IOException e) {
			e.printStackTrace();
		} finally {
            connection.disconnect();
        }
        
        //System.out.println(jsonInputString);
    	
    	return x;
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(() -> {
            Driver dash = new Driver();
            dash.setVisible(true);
        });
    }
}

