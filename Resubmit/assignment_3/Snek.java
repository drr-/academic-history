package assignment_3;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Random;

import javax.swing.*;

public class Snek extends JFrame implements ActionListener, KeyListener {
    private static final int GRID_SIZE = 20;
    private static final int TILE_SIZE = 20;
    private static final int GAME_SPEED = 100;

    private ArrayList<Point> snake;
    private Point food;
    private char direction;
    
    
    
    
    public String serverurl = "";
    public String zeGame = "Snek";
    public String user = "";
    public String key = "";
    
    public void setAuth (String s, String u, String k) {
    	serverurl = s;
    	user = u;
    	key = k;
    }
    
    private void jsonPost(int score) {
    	System.out.println("Game: "+zeGame+" - Score: "+score);
        //String jsonInputString = "{\"key1\": \"value1\", \"key2\": \"value2\"}";
        //String responseBody = sendPostRequest(serverurl, jsonInputString);
    	//System.out.println("Response Body:\n" + responseBody);
    	
    	String jsonInputString = "{\"Username\":\""+user+"\",\"APIKey\":\""+key+"\",\"Game\":\""+zeGame+"\",\"Player\":\"X\",\"Score\":\""+score+"\"}";
        
        URL apiUrl = null;
        HttpURLConnection connection = null; 

        // Set up the connection for a POST request
        try {
        	apiUrl = new URL(serverurl);
        	connection = (HttpURLConnection) apiUrl.openConnection();
			connection.setRequestMethod("POST");
	        connection.setRequestProperty("Content-Type", "application/json");
	        connection.setDoOutput(true);
		} catch (IOException e) {
			e.printStackTrace();
		}

        try (OutputStream os = connection.getOutputStream()) {
            byte[] input = jsonInputString.getBytes("utf-8");
            os.write(input, 0, input.length);
        } catch (IOException e) {
			e.printStackTrace();
		}

        try (BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream(), "utf-8"))) {
            StringBuilder response = new StringBuilder();
            String responseLine;
            while ((responseLine = br.readLine()) != null) {
                response.append(responseLine.trim());
            }
            System.out.println(response.toString());
        } catch (IOException e) {
			e.printStackTrace();
		} finally {
            connection.disconnect();
        }
    	
    }
    

    public Snek() {
        setTitle("Snek Game");
        setSize(GRID_SIZE * TILE_SIZE, GRID_SIZE * TILE_SIZE);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setLocationRelativeTo(null);

        snake = new ArrayList<>();
        snake.add(new Point(5, 5));
        direction = 'R';
        
        setAuth("http://93.95.228.40:8000/postStats/1", "itech_300", "totalyCoolApiKey");

        spawnFood();

        Timer timer = new Timer(GAME_SPEED, this);
        timer.start();

        addKeyListener(this);
        setFocusable(true);
        setFocusTraversalKeysEnabled(false);
    }

    private void spawnFood() {
        Random rand = new Random();
        int x = rand.nextInt(GRID_SIZE);
        int y = rand.nextInt(GRID_SIZE);

        food = new Point(x, y);
    }

    private void move() {
        Point head = snake.get(0);
        Point newHead = null;

        switch (direction) {
            case 'U':
                newHead = new Point(head.x, (head.y - 1 + GRID_SIZE) % GRID_SIZE);
                break;
            case 'D':
                newHead = new Point(head.x, (head.y + 1) % GRID_SIZE);
                break;
            case 'L':
                newHead = new Point((head.x - 1 + GRID_SIZE) % GRID_SIZE, head.y);
                break;
            case 'R':
                newHead = new Point((head.x + 1) % GRID_SIZE, head.y);
                break;
        }

        snake.add(0, newHead);
        if (newHead.equals(food)) {
            spawnFood();
        } else {
            snake.remove(snake.size() - 1);
        }
        
        
       
        for (int i = 1; i < snake.size(); i++) {
            if (newHead.equals(snake.get(i))) {
                gameOver();
            }
        }

        repaint();
    }
    
    
    
    private void gameOver() {
    	jsonPost(snake.size());
        JOptionPane.showMessageDialog(this, "Game Over!", "Game Over", JOptionPane.INFORMATION_MESSAGE);
        Snek.this.removeAll();
        dispose();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        move();
    }
    
    
    
    @Override
    public void keyPressed(KeyEvent e) {
        switch (e.getKeyCode()) {
            case KeyEvent.VK_UP:
                if (direction != 'D') direction = 'U';
                break;
            case KeyEvent.VK_DOWN:
                if (direction != 'U') direction = 'D';
                break;
            case KeyEvent.VK_LEFT:
                if (direction != 'R') direction = 'L';
                break;
            case KeyEvent.VK_RIGHT:
                if (direction != 'L') direction = 'R';
                break;
        }
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

    @Override
    public void keyReleased(KeyEvent e) {
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        
        
       
        for (int i = 0; i < GRID_SIZE; i++) {
            for (int j = 0; j < GRID_SIZE; j++) {
                g.drawRect(i * TILE_SIZE, j * TILE_SIZE, TILE_SIZE, TILE_SIZE);
            }
        }
        
        
        
        g.setColor(Color.GREEN);
        for (Point point : snake) {
            g.fillRect(point.x * TILE_SIZE, point.y * TILE_SIZE, TILE_SIZE, TILE_SIZE);
        }
        
        
       
        g.setColor(Color.RED);
        g.fillRect(food.x * TILE_SIZE, food.y * TILE_SIZE, TILE_SIZE, TILE_SIZE);
        
        
        
        Toolkit.getDefaultToolkit().sync();
    }
}
