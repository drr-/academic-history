package dreyesAccounts;

import java.util.Comparator;

public class NameComparator implements Comparator<Account> {

	
	public int compare(Account acct1, Account acct2) {
		String acctName1 = acct1.getCustomer().getLast() + acct1.getCustomer().getFirst();
		String acctName2 = acct1.getCustomer().getLast() + acct2.getCustomer().getFirst();
		
		if (acctName1.compareTo(acctName2) <0)
			return -1;
		else if (acctName1.compareTo(acctName2) >0)
			return 1;
		else
			return 0;
	}

	
	
}
