package dreyesAccounts;

import java.text.NumberFormat;
import java.time.LocalDate;

public class CheckingAccount extends Account {

	protected double monthlyFee;

	public CheckingAccount() {
		super();
		
	}

	public CheckingAccount(double x, Customer c, double bal, LocalDate d) {
		super(c, bal, d);
		monthlyFee = x;
	}
	
	public String toString() {
		NumberFormat nf = NumberFormat.getCurrencyInstance();
		
		return getClass().getSimpleName() + super.toString()
			+ " Monthly Fee = " + nf.format(monthlyFee) + "\n";
	}

	public double getMonthlyFee() {
		return monthlyFee;
	}

	public void setMonthlyFee(double monthlyFee) {
		this.monthlyFee = monthlyFee;
	}
	
	
	
}
