package dreyesAccounts;

import java.util.Comparator;

public class AccountIDComparator implements Comparator<Account> {

	public int compare (Account acct1, Account acct2) {
		int accNum1 = acct1.getAcctNum();
		int accNum2 = acct2.getAcctNum();
		
		if (accNum1 > accNum2)
			return 1;
		else if (accNum1 < accNum2)
			return -1;
		else
			return 0;
	}
}
