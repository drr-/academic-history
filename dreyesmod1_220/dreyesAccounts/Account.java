
package dreyesAccounts;

import java.text.NumberFormat;
import java.time.*;

public abstract class Account implements Comparable <Account> {
    
    protected LocalDate dateOpened;
    protected Customer customer;
    protected double balance;
    protected int acctNum;
        
    //formatting
    NumberFormat currency = NumberFormat.getCurrencyInstance();   
    
    //set beginning account
    public static int num = 200;
    
    //empty constructor
    public Account() {
        acctNum = num;
	num++;        
    }
    
        //full constructor
    public Account(Customer c, double bal, LocalDate d) {
        customer = c;
        balance = bal;
        dateOpened = d;
        acctNum = num;
        num++;
    }
    
    @Override
    public String toString(){
        return customer.toString() + "\n    Balance=" + currency.format(balance) + 
                " and acctNum=" + acctNum + ". Date Opened: " + dateOpened;
    }
 
    
    // write the method so it compares the balances for the Accounts
    public int compareTo (Account acc){
        if (acc.getBalance() < this.getBalance())
        	return -1;
        else if (acc.getBalance() > this.getBalance())
        	return 1;
        else
        	return 0;
    }

    //getters and setters
    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public LocalDate getDateOpened() {
        return dateOpened;
    }

    public void setDateOpened(LocalDate day) {
        this.dateOpened = day;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public int getAcctNum() {
        return acctNum;
    }

    public void setAcctNum(int acctNum) {
        this.acctNum = acctNum;
    }    
    
    
    //end getters and setters
}//end class Account
