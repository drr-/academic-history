package dreyesAccounts;

import java.io.*;

public class Customer implements  Comparable<Customer> {
	
    // three private fields
   private String last;   
    private String first;    
    private int id;
    
    public static int num = 1000;

    public Customer(String custLast, String custFirst) {
        last = custLast;
        first = custFirst;
        id = num;
        num++;
    }

    public Customer() {
        id = num;
        num++;
    }

    public Customer(String custLast, String custFirst, int idNum) {
        last = custLast;
        first = custFirst;
        id = idNum;
    }
    

    public String toString() {
        return ":   Customer #: " + id + ": " + first + " " + last;
    }

        
    public  boolean equals(Customer c) {
    	if (c.getLast().equals(this.getLast()) && c.getFirst().equals(this.getFirst()))
    		return true;
    	else
    		return false;
    }

    @Override
    public int compareTo(Customer c) {
       if ((c.getLast() + c.getFirst()).compareTo(getLast() + getFirst()) > 0) {
            return 1;
        } else {
            return -1;
        }
    }

	public String getLast() {
		return last;
	}

	public void setLast(String last) {
		this.last = last;
	}

	public String getFirst() {
		return first;
	}

	public void setFirst(String first) {
		this.first = first;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

    
}//end Customer class