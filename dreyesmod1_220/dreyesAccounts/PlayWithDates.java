package dreyesAccounts;

import java.time.DateTimeException;
import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Locale;
import java.util.Scanner;

public class PlayWithDates {
	
	public static void vacation() {
		boolean goodThing;
		goodThing = false;
		
		LocalDate enterDate = null;
		
		Scanner scan = new Scanner(System.in);

		
		while (!goodThing) {
			System.out.println("Enter the date you're leaving for your vaction:");
			System.out.print("Day: ");
			int dayEnter = scan.nextInt();
			System.out.print("Month: ");
			int monthEnter = scan.nextInt();
			System.out.print("Year: ");
			int yearEnter = scan.nextInt();
			
			
			
			try {
				enterDate = LocalDate.of(yearEnter, monthEnter, dayEnter);
				goodThing = true;
			}
			catch (DateTimeException dte) {
				System.out.println("This is not a valid date. Please re-enter");
			}
		}
		
		
		
		String dateString = enterDate.toString();
		DateTimeFormatter dateFromString = DateTimeFormatter.ofPattern("yyyy-MM-dd", Locale.ENGLISH);
		DateTimeFormatter convertFormat = DateTimeFormatter.ofPattern("EEEE", Locale.ENGLISH);
		LocalDate parseDate = LocalDate.parse(dateString, dateFromString);
		String dateDay = convertFormat.format(parseDate);
		
		Period p = Period.between(LocalDate.now(), enterDate);
		
		long untilDays = LocalDate.now().until(enterDate, ChronoUnit.DAYS);
		System.out.println("The date you entered " + enterDate + " is a " + dateDay);
		System.out.println("There are " + untilDays + " days (" +
				p.getYears() + " years, " + p.getMonths() + " months and " + p.getDays() + " days)"
				+ " until your vacation");

	}
	
	public static void timedSquares() {
		long timeTook;
		
		System.out.println("\nCalculating the sum of the squares of the numbers between 0 and 100,000,000");
		
		long sum = 0;
		
		long startTime = System.nanoTime();
		for (long i = 1; i < 10000000000L; i++) {
			sum += (i * i);
		}
		long stopTime = System.nanoTime();
		
		timeTook = (stopTime - startTime) / 1000000;
		
		System.out.println("It took " + timeTook + " milliseconds to calculate the sum " + sum);
	}
	
	public static void future() {
		
		String dateString = LocalDate.now().plusDays(100).toString();
		DateTimeFormatter dateFromString = DateTimeFormatter.ofPattern("yyyy-MM-dd", Locale.ENGLISH);
		DateTimeFormatter convertFormat = DateTimeFormatter.ofPattern("MM-dd-yyyy", Locale.ENGLISH);
		LocalDate parseDate = LocalDate.parse(dateString, dateFromString);
		String dateDay = convertFormat.format(parseDate);		
		
		System.out.println("\n100 days from today is " + dateDay);
	}

	public static void main(String[] args) {
		vacation();
		
		timedSquares();
		
		future();
	}
	
	
}
