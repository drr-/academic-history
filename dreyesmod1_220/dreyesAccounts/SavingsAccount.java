package dreyesAccounts;

import java.time.LocalDate;

public class SavingsAccount extends Account {
	
	protected double intRate;

	public SavingsAccount() {
		super();
		
	}

	public SavingsAccount(Customer c, double bal, LocalDate d, double x) {
		super(c, bal, d);
		
		intRate = x;
	}
	
	public String toString() {
		if (this instanceof SuperSavings)
			return super.toString() + " Interest rate = " + intRate + "%" + "\n";
		else
			return getClass().getSimpleName() + super.toString()
				+ " Interest = " + intRate + "%" + "\n";
	}

	public double getIntRate() {
		return intRate;
	}

	public void setIntRate(double intRate) {
		this.intRate = intRate;
	}

	
	
}
