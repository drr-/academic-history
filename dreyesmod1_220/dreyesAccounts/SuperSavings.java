package dreyesAccounts;

import java.text.NumberFormat;
import java.time.LocalDate;

public class SuperSavings extends SavingsAccount {

	protected double minDeposit;

	public SuperSavings() {
		super();
		
	}

	public SuperSavings(Customer c, double bal, LocalDate d, double x, double y) {
		super(c, bal, d, x);
		
		minDeposit = y;
		
	}
	
	public String toString() {
		NumberFormat nf = NumberFormat.getCurrencyInstance();
		
		return getClass().getSimpleName() + super.toString() + "\t"
				+ "min deposit = " + nf.format(minDeposit);
		
	}

	public double getMinDeposit() {
		return minDeposit;
	}

	public void setMinDeposit(double minDeposit) {
		this.minDeposit = minDeposit;
	}
	
	
	
	
	
}
