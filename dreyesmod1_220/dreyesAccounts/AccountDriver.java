
package dreyesAccounts;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class AccountDriver {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
    	 //create an ArrayList of Accounts
        ArrayList<Account> act = new ArrayList<>();
        
    	int choice = -1;
    	while(choice!=999) {
    		choice = menu();
    		if (choice ==1) {
    			//call loadData()
    	        loadData(act);
    		}
    		else if (choice ==2) {
    			print(act);
    		}
    		else if (choice == 3) {
    			 //call printSortedBalance()
    	        printSortedBalance(act);
    		}
    		else if (choice == 4) {
    			  //call printSortedName
    	        printSortedName(act);    	        
    		}
    		else if (choice ==5) {
    			 //call printSortedID
    	        printSortedID(act);
    		}
    		
    		else if (choice ==6) {
    			find(act);
    		}
    		else if (choice == 999) {
    			System.out.println("BYE!");
    			System.exit(0);
    		}
    	    		
    	}
    	
   
       
    }//end main
    
    public static void find(ArrayList<Account> act) {
    	boolean found = false;
    	
    	Scanner scan = new Scanner(System.in);
    	
    	System.out.print("First Name: ");
    	String first = scan.nextLine();
    	System.out.print("Last Name: ");
    	String last = scan.nextLine();
    	
    	Customer c = new Customer(last, first);
    	
    	for (int i=0; i < act.size(); i++) {
    		if (act.get(i).getCustomer().equals(c)) {
    			System.out.println(act.get(i).toString());
    			found = true;
    		}
    	}
    	
    	if (!found)
    		System.out.println("No person was found");
    	
    }
    
    public static int menu() {
    	Scanner scan = new Scanner(System.in);
    	System.out.println("\nJava Banking is in Business!");
    	System.out.println("1.  Load the data");
    	System.out.println("2.  Print out the data");
    	System.out.println("3.  Sort the data sorted by balance ");
    	System.out.println("4.  Sort the data by name");
    	System.out.println("5.  Sort the data by id");
    	System.out.println("6.  Search for a person and list all of their accounts");
    	System.out.println("999. End");
    	System.out.println("Choice:");
    	int choice = scan.nextInt();
    	return choice;
    }
    
    
    //method to load the data and print.  Uncomment this out after you
    //make your blueprints
    public static void loadData(ArrayList<Account> act){
        System.out.println("Load");
        act.add(new CheckingAccount(4.00, new Customer("Verlander", "Justin"), 
                45000.00, LocalDate.of(2019,3,22)));
        act.add(new CheckingAccount(8.00, new Customer("Trout", "Mike"), 
                22000.00, LocalDate.of(2019,4,28)));
        act.add(new CheckingAccount(4.00, new Customer("Tatis Jr.", "Fernando"), 
                1200.00, LocalDate.of(2019,6,22)));
        act.add(new SavingsAccount(new Customer("Bregman", "Alex"), 
                2000, LocalDate.of(2019,7,1),3.5));
        act.add(new SavingsAccount(new Customer("Judge", "Aaron"), 
                1100, LocalDate.of(2019,5,15), 4.5));
        act.add(new SuperSavings(new Customer("Moustakas", "Mike"), 
                45000, LocalDate.of(2019,3,22), 6.5, 20000));
        act.add(new SuperSavings(new Customer("Altuve", "Jose"), 
                28000, LocalDate.of(2019,7,8),7.0,40000));
        act.add(new SavingsAccount(new Customer("Verlander", "Justin"), 
                1350, LocalDate.of(2019,6,1), 4.5));
        
        
    }
    
  //print method
    public static void print(ArrayList<Account> act){
    	for (int i=0; i < act.size(); i++)
    		System.out.print(act.get(i).toString());
    }
    
    //method to sort by balance and print
    public static void printSortedBalance(ArrayList<Account> act){
    		System.out.println("Sorted by Balance: ");
    		Collections.sort(act);
    }
    
    //method to sort by name and print
    public static void printSortedName(ArrayList<Account> act){
        System.out.println("Sorted by Name: ");
        Collections.sort(act, new NameComparator());
           
    }
    
    //method to sort by id and print
    public static void printSortedID(ArrayList<Account> act){
    	System.out.println("Sorted by account ID: ");
        Collections.sort(act, new AccountIDComparator());
        
    }
    
}//end AccountDriver class
