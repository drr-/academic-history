package dreyesDesserts;

import java.text.DecimalFormat;
import java.text.NumberFormat;

public class Sundae extends IceCream{
	
	private String flavor;
	private double toppingCost;
	
	
	public Sundae() {
		super();
		
	}
	public Sundae(String n, double scoop, int num, String x, double y) {
		super(n, scoop, num);
		flavor = x;
		toppingCost = y;
	}
	
	public String toString() {
		NumberFormat nf = NumberFormat.getCurrencyInstance();
		return super.toString() + "\n\tTopping: " + this.flavor + " " + nf.format(getToppingCost())
			+ " Total Cost: " + nf.format(getCost());
	}
	
	public double getCost() {
		return (this.costPerScoop * this.numScoops * toppingCost);
	}
	
	public String getFlavor() {
		return flavor;
	}
	public void setFlavor(String flavor) {
		this.flavor = flavor;
	}
	public double getToppingCost() {
		return toppingCost;
	}
	public void setToppingCost(double toppingCost) {
		this.toppingCost = toppingCost;
	}
	
	

}
