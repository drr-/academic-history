

package dreyesDesserts;
import java.text.DecimalFormat;

public class Cookie extends Dessert {
    
    //fields should be number and pricePerDz
	private int number;
	private double pricePerDz;
	
	//Need the empty and full constructor
	
	
        
    public Cookie() {
		super();
		
	}

	public Cookie(String n, int x, double y) {
		super(n);
		number = x;
		pricePerDz = y;
	}

	//toString method
    public String toString() {
   
    DecimalFormat df = new DecimalFormat("#.00");
    String ans = super.toString();
    return ans + " -- " + number + " @ " + " $" + pricePerDz 
               + "  Cost: " + " $" + df.format(getCost());
    }
    
    public double getCost() {
        return number * pricePerDz/12;
    }

	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}

	public double getPricePerDz() {
		return pricePerDz;
	}

	public void setPricePerDz(double pricePerDz) {
		this.pricePerDz = pricePerDz;
	}
    
    //generate your setters and getters
    

    
    
}//end class


