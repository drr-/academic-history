
package dreyesDesserts;


import java.util.Collections;
import java.text.NumberFormat;
import java.util.ArrayList;


public class Order {

    //instance variable
    public static final double TAX_RATE = 0.065; //I chose 6.5% as a tax rate
    private ArrayList<Dessert> desserts; //arraylist as field
    private Customer cust;

    //empty constructor
    public Order() {
        desserts = new ArrayList<>();
    }
    
    public Order(Customer c) {
    	cust = c;
    	desserts = new ArrayList<>();   	
    }
    
    public Order (ArrayList<Dessert> d, Customer c) {
    	desserts = d;
    	cust = c;
    }

    //clear method - clearing the arraylist
    public void clear() {
        desserts.clear();
    }

    

    //toString method
    public String toString() {
    	NumberFormat nf = NumberFormat.getCurrencyInstance();
    	
        String receipt = "\nThe customer is " +cust.toString();
        for (int i = 0; i < desserts.size(); i++) {
            receipt = receipt + "\n";
            receipt = receipt + desserts.get(i).toString()+ "\n";
        }
        
        receipt = receipt + "      Number of Items: " + getDesserts().size() + "\n";
        receipt = receipt + "      Total cost: " + nf.format(totalCost())+ "\n";
        receipt = receipt + "      Total tax: " + nf.format(totalTax())+ "\n";
        receipt = receipt + "      Cost + Tax: " + nf.format((totalCost() + totalTax()));
        

        return receipt;

    }

    

    //totalCost method
    public double totalCost() {
        double cost = 0;
        for (int i = 0; i < desserts.size(); i++) {
            cost = cost + desserts.get(i).getCost();
        }
        return cost;
    }

    //totalTax method
    public double totalTax() {
        return totalCost() * 0.065;
    }

    //getter and setter for arraylist
    /**
     * @return the desserts
     */
    public ArrayList<Dessert> getDesserts() {
        return desserts;
    }

    /**
     * @param desserts the desserts to set
     */
    public void setDesserts(ArrayList<Dessert> desserts) {
        this.desserts = desserts;
    }

	public Customer getCust() {
		return cust;
	}

	public void setCust(Customer cust) {
		this.cust = cust;
	}

    
    
}//end class
