package drebollar_reyes_itech_340;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;

public class QueueDriver {
	static Scanner scan = new Scanner(System.in);
	
	static Queue<String> qq = new LinkedList<String>();
	
	
	
	public static String menu() {
		String which = null;
		System.out.println("\nEnter one of the following commands:");
		System.out.println("\t PRINT queue");
		System.out.println("\t REMOVE item");
		System.out.println("\t PEAK queue");
		System.out.println("\t ADD item");
		System.out.println("\t END\nChoice: ");
		which = scan.nextLine();
		return which;
	}
	
	
	
	
	public static void main(String[] args) {
		System.out.println("QUEUE EXAMPLE:");
		
		boolean coco = true;
		while (coco) {
			String choice = menu();
			
			String[] cA = choice.split(" ");
			
			if (cA[0].equalsIgnoreCase("PRINT"))
				printInfo();
			else if (cA[0].equalsIgnoreCase("REMOVE")) {
				remove(cleanInput(cA));
			} else if (cA[0].equalsIgnoreCase("ADD")) {
				add(cleanInput(cA));
			} else if (cA[0].equalsIgnoreCase("PEEK")) {
				peek();
			} else if (cA[0].equalsIgnoreCase("TEST")) {
				test();
			} else {
				coco = false;
				cleanup();
			}
		}
	}
	
	public static void cleanup() {
		scan.close();
		System.exit(0);
	}
	
	public static String cleanInput(String[] x) {
		String e = "";
		for (int i = 1; i < x.length; i++)
			e += x[i] + " ";
		//System.out.println("'" + e.strip() + "'");
		
		return e;
	}

	public static void printInfo() {
		System.out.println("Queue: ");
		System.out.println("PRINT: "+qq);
	}

	public static void remove(String item) {
		
		int lenlen = qq.size();
		
		qq.remove(item);
		
		if (qq.size() == lenlen) {
			System.out.println(item+"was not found.");
		} else {
			System.out.println(item+"was removed.");
		}
	}

	public static void add(String item) {
		int lenlen = qq.size();
		
		qq.add(item);
		
		if (qq.size() == lenlen) {
			System.out.println(item+"was not able to be added");
		} else {
			System.out.println(item+"was added.");
		}
		
		//System.out.println("ADD: "+qq);
	}
	
	public static void peek() {
		System.out.println("Peek: "+qq.peek());
		//System.out.println("ADD: "+qq);
	}
	
	public static void test() {
		
		System.out.println("Start: "+qq.toString());
		
		System.out.println("\nAdding: 'itemOne'");
		add("itemOne");
		
		System.out.println("\nAdding: 'itemTwo'");
		add("itemTwo");
		
		System.out.println("\nAdding: 'itemThree'");
		add("itemThree");
		
		System.out.println("\nPrinting Queue:");
		printInfo();
		
		System.out.println("\nAdding: 'itemFour'");
		add("itemFour");
		
		System.out.println("\nPeeking Queue:");
		peek();
		
		System.out.println("\nRemoving: 'itemFour'");
		remove("itemFour");
		
		System.out.println("\nRemoving: 'itemThree'");
		remove("itemThree");
		
		System.out.println("\nRemoving: 'itemTwo'");
		remove("itemTwo");
		
		System.out.println("\nRemoving: 'itemOne'");
		remove("itemOne");
		
		System.out.println("\nPrinting Queue:");
		printInfo();
	}
}
