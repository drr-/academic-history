package drebollar_reyes_itech_340;

import java.util.Scanner;
import java.util.*;

public class StackDriver {
	static Scanner scan = new Scanner(System.in);
	
	static Stack<String> qq = new Stack<String>();
	
	
	public static String menu() {
		String which = null;
		System.out.println("\nEnter one of the following commands:");
		System.out.println("\t PRINT stack");
		System.out.println("\t REMOVE stack");
		System.out.println("\t ADD item");
		System.out.println("\t PEEK stack");
		System.out.println("\t END\nChoice: ");
		which = scan.nextLine();
		return which;
	}
	
	
	
	
	public static void main(String[] args) {
		System.out.println("STACK EXAMPLE:");
		
		boolean coco = true;
		while (coco) {
			String choice = menu();
			
			String[] cA = choice.split(" ");
			
			if (cA[0].equalsIgnoreCase("PRINT"))
				printInfo();
			else if (cA[0].equalsIgnoreCase("REMOVE")) {
				remove();
			} else if (cA[0].equalsIgnoreCase("ADD")) {
				add(cleanInput(cA));
			} else if (cA[0].equalsIgnoreCase("PEEK")) {
				peek(); 
			} else if (cA[0].equalsIgnoreCase("TEST")) {
				test();
			} else {
				coco = false;
				cleanup();
			}
		}
	}
	
	public static void cleanup() {
		scan.close();
		System.exit(0);
	}
	
	public static String cleanInput(String[] x) {
		String e = "";
		for (int i = 1; i < x.length; i++)
			e += x[i] + " ";
		//System.out.println("'" + e.strip() + "'");
		
		return e;
	}

	public static void printInfo() {
		System.out.println("Stack: ");
		System.out.println("PRINT: "+qq);
	}

	public static void remove() {
		
		qq.pop();

		System.out.println("Updated list: "+qq.toString());
	}
	
	public static void peek() {
		System.out.println("Peeked Item: "+qq.peek());
	}

	public static void add(String item) {
		int lenlen = qq.size();
		
		qq.push(item);
		
		if (qq.size() == lenlen) {
			System.out.println(item+"was not able to be added");
		} else {
			System.out.println(item+"was added.");
		}
		
		//System.out.println("ADD: "+qq);
	}
	
	
	public static void test() {

		System.out.println("Start: "+qq.toString());
		
		System.out.println("\nAdding: 'itemOne'");
		add("itemOne");
		
		System.out.println("\nAdding: 'itemTwo'");
		add("itemTwo");
		
		System.out.println("\nAdding: 'itemThree'");
		add("itemThree");
		
		System.out.println("\nPrinting Queue:");
		printInfo();
		
		System.out.println("\nAdding: 'itemFour'");
		add("itemFour");
		
		System.out.println("\nPeeking Queue:");
		peek();
		
		System.out.println("\nRemoving top item");
		remove();
		
		System.out.println("\nRemoving top item");
		remove();
		
		System.out.println("\nRemoving top item");
		remove();
		
		System.out.println("\nRemoving top item");
		remove();
		
		System.out.println("\nPrinting Queue:");
		printInfo();
	}
}
