# Assignment One

### BY Damian Rebollar-Reyes


-----

## How To Use
* Each of the different implementations were broken down into individual classes
* Data is added via the keyboard
* This assignment was created with Eclipse
* Each class worked based on commands
     * QueueDriver has "**PRINT** queue", "**REMOVE** $item", "**PEEK** queue", "**ADD** $item"
     * ListDriver has "**PRINT** queue", "**REMOVE** $item", "**ADD** $item"
     * StackDriver has "**PRINT** queue", "**REMOVE**", "**ADD** $item", "**PEEK** queue"


> Even though the classes have the same commands, they are implemented based on each variant

## QueueDriver
* To PRINT the queue: `PRINT queue`
* To ADD an item: `ADD itemOne`
* To REMOVE and item: `REMOVE itemOne`
* To PEEK the queue: `PEEK queue`

## ListDriver
* To PRINT the queue: `PRINT queue`
* To ADD an item: `ADD itemOne`
* To REMOVE and item: `REMOVE itemOne`

## StackDriver
* To PRINT the queue: `PRINT queue`
* To ADD an item: `ADD itemOne`
* To REMOVE the top item: `REMOVE`
* To PEEK the queue: `PEEK queue`

## Extra convenience?
* Want extra convenience? With all of them, there's a "test" function that runs through a simulation of all functionality
 