package dreyesmod2_wk1;

import java.util.ArrayList;

/*
 * 
 * For each number between 1 and 10 = (i):
 *	for each number starting with 'i' = (j)
 *		call puzzle:  
 *			if base(i)   equals    limit(j):  return 1
 *    		otherwise:   call itself again and multiply by the base +1
 *    
 *    
 *    
 *	Short Version:
 *		Define a base and limit
 *		If base and limit don't equal,   then keep going down the chain (by adding 1 to base)   and call again
 */


public class puzzlePuzzlePuzzle {
	
	public static int puzzle(int base, int limit) {
		if (base>limit) {
			//System.out.println("\tBase:" + base + ">" + limit + "\n");
			return -1;
		} else if (base==limit) {
			//System.out.println("\tBase:" + base + "=" + limit + "\n");
			return 1;
		} else {
			// System.out.println("\tB: " + base + "\tL:" + limit);
			//System.out.println("\tReturning Base:" + base + "* (" + base + "+1), " + limit + "\n");
			return base * puzzle(base+1,limit);
		}
	}
	
	public static void originial(boolean debug) {
		if (debug) {
			for (int i=1;i<10;i++) {
				System.out.println("base: " + i);
				for (int j=i;j<10;j++) {
					System.out.println("\t  limit: " + j + "  puzzle: " + puzzle(i,j) + "      ");
				}
				System.out.println("\n");
			}
		} else {
			for (int i=1;i<10;i++) 
				for (int j=i;j<10;j++) {
					System.out.print("base: " + i + "  limit: " + j + "  puzzle: " + puzzle(i,j) + "      ");
				}
		}
	}
	
	public static void nonRecursive(boolean z, int x, int y) {
		System.out.println("Global Base:\t" + x + "\nGlobal Limit:\t" + y);
		
		if (!z) {
			System.out.println("\nVerbose (off): Tough luck! Here's the easier to read format anyway! muahahwhaha");
		}
		
		
		for (int g=x; g < y; g++) {
			System.out.println("\n\nbase:" + g);
			
			ArrayList<Integer> o = new ArrayList<>();
			for (int n=g; n < y; n++) {
				if ((n-1) <= 0) {
					o.add(1);
				} else if (n == g) {
					o.add(1);
				} else {
					o.add(n-1);
				}
				
				int f = 1;
				for (int i = 0; i < o.size(); i++) {
					f = f * o.get(i);
				}
				System.out.println("\t  limit: " + n + "  puzzle: " + f);
			}
		}
	}
	
	public static void main(String[] args) {
		boolean verbose = true;
		
		
		
		System.out.println("==========ORIGINIAL PORTION==========");
		originial(verbose);
		
		
		
		int gBase = 1;
		int gLimit = 10;
		System.out.println("\n\n==========NonRecursive Part==========");
		nonRecursive(verbose, gBase, gLimit);
	}

}





/*
 *Originial

public static int puzzle(int base, int limit) {
		if (base>limit)
			return -1;
		else if (base==limit)
			return 1;
		else
			return base * puzzle(base+1,limit);
	}
	
	
	public static void main(String[] args) {
		for (int i=1;i<10;i++) {
			for (int j=i;j<10;j++) {
				System.out.print("base: " + i + "  limit: " + j + "  puzzle: " + puzzle(i,j) + "      ");
			}
			System.out.println("\n");
		}
	}

*/
