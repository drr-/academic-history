package checkpointThree;

import java.text.NumberFormat;


public class Seat implements Comparable<Seat>{
	// To know which seat and on which plane
	// protected String seatID;
	protected int seatNum;
	// Economy or first class
	protected String seatType;
	protected double cost;
	// Status == R (reserved) or A (available)
	protected String status;

	public Seat() {

	}

	public Seat(int sN) {
		// seatID = sID;
		seatNum = sN;
		if (seatNum <= 4 && seatNum > 0) {
			cost = 850;
			seatType = "First Class";
		} else if (seatNum <= 12 && seatNum > 4) {
			cost = 450;
			seatType = "Economy";
		}
		status = "A";
	}

	public Seat(int sN, String t, double c, String st) {
		// seatID = sID;
		seatType = t;
		seatNum = sN;
		cost = c;
		status = st;
	}

	public String toString() {
		NumberFormat nf = NumberFormat.getCurrencyInstance();
		return "Seat number " + seatNum + " in " + seatType + " costs " + nf.format(cost) 
		+ " and has a status of: " + status;
	}
	
	public int compareTo(Seat s) {
		if (s.getSeatNum() > this.getSeatNum()) {
			return -1;
		}
		else if (s.getSeatNum() < this.getSeatNum())
			return 1;
		else
			return 0;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getSeatType() {
		return seatType;
	}

	public void setSeatType(String seatType) {
		this.seatType = seatType;
	}

	public double getCost() {
		return cost;
	}

	public void setCost(double cost) {
		this.cost = cost;
	}

	public int getSeatNum() {
		return seatNum;
	}

	public void setSeatNum(int seatNum) {
		this.seatNum = seatNum;
	}

}
