package checkpointThree;

import java.sql.CallableStatement;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;

public class EAMethodsDB {

	static Connection conn = null;
	static Statement stmt = null;
	static CallableStatement call = null;
	private static Scanner scan = new Scanner(System.in);

	public static Connection createConnection() {

		String user = "itp220";
		String pass = "itp220";
		String name = "eagles";
		String driver = "com.mysql.jdbc.Driver";
		String url = "jdbc:mysql://localhost:3306/" + name;

		System.out.println(driver);
		System.out.println(url);

		try {
			Class.forName(driver).newInstance();
			conn = DriverManager.getConnection(url, user, pass);
			System.out.println("Connection really is from : " + conn.getClass().getName());
			System.out.println("Connection successful!");

		} catch (Exception e) {
			e.printStackTrace();
		}
		return conn;
	}

	public static void closeConnection() {
		if (conn != null) {
			try {
				conn.close();
				conn = null;
				// stmt.close();
				System.out.println("The connection was successfully closed");
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	public static void checkConnect() {
		if (conn == null) {
			conn = createConnection();
		}
		if (stmt == null) {
			try {
				stmt = conn.createStatement();
			} catch (SQLException e) {
				System.out.println("Cannot create the statement");
			}

		}
	}

	public void createReservation() {
		Scanner scan = new Scanner(System.in);
		checkConnect();

		System.out.println("Please enter your first and last name:");
		String custName = scan.nextLine();

	

		int custID = 0;

		String query = "SELECT * FROM customers WHERE customers.name = '" + custName + "'";
		try {
			stmt = conn.createStatement();

			ResultSet rs = stmt.executeQuery(query);

			boolean found = false;

			while (rs.next()) {

				custID = rs.getInt(1);

				String name = rs.getString(2);
				String address = rs.getString(3);
				String phone = rs.getString(4);
				if (name == custName)
					found = true;

			}
		} catch (SQLException e) {
			System.out.println("SQL Exception");
			e.printStackTrace();
		}

		boolean valid = false;
		System.out.println("What is your destination (R = Roanoke, P = Pheonix): ");
		String direction = "";
		while (!valid) {
			String destination = scan.nextLine();
			if (destination.equalsIgnoreCase("R")) {
				valid = true;
				direction = "PR";
			} else if (destination.equalsIgnoreCase("P")) {
				valid = true;
				direction = "RP";
			} else {
				System.out.println("Please enter a valid destination for your reservation");
			}
		}
		valid = false;
		int date1 = 0;
		System.out.println("Enter a number between 12 and 18 for your reservation date in November");
		while (!valid) {
			try {
				date1 = scan.nextInt();
				if (date1 >= 12 && date1 <= 18) {
					valid = true;
				} else {
					System.out.println("Please enter a valid date between 12 and 18");
				}
			} catch (InputMismatchException e) {
				System.out.println("Please enter numbers only.");
				scan.nextLine();
			}
		}
		scan.nextLine();
		String time1 = "";
		valid = false;
		System.out.println("Do you want the AM flight or the PM flight?");
		while (!valid) {
			time1 = scan.nextLine();
			if (time1.equalsIgnoreCase("AM") || time1.equalsIgnoreCase("PM")) {
				valid = true;
			} else {
				System.out.println("Please choose either AM or PM for your flight time");
			}
		}
		String flightCode1 = Integer.toString(date1) + direction + time1;
		flightCode1 = flightCode1.toUpperCase();

		String queryCheckFlight = "SELECT * FROM flights WHERE flights.flightCode = '" + flightCode1 + "'";
		int availableEcon = 0;
		int availableFirstClass = 0;
		ArrayList<Integer> availableEconSeats = new ArrayList<>();
		ArrayList<Integer> availableFirstClassSeats = new ArrayList<>();
		try {
			stmt = conn.createStatement();

			ResultSet rs1 = stmt.executeQuery(queryCheckFlight);

			int flightID = 0;
			String flightCode = "";
			int date = 0;
			String route = "";
			String time = "";

			while (rs1.next()) {

				flightID = rs1.getInt(1);
				flightCode = rs1.getString(2);
				date = rs1.getInt(3);
				route = rs1.getString(4);
				time = rs1.getString(5);
				boolean found = true;
			}

			String queryFlightAvailableSeats = "SELECT seatNumber from flight_seats WHERE flightID = '" + flightID
					+ "' AND seatStatus = 'A' ";

			ResultSet ras = stmt.executeQuery(queryFlightAvailableSeats);

			while (ras.next()) {
				if (ras.getInt(1) < 5) {
					availableFirstClass += 1;
					availableFirstClassSeats.add(rs1.getInt(1));
				} else {
					availableEcon += 1;
					availableEconSeats.add(rs1.getInt(1));
				}
			}
		} catch (SQLException e) {
			System.out.println("SQL Exception");
			e.printStackTrace();
		}
		

		valid = false;
		String section = "";
		System.out.println("Which section for your seats? E for economy." + " F for FirstClass.");
		while (!valid) {
			section = scan.nextLine();
			if (section.equalsIgnoreCase("E") || section.equalsIgnoreCase("F")) {
				// if (section.equalsIgnoreCase("E") && availableEcon.size() == 0) {
				if (section.equalsIgnoreCase("E") && availableEcon == 0) {
					System.out.println("There are no seats in economy.");}
				//} else if (section.equalsIgnoreCase("F") && availableFirstClass.size() == 0) {
				else if (section.equalsIgnoreCase("F") && availableFirstClass == 0) {
					System.out.println("There are no seats in first class.");
				} else
					valid = true;
			} else {
				System.out.println("Please enter a valid section");
			}
		}
		
		valid = false;
		int numSeats = 0;
		System.out.println("How many seats would you like to reserve?");
		while (!valid) {
			try {
				numSeats = scan.nextInt();
				if (numSeats > 0) {
					//if (numSeats <= availableFirstClass.size() && section.equalsIgnoreCase("F"))
					if (numSeats <= availableFirstClass && section.equalsIgnoreCase("F")) {
						valid = true; }
					//else if (numSeats <= availableEcon.size() && section.equalsIgnoreCase("E")) {
					else if (numSeats <= availableEcon && section.equalsIgnoreCase("E")) {
						valid = true;
					} else {
						System.out.println("Please enter a valid number of seats");
					}
				} else {
					System.out.println("Please enter a valid number of seats");
				}
			} catch (InputMismatchException e) {
				System.out.println("Please enter numbers only.");
				scan.nextLine();
			}
		}
		scan.nextLine();
		ArrayList<Integer> seatsSelected = new ArrayList<Integer>();
		if (section.equalsIgnoreCase("E")) {
			for (int i = 0; i < numSeats; i++) {
				//seatsSelected.add(availableEcon.get(i));
				seatsSelected.add(availableEconSeats.get(i));
				// System.out.println(seatsSelected.toString());
			}
		} else {
			for (int i = 0; i < numSeats; i++) {
				//seatsSelected.add(availableFirstClass.get(i));
				seatsSelected.add(availableFirstClassSeats.get(i));
				// System.out.println(seatsSelected.toString());
			}
		}
		

		String query1 = "INSERT INTO reservations (seat_type, flightID) VALUES ('" + section + "','" + flightCode1
				+ ")";

		try {
			stmt = conn.createStatement();
			stmt.executeUpdate(query1);
			
		} catch (SQLException e) {
			System.out.println("SQL Exception");
			e.printStackTrace();
		}
		
	
		
		int reservationID = 0;
		String query2 = "SELECT MAX(reservationID) FROM reservations WHERE deleted = 0";

		try {
			stmt = conn.createStatement();
			stmt.executeUpdate(query2);
			
		} catch (SQLException e) {
			System.out.println("SQL Exception");
			e.printStackTrace();
		}
		
		
		String query3 = "INSERT INTO customer_reservations (custID, reservationID) VALUES ("+custID+","+reservationID+")";
		try {
			stmt = conn.createStatement();
			stmt.executeUpdate(query3);
			
		} catch (SQLException e) {
			System.out.println("SQL Exception");
			e.printStackTrace();
		}
		
		
	}

	

	public void printCustomers() {

		checkConnect();
		String stored = "{call print_cust ()}";

		try {
			call = conn.prepareCall(stored);
			ResultSet rs = call.executeQuery();

			System.out.println("Customer Information:");
			System.out.println();

			while (rs.next()) {

				int custID = rs.getInt(1);
				String name = rs.getString(2);
				String address = rs.getString(3);
				String phone = rs.getString(4);

				System.out.println(
						"Customer ID: " + custID + "  Name: " + name + "   Address: " + address + "   Phone: " + phone);
			}

		}

		catch (SQLException e) {
			System.out.println("SQL Exception");
			e.printStackTrace();
		}

	}

	public void printPilots() {

		checkConnect();
		String stored = "{call print_pilots ()}";

		try {
			call = conn.prepareCall(stored);
			ResultSet rs = call.executeQuery();

			System.out.println("Pilot Information:");
			System.out.println();

			while (rs.next()) {

				int pilotID = rs.getInt(1);
				String pilotName = rs.getString(2);
				String pilotAddress = rs.getString(3);
				String pilotPhone = rs.getString(4);

				System.out.println("Pilot ID: " + pilotID + "  Name: " + pilotName + "   Address: " + pilotAddress
						+ "   Phone: " + pilotPhone);
			}

		}

		catch (SQLException e) {
			System.out.println("SQL Exception");
			e.printStackTrace();
		}

	}

	public void findCustInfo() {
		checkConnect();

		Scanner scan = new Scanner(System.in);

		System.out.println("Please enter a customer ID:");
		int id = scan.nextInt();

		String query = "SELECT * FROM customers WHERE customers.custID = " + id;
		try {
			stmt = conn.createStatement();

			ResultSet rs = stmt.executeQuery(query);
			System.out.println("Customer Information:");
			System.out.println();

			boolean found = false;
			while (rs.next()) {

				int custID = rs.getInt(1);
				String name = rs.getString(2);
				String address = rs.getString(3);
				String phone = rs.getString(4);

				if (custID == id)
					;
				System.out.println(
						"Customer ID: " + custID + "  Name: " + name + "   Address: " + address + "   Phone: " + phone);
				found = true;
			}

			if (!found) {
				System.out.println("That customer was not found");

			}

		}

		catch (SQLException e) {
			System.out.println("SQL Exception");
			e.printStackTrace();
		}

	}

	public void addNewCust() {

		checkConnect();

		String yOrN = "y";

		while (yOrN.equals("y")) {
			Scanner scan = new Scanner(System.in);
			System.out.println("Customer's name?");
			String name = scan.nextLine();
			System.out.println("Customer's address?");
			String address = scan.nextLine();
			System.out.println("Customer's phone number?");
			String phone = scan.nextLine();

			String queryAddCustomer = "INSERT INTO customers (name, address, phone) VALUES ('" + name + "','" + address
					+ "','" + phone + "')";

			try {
				stmt = conn.createStatement();
				stmt.executeUpdate(queryAddCustomer);
				System.out.println("Customer added!");
			} catch (SQLException e) {
				System.out.println("SQL Exception");
				e.printStackTrace();
			}

			System.out.println("More Customers? (y/n)");
			yOrN = scan.next();

		}

	}

	public void printPilotSchedule() {

		checkConnect();

		Scanner scan = new Scanner(System.in);

		System.out.println("Please enter a pilot ID:");
		int pId = scan.nextInt();

		String query = "SELECT flights.flightCode, pilot_flights.pilotID, pilots.pilotName" + " FROM flights\r\n"
				+ " INNER JOIN pilot_flights on flights.flightID = pilot_flights.flightID"
				+ " INNER JOIN pilots on pilot_flights.pilotID = pilots.pilotID" + " WHERE pilots.pilotID = " + pId;
		try {
			stmt = conn.createStatement();

			ResultSet rs = stmt.executeQuery(query);
			System.out.println("Pilot Schedule");
			System.out.println();

			boolean found = false;
			while (rs.next()) {

				found = true;
			}

			if (!found) {
				System.out.println("That pilot was not found");

			}

		}

		catch (SQLException e) {
			System.out.println("SQL Exception");
			e.printStackTrace();
		}

	}

	public void displaySeatMapForFlight() {
		checkConnect();

		Scanner scan = new Scanner(System.in);
		System.out.println("Please enter a valid flight code");
		String flightCode = scan.nextLine();
		flightCode.toUpperCase();

		String query = "SELECT flight_seats.seatNumber, flight_seats.seatStatus "
				+ "FROM (flight_seats INNER JOIN flights on flight_seats.flightID = flights.flightID) "
				+ "WHERE flights.flightCode = '" + flightCode + "'";
		try {
			stmt = conn.createStatement();

			ResultSet rs = stmt.executeQuery(query);

			boolean found = false;
			System.out.println();
			System.out.println("Flight " + flightCode + " Seat Chart");
			System.out.println();
			while (rs.next()) {

				int seatNumber = rs.getInt(1);
				String seatStatus = rs.getString(2);
				System.out.println("Seat: " + seatNumber + " | Available/Reserved: " + seatStatus);
				found = true;
			}

			if (!found) {
				System.out.println("That flight was not found");

			}

		}

		catch (SQLException e) {
			System.out.println("SQL Exception");
			e.printStackTrace();
		}

	}

	public void printIncomeOneFlight() {
		checkConnect();
		NumberFormat nf = NumberFormat.getCurrencyInstance();
		Scanner scan = new Scanner(System.in);
		System.out.println("Please enter a valid flight code");
		String flightCode = scan.nextLine();
		flightCode.toUpperCase();
		ArrayList<Double> totalIncome = new ArrayList<Double>();
		String query = "SELECT flight_seats.seatNumber, flight_seats.seatStatus "
				+ "FROM (flight_seats INNER JOIN flights on flight_seats.flightID = flights.flightID) "
				+ "WHERE flights.flightCode = '" + flightCode + "'";
		try {
			stmt = conn.createStatement();

			ResultSet rs = stmt.executeQuery(query);

			System.out.println();
			System.out.println("Flight " + flightCode + " Income");
			System.out.println();
			while (rs.next()) {

				int cost = 0;

				int seatNumber = rs.getInt(1);
				String seatStatus = rs.getString(2);

				if (seatNumber <= 4 && seatNumber > 0 && seatStatus.equals("R"))
					cost = 450;

				else if (seatNumber <= 12 && seatNumber > 4 && seatStatus.equals("R"))
					cost = 850;

				totalIncome.add((double) cost);

			}

		}

		catch (SQLException e) {
			System.out.println("SQL Exception");
			e.printStackTrace();
		}
		double income = 0;
		for (int i = 0; i < totalIncome.size(); i++)
			income = income + totalIncome.get(i);
		System.out.println(nf.format(income));
	}

	public void printIncomeAllFlights() {
		checkConnect();
		NumberFormat nf = NumberFormat.getCurrencyInstance();
		Scanner scan = new Scanner(System.in);

		ArrayList<Double> totalIncome = new ArrayList<Double>();
		String query = "SELECT flight_seats.seatNumber, flight_seats.seatStatus FROM flight_seats";
		try {
			stmt = conn.createStatement();

			ResultSet rs = stmt.executeQuery(query);

			System.out.println();
			System.out.println("Income From All Scheduled Flights");
			System.out.println();
			while (rs.next()) {

				int cost = 0;

				int seatNumber = rs.getInt(1);
				String seatStatus = rs.getString(2);

				if (seatNumber <= 4 && seatNumber > 0 && seatStatus.equals("R"))
					cost = 450;

				else if (seatNumber <= 12 && seatNumber > 4 && seatStatus.equals("R"))
					cost = 850;

				totalIncome.add((double) cost);

			}

		}

		catch (SQLException e) {
			System.out.println("SQL Exception");
			e.printStackTrace();
		}
		double income = 0;
		for (int i = 0; i < totalIncome.size(); i++)
			income = income + totalIncome.get(i);
		System.out.println(nf.format(income));
	}

	public void findDeletedReservationCustID() {
		Scanner scan = new Scanner(System.in);
		checkConnect();
		boolean found = false;

		System.out.println("Enter Customer ID");
		int customerID = scan.nextInt();

		try {
			String query = "SELECT * FROM flights,reservations,reservation_seats,customers,customer_reservations WHERE flights.flightID = reservations.flightID AND reservations.reservationID = reservation_seats.reservationID AND reservations.reservationID = customer_reservations.reservationID AND customers.custID = customer_reservations.custID AND "
					+ "customers.custID LIKE '" + customerID
					+ "' AND reservations.deleted LIKE '1' ORDER BY reservations.reservationID;";

			stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(query);

			while (rs.next()) {
				if (found == false)
					System.out.println("Reservation Information: ");

				System.out.println("Reservation ID: " + rs.getString(11) + " \n\t " + rs.getString(14)
						+ " has a reservation for flight " + rs.getString(2) + " and thier seat numbers are "
						+ rs.getString(12));
				found = true;
			}

			if (!found)
				System.out.println("Reservation was not found\n");

		} catch (SQLException e) {
			System.out.println("Cannot create the statement" + e);
		}

	}

	public void findDeletedReservationNumber() {
		Scanner scan = new Scanner(System.in);
		checkConnect();
		boolean found = false;
		System.out.println("Enter Reservation ID");
		int reservationID = scan.nextInt();

		try {
			String query = "SELECT * FROM flights,reservations,reservation_seats,customers,customer_reservations WHERE flights.flightID = reservations.flightID AND reservations.reservationID = reservation_seats.reservationID AND reservations.reservationID = customer_reservations.reservationID AND customers.custID = customer_reservations.custID AND "
					+ "reservations.reservationID LIKE '" + reservationID
					+ "' AND reservations.deleted LIKE '1' ORDER BY reservations.reservationID;";

			stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(query);

			while (rs.next()) {
				if (found == false)
					System.out.println("Reservation Information: ");

				System.out.println("Reservation ID: " + rs.getString(11) + " \n\t " + rs.getString(14)
						+ " has a reservation for flight " + rs.getString(2) + " and thier seat numbers are "
						+ rs.getString(12));
				found = true;
			}

			if (!found)
				System.out.println("Reservation was not found\n");

		} catch (SQLException e) {
			System.out.println("Cannot create the statement" + e);
		}

	}

	public void findReservationCustID() {
		Scanner scan = new Scanner(System.in);
		checkConnect();
		boolean found = false;
		System.out.println("Enter Customer ID");
		int customerID = scan.nextInt();

		try {
			String query = "SELECT * FROM flights,reservations,reservation_seats,customers,customer_reservations WHERE flights.flightID = reservations.flightID AND reservations.reservationID = reservation_seats.reservationID AND reservations.reservationID = customer_reservations.reservationID AND customers.custID = customer_reservations.custID AND "
					+ "customers.custID LIKE '" + customerID
					+ "' AND reservations.deleted LIKE '0' ORDER BY reservations.reservationID;";

			stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(query);

			while (rs.next()) {
				if (found == false)
					System.out.println("Reservation Information: ");

				System.out.println("Reservation ID: " + rs.getString(11) + " \n\t " + rs.getString(14)
						+ " has a reservation for flight " + rs.getString(2) + " and thier seat numbers are "
						+ rs.getString(12));
				found = true;
			}

			if (!found)
				System.out.println("Reservation was not found\n");

		} catch (SQLException e) {
			System.out.println("Cannot create the statement" + e);
		}

	}

	public void findReservationNumber() {
		Scanner scan = new Scanner(System.in);
		checkConnect();
		boolean found = false;
		System.out.println("Enter Reservation ID");
		int reservationID = scan.nextInt();

		try {
			String query = "SELECT * FROM flights,reservations,reservation_seats,customers,customer_reservations WHERE flights.flightID = reservations.flightID AND reservations.reservationID = reservation_seats.reservationID AND reservations.reservationID = customer_reservations.reservationID AND customers.custID = customer_reservations.custID AND "
					+ "reservations.reservationID LIKE '" + reservationID
					+ "' AND reservations.deleted LIKE '0' ORDER BY reservations.reservationID;";

			stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(query);

			while (rs.next()) {
				if (found == false)
					System.out.println("Reservation Information: ");

				System.out.println("Reservation ID: " + rs.getString(11) + " \n\t " + rs.getString(14)
						+ " has a reservation for flight " + rs.getString(2) + " and thier seat numbers are "
						+ rs.getString(12));
				found = true;
			}

			if (!found)
				System.out.println("Reservation was not found\n");

		} catch (SQLException e) {
			System.out.println("Cannot create the statement" + e);
		}

	}

	public void printFlights() {
		checkConnect();
		try {
			String query = "SELECT * FROM flights";

			stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(query);

			while (rs.next()) {
				System.out.println("Flight ID: " + rs.getInt(1) + " \n\tCode: " + rs.getString(2) + " \n\tDate: "
						+ rs.getInt(3) + " \n\tRoute: " + rs.getString(4) + " \n\tTime: " + rs.getString(5) + "\n");
			}
		} catch (SQLException e) {
			System.out.println("Cannot create the statement" + e);
		}

	}

	public void deleteReservation() {
		Scanner scan = new Scanner(System.in);
		checkConnect();
		System.out.println("Enter Reservation ID");
		int reservationID = scan.nextInt();

		try {
			String query = "DELETE FROM reservations WHERE reservations.reservationID =" + reservationID + ";";

			stmt = conn.createStatement();
			int rs = stmt.executeUpdate(query);

			if (rs == 0)
				System.out.println("Reservation Not Found");
			else
				System.out.println("Reservation was deleted");

		} catch (SQLException e) {
			System.out.println("Cannot create the statement");
		}

	}

}
