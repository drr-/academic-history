package checkpointThree;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collections;

public class Flight {
	//Include which plane in flightCode
	protected String flightCode;
	protected Seat[][] seatMap;
	protected int date;
	protected String route;
	protected String time;
	protected Pilot pilot;
	
	public Flight() {
		
	}
	
	public Flight(String fC, int d, String r, String t, Pilot p , Seat[][] sm) {
		flightCode = fC;
		date = d;
		route = r;
		time = t;
		pilot = p;
		seatMap = sm;
	}
	
	public String toString() {
		return "Flight: " + flightCode + " PilotID: " + pilot.getPilotId() + " - " + pilot.getName();
	}
	
	public String seatsAvailable() {
		NumberFormat nf = NumberFormat.getCurrencyInstance();
		String sA;
		int firstClass = 0;
		int economy = 0;
		double fCCost = 0;
		double econCost = 0;
		for (int row = 0; row < seatMap.length; row++) {
			for (int col = 0; col<seatMap[row].length; col++) {
				if (seatMap[row][col] != null) {
					Seat st = seatMap[row][col];
					if (st.getStatus().equals("A")) {
						if (st.getSeatNum() < 5) {
							firstClass++;
							fCCost = st.getCost();
						}
						else
							economy++;
							econCost = st.getCost();
					}
				}
			}
		}
		sA = "There are " + firstClass + " seats in First Class (" + nf.format(fCCost) + 
				" per seat) and " + economy + " seats in Economy (" + nf.format(econCost) + " per seat).";
		return sA;
	}
	
	public ArrayList<Seat> firstClassSeatsAvailable() {
		ArrayList<Seat> firstClass = new ArrayList<Seat>();
		
		for (int row = 0; row < seatMap.length; row++) {
			for (int col = 0; col<seatMap[row].length; col++) {
				if (seatMap[row][col] != null) {
					Seat st = seatMap[row][col];
					if (st.getStatus().equals("A")) {
						if (st.getSeatNum() < 5) {
							firstClass.add(st);
						}	
					}
				}
			}
		}
		Collections.sort(firstClass);
		return firstClass;	
	}
	
	public ArrayList<Seat> economySeatsAvailable() {
		ArrayList<Seat> economy = new ArrayList<Seat>();
		for (int row = 0; row < seatMap.length; row++) {
			for (int col = 0; col<seatMap[row].length; col++) {
				if (seatMap[row][col] != null) {
					Seat st = seatMap[row][col];
					if (st.getStatus().equals("A")) {
						if (st.getSeatNum() > 4) {
							economy.add(st);
						}
					}
				}
			}
		}
		Collections.sort(economy);
		return economy;

	}
	
	public String seats() {
		String seatsMap = "";
		
		for (int row = 0; row < seatMap.length; row ++) {
			for (int col = 0; col < seatMap[row].length; col ++) {
				if (seatMap[row][col] == null) {
					seatsMap = seatsMap + "     ";
				}
				else {
					Seat st = seatMap[row][col];
					if (Integer.toString(st.getSeatNum()).length() == 1) {
						seatsMap = seatsMap + " " + st.getSeatNum() + "(" + st.getStatus() + ")";
					}
					else {
						seatsMap = seatsMap + st.getSeatNum() + "(" + st.getStatus() + ")";
					}
				}
				
			}
			seatsMap = seatsMap + "\n";
		}
		
		return seatsMap;
	}

	public String getFlightCode() {
		return flightCode;
	}

	public void setFlightCode(String flightCode) {
		this.flightCode = flightCode;
	}

	public int getDate() {
		return date;
	}

	public void setDate(int date) {
		this.date = date;
	}

	public String getRoute() {
		return route;
	}

	public void setRoute(String route) {
		this.route = route;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public Pilot getPilot() {
		return pilot;
	}

	public void setPilot(Pilot pilot) {
		this.pilot = pilot;
	}
	
	
}
