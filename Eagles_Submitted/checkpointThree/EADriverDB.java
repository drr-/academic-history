package checkpointThree;

import java.sql.Connection;
import java.util.Scanner;

public class EADriverDB {

	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);
		EAMethodsDB bm = new EAMethodsDB();

		Connection conn;

		boolean more = true;
		while (more) {
			menu();
			System.out.println("Choice:");
			int ans = scan.nextInt();
			if (ans == 0)
				conn = bm.createConnection();

			else if (ans == 1)
				bm.findCustInfo();
			else if (ans == 2)
				bm.addNewCust();
			else if (ans == 3)
				bm.printPilotSchedule();
			else if (ans==4)
				bm.createReservation();
			else if (ans==5)
				bm.deleteReservation();
			else if (ans==6)
				bm.findReservationNumber();
			else if (ans==7)
				bm.findReservationCustID();
			else if (ans==8)
				bm.printIncomeOneFlight();
			else if (ans==9)
				bm.printIncomeAllFlights();
			else if (ans==10)
				bm.findDeletedReservationNumber();
			else if (ans==11)
				bm.findDeletedReservationCustID();
			else if (ans == 12)
				bm.displaySeatMapForFlight();
			else if (ans == 13)
				bm.printCustomers();
			else if (ans == 14)
				bm.printPilots();
			else if (ans==15)
				bm.printFlights();

			else {
				System.out.println("BYE!!!");
				more = false;
				System.exit(0);
			}

		}

	}

	public static void menu() {
		System.out.println();
		System.out.println();
		System.out.println("          Welcome to Eagle Airways\n_____________________________________________");
		System.out.println("Choose one:");
		System.out.println("   1. Find Customer information");
		System.out.println("   2. Add New Customer");
		System.out.println("   3. Print Pilot Schedule");
		System.out.println("   4. Create reservation");
		System.out.println("   5. Delete Existing Reservation");
		System.out.println("   6. Find Existing Reservation By Reservation Number");
		System.out.println("   7. Find Existing Reservation By Customer ID");
		System.out.println("   8. Print Gross Income For A Specific Flight");
		System.out.println("   9. Print Gross Income For All Scheduled Flights ");
		System.out.println("   10. Find Deleted Reservation By Reservation Number ");
		System.out.println("   11. Find Deleted Reservation By Customer ID");
		System.out.println("   12. Show Seat Arranagement For A Flight");
		System.out.println("   13. Print out all of the Customer information");
		System.out.println("   14. Print out all of the Pilot information");
		System.out.println("   15. Print out all the Flight information");

		System.out.println("   999. Exit");
	}
}
