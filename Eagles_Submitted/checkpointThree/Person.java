package checkpointThree;

public abstract class Person {
	
	protected String name;
	
	protected String address;
	
	protected String phone;
	
	public Person(){
		
	}
	
	public Person(String n, String add, String ph) {
		
		name = n;
		address = add;
		phone = ph;
	}
	
	public String toString() {
		return name + " lives at " + address + " and their phone number is " + phone;
	}
	
	public String toStringF() {
		return name + "|" + address + "|" + phone;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

}
