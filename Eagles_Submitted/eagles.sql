-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 22, 2022 at 04:04 AM
-- Server version: 10.4.24-MariaDB
-- PHP Version: 8.1.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `eagles`
--

DELIMITER $$
--
-- Procedures
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `print_cust` ()   SELECT * FROM customers$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `print_pilots` ()   SELECT * FROM pilots$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `custID` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `address` varchar(100) NOT NULL,
  `phone` char(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`custID`, `name`, `address`, `phone`) VALUES
(1, 'Donald Duck', '1134 DisneyLand Road', '5403372281'),
(2, 'Mickey Mouse', '741 Mouse Lane', '7911734877'),
(3, 'Minnie Mouse', '742 Mouse Lane', '7916728990'),
(4, 'Goofy', '532 Silly Circle', '9999990110'),
(6, 'Todd Blankenship', '123 Street name', '5555555555');

-- --------------------------------------------------------

--
-- Table structure for table `customer_reservations`
--

CREATE TABLE `customer_reservations` (
  `customerReservationID` int(11) NOT NULL,
  `custID` int(11) NOT NULL,
  `reservationID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `flights`
--

CREATE TABLE `flights` (
  `flightID` int(11) NOT NULL,
  `flightCode` varchar(10) NOT NULL,
  `date` int(11) NOT NULL,
  `route` char(2) NOT NULL,
  `time` char(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `flights`
--

INSERT INTO `flights` (`flightID`, `flightCode`, `date`, `route`, `time`) VALUES
(1, '12RPAMA', 12, 'RP', 'AM'),
(2, '12PRAMB', 12, 'PR', 'AM'),
(3, '12RPPMB', 12, 'RP', 'PM'),
(4, '12PRPMA', 12, 'PR', 'PM'),
(5, '13RPAMA', 13, 'RP', 'AM'),
(6, '13PRAMB', 13, 'PR', 'AM'),
(7, '13RPPMB', 13, 'RP', 'PM'),
(8, '13PRPMA', 13, 'PR', 'PM'),
(9, '14RPAMA', 14, 'RP', 'AM'),
(10, '14PRAMB', 14, 'PR', 'AM'),
(11, '14RPPMB', 14, 'RP', 'PM'),
(12, '14PRPMA', 14, 'PR', 'PM'),
(13, '15RPAMA', 15, 'RP', 'AM'),
(14, '15PRAMB', 15, 'PR', 'AM'),
(15, '15RPPMB', 15, 'RP', 'PM'),
(16, '15PRPMA', 15, 'PR', 'PM'),
(17, '16RPAMA', 16, 'RP', 'AM'),
(18, '16PRAMB', 16, 'PR', 'AM'),
(19, '16RPPMB', 16, 'RP', 'PM'),
(20, '16PRPMA', 16, 'PR', 'PM'),
(21, '17RPAMA', 17, 'RP', 'AM'),
(22, '17PRAMB', 17, 'PR', 'AM'),
(23, '17RPPMB', 17, 'RP', 'PM'),
(24, '17PRPMA', 17, 'PR', 'PM'),
(25, '18RPAMA', 18, 'RP', 'AM'),
(26, '18PRAMB', 18, 'PR', 'AM'),
(27, '18RPPMB', 18, 'RP', 'PM'),
(28, '18PRPMA', 18, 'PR', 'PM');

-- --------------------------------------------------------

--
-- Table structure for table `flight_seats`
--

CREATE TABLE `flight_seats` (
  `flightSeatID` int(11) NOT NULL,
  `flightID` int(11) NOT NULL,
  `seatNumber` int(11) NOT NULL,
  `seatStatus` char(1) NOT NULL DEFAULT 'A'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `flight_seats`
--

INSERT INTO `flight_seats` (`flightSeatID`, `flightID`, `seatNumber`, `seatStatus`) VALUES
(1, 1, 1, 'R'),
(2, 1, 2, 'A'),
(3, 1, 3, 'A'),
(4, 1, 4, 'A'),
(5, 1, 5, 'A'),
(6, 1, 6, 'A'),
(7, 1, 7, 'A'),
(8, 1, 8, 'A'),
(9, 1, 9, 'R'),
(10, 1, 10, 'A'),
(11, 1, 11, 'A'),
(12, 1, 12, 'A'),
(13, 2, 1, 'A'),
(14, 2, 2, 'A'),
(15, 2, 3, 'A'),
(16, 2, 4, 'A'),
(17, 2, 5, 'A'),
(18, 2, 6, 'A'),
(19, 2, 7, 'A'),
(20, 2, 8, 'A'),
(21, 2, 9, 'A'),
(22, 2, 10, 'A'),
(23, 2, 11, 'A'),
(24, 2, 12, 'A'),
(25, 3, 1, 'A'),
(26, 3, 2, 'A'),
(27, 3, 3, 'A'),
(28, 3, 4, 'R'),
(29, 3, 5, 'A'),
(30, 3, 6, 'A'),
(31, 3, 7, 'A'),
(32, 3, 8, 'A'),
(33, 3, 9, 'A'),
(34, 3, 10, 'A'),
(35, 3, 11, 'A'),
(36, 3, 12, 'A'),
(37, 4, 1, 'A'),
(38, 4, 2, 'A'),
(39, 4, 3, 'A'),
(40, 4, 4, 'A'),
(41, 4, 5, 'A'),
(42, 4, 6, 'A'),
(43, 4, 7, 'A'),
(44, 4, 8, 'A'),
(45, 4, 9, 'A'),
(46, 4, 10, 'A'),
(47, 4, 11, 'A'),
(48, 4, 12, 'A'),
(49, 5, 1, 'A'),
(50, 5, 2, 'A'),
(51, 5, 3, 'A'),
(52, 5, 4, 'A'),
(53, 5, 5, 'A'),
(54, 5, 6, 'A'),
(55, 5, 7, 'A'),
(56, 5, 8, 'A'),
(57, 5, 9, 'A'),
(58, 5, 10, 'A'),
(59, 5, 11, 'A'),
(60, 5, 12, 'A'),
(61, 6, 1, 'A'),
(62, 6, 2, 'A'),
(63, 6, 3, 'A'),
(64, 6, 4, 'A'),
(65, 6, 5, 'A'),
(66, 6, 6, 'A'),
(67, 6, 7, 'A'),
(68, 6, 8, 'A'),
(69, 6, 9, 'A'),
(70, 6, 10, 'A'),
(71, 6, 11, 'A'),
(72, 6, 12, 'A'),
(73, 7, 1, 'A'),
(74, 7, 2, 'A'),
(75, 7, 3, 'A'),
(76, 7, 4, 'A'),
(77, 7, 5, 'A'),
(78, 7, 6, 'A'),
(79, 7, 7, 'A'),
(80, 7, 8, 'A'),
(81, 7, 9, 'A'),
(82, 7, 10, 'A'),
(83, 7, 11, 'A'),
(84, 7, 12, 'A'),
(85, 8, 1, 'A'),
(86, 8, 2, 'A'),
(87, 8, 3, 'A'),
(88, 8, 4, 'A'),
(89, 8, 5, 'A'),
(90, 8, 6, 'A'),
(91, 8, 7, 'A'),
(92, 8, 8, 'A'),
(93, 8, 9, 'A'),
(94, 8, 10, 'A'),
(95, 8, 11, 'A'),
(96, 8, 12, 'A'),
(97, 9, 1, 'A'),
(98, 9, 2, 'A'),
(99, 9, 3, 'A'),
(100, 9, 4, 'A'),
(101, 9, 5, 'A'),
(102, 9, 6, 'A'),
(103, 9, 7, 'A'),
(104, 9, 8, 'A'),
(105, 9, 9, 'A'),
(106, 9, 10, 'A'),
(107, 9, 11, 'A'),
(108, 9, 12, 'A'),
(109, 10, 1, 'A'),
(110, 10, 2, 'A'),
(111, 10, 3, 'A'),
(112, 10, 4, 'A'),
(113, 10, 5, 'A'),
(114, 10, 6, 'A'),
(115, 10, 7, 'A'),
(116, 10, 8, 'A'),
(117, 10, 9, 'A'),
(118, 10, 10, 'A'),
(119, 10, 11, 'A'),
(120, 10, 12, 'A'),
(121, 11, 1, 'A'),
(122, 11, 2, 'A'),
(123, 11, 3, 'A'),
(124, 11, 4, 'A'),
(125, 11, 5, 'A'),
(126, 11, 6, 'A'),
(127, 11, 7, 'A'),
(128, 11, 8, 'A'),
(129, 11, 9, 'A'),
(130, 11, 10, 'A'),
(131, 11, 11, 'A'),
(132, 11, 12, 'A'),
(133, 12, 1, 'A'),
(134, 12, 2, 'A'),
(135, 12, 3, 'A'),
(136, 12, 4, 'A'),
(137, 12, 5, 'A'),
(138, 12, 6, 'A'),
(139, 12, 7, 'A'),
(140, 12, 8, 'A'),
(141, 12, 9, 'A'),
(142, 12, 10, 'A'),
(143, 12, 11, 'A'),
(144, 12, 12, 'A'),
(145, 13, 1, 'A'),
(146, 13, 2, 'A'),
(147, 13, 3, 'A'),
(148, 13, 4, 'A'),
(149, 13, 5, 'A'),
(150, 13, 6, 'A'),
(151, 13, 7, 'A'),
(152, 13, 8, 'A'),
(153, 13, 9, 'A'),
(154, 13, 10, 'A'),
(155, 13, 11, 'A'),
(156, 13, 12, 'A'),
(157, 14, 1, 'A'),
(158, 14, 2, 'A'),
(159, 14, 3, 'A'),
(160, 14, 4, 'A'),
(161, 14, 5, 'A'),
(162, 14, 6, 'A'),
(163, 14, 7, 'A'),
(164, 14, 8, 'A'),
(165, 14, 9, 'A'),
(166, 14, 10, 'A'),
(167, 14, 11, 'A'),
(168, 14, 12, 'A'),
(169, 15, 1, 'A'),
(170, 15, 2, 'A'),
(171, 15, 3, 'A'),
(172, 15, 4, 'A'),
(173, 15, 5, 'A'),
(174, 15, 6, 'A'),
(175, 15, 7, 'A'),
(176, 15, 8, 'A'),
(177, 15, 9, 'A'),
(178, 15, 10, 'A'),
(179, 15, 11, 'A'),
(180, 15, 12, 'A'),
(181, 16, 1, 'A'),
(182, 16, 2, 'A'),
(183, 16, 3, 'A'),
(184, 16, 4, 'A'),
(185, 16, 5, 'A'),
(186, 16, 6, 'A'),
(187, 16, 7, 'A'),
(188, 16, 8, 'A'),
(189, 16, 9, 'A'),
(190, 16, 10, 'A'),
(191, 16, 11, 'A'),
(192, 16, 12, 'A'),
(193, 17, 1, 'A'),
(194, 17, 2, 'A'),
(195, 17, 3, 'A'),
(196, 17, 4, 'A'),
(197, 17, 5, 'A'),
(198, 17, 6, 'A'),
(199, 17, 7, 'A'),
(200, 17, 8, 'A'),
(201, 17, 9, 'A'),
(202, 17, 10, 'A'),
(203, 17, 11, 'A'),
(204, 17, 12, 'A'),
(205, 18, 1, 'A'),
(206, 18, 2, 'A'),
(207, 18, 3, 'A'),
(208, 18, 4, 'A'),
(209, 18, 5, 'A'),
(210, 18, 6, 'A'),
(211, 18, 7, 'A'),
(212, 18, 8, 'A'),
(213, 18, 9, 'A'),
(214, 18, 10, 'A'),
(215, 18, 11, 'A'),
(216, 18, 12, 'A'),
(217, 19, 1, 'A'),
(218, 19, 2, 'A'),
(219, 19, 3, 'A'),
(220, 19, 4, 'A'),
(221, 19, 5, 'A'),
(222, 19, 6, 'A'),
(223, 19, 7, 'A'),
(224, 19, 8, 'A'),
(225, 19, 9, 'A'),
(226, 19, 10, 'A'),
(227, 19, 11, 'A'),
(228, 19, 12, 'A'),
(229, 20, 1, 'A'),
(230, 20, 2, 'A'),
(231, 20, 3, 'A'),
(232, 20, 4, 'A'),
(233, 20, 5, 'A'),
(234, 20, 6, 'A'),
(235, 20, 7, 'A'),
(236, 20, 8, 'A'),
(237, 20, 9, 'A'),
(238, 20, 10, 'A'),
(239, 20, 11, 'A'),
(240, 20, 12, 'A'),
(241, 21, 1, 'A'),
(242, 21, 2, 'A'),
(243, 21, 3, 'A'),
(244, 21, 4, 'A'),
(245, 21, 5, 'A'),
(246, 21, 6, 'A'),
(247, 21, 7, 'A'),
(248, 21, 8, 'A'),
(249, 21, 9, 'A'),
(250, 21, 10, 'A'),
(251, 21, 11, 'A'),
(252, 21, 12, 'A'),
(253, 22, 1, 'A'),
(254, 22, 2, 'A'),
(255, 22, 3, 'A'),
(256, 22, 4, 'A'),
(257, 22, 5, 'A'),
(258, 22, 6, 'A'),
(259, 22, 7, 'A'),
(260, 22, 8, 'A'),
(261, 22, 9, 'A'),
(262, 22, 10, 'A'),
(263, 22, 11, 'A'),
(264, 22, 12, 'A'),
(265, 23, 1, 'A'),
(266, 23, 2, 'A'),
(267, 23, 3, 'A'),
(268, 23, 4, 'A'),
(269, 23, 5, 'A'),
(270, 23, 6, 'A'),
(271, 23, 7, 'A'),
(272, 23, 8, 'A'),
(273, 23, 9, 'A'),
(274, 23, 10, 'A'),
(275, 23, 11, 'A'),
(276, 23, 12, 'A'),
(277, 24, 1, 'A'),
(278, 24, 2, 'A'),
(279, 24, 3, 'A'),
(280, 24, 4, 'A'),
(281, 24, 5, 'A'),
(282, 24, 6, 'A'),
(283, 24, 7, 'A'),
(284, 24, 8, 'A'),
(285, 24, 9, 'A'),
(286, 24, 10, 'A'),
(287, 24, 11, 'A'),
(288, 24, 12, 'A'),
(289, 25, 1, 'A'),
(290, 25, 2, 'A'),
(291, 25, 3, 'A'),
(292, 25, 4, 'A'),
(293, 25, 5, 'A'),
(294, 25, 6, 'A'),
(295, 25, 7, 'A'),
(296, 25, 8, 'A'),
(297, 25, 9, 'A'),
(298, 25, 10, 'A'),
(299, 25, 11, 'A'),
(300, 25, 12, 'A'),
(301, 26, 1, 'A'),
(302, 26, 2, 'A'),
(303, 26, 3, 'A'),
(304, 26, 4, 'A'),
(305, 26, 5, 'A'),
(306, 26, 6, 'A'),
(307, 26, 7, 'A'),
(308, 26, 8, 'A'),
(309, 26, 9, 'A'),
(310, 26, 10, 'A'),
(311, 26, 11, 'A'),
(312, 26, 12, 'A'),
(313, 27, 1, 'A'),
(314, 27, 2, 'A'),
(315, 27, 3, 'A'),
(316, 27, 4, 'A'),
(317, 27, 5, 'A'),
(318, 27, 6, 'A'),
(319, 27, 7, 'A'),
(320, 27, 8, 'A'),
(321, 27, 9, 'A'),
(322, 27, 10, 'A'),
(323, 27, 11, 'A'),
(324, 27, 12, 'A'),
(325, 28, 1, 'A'),
(326, 28, 2, 'A'),
(327, 28, 3, 'A'),
(328, 28, 4, 'A'),
(329, 28, 5, 'A'),
(330, 28, 6, 'A'),
(331, 28, 7, 'A'),
(332, 28, 8, 'A'),
(333, 28, 9, 'A'),
(334, 28, 10, 'A'),
(335, 28, 11, 'A'),
(336, 28, 12, 'A');

-- --------------------------------------------------------

--
-- Table structure for table `pilots`
--

CREATE TABLE `pilots` (
  `pilotID` int(11) NOT NULL,
  `pilotName` varchar(50) NOT NULL,
  `pilotAddress` varchar(100) NOT NULL,
  `pilotPhone` char(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pilots`
--

INSERT INTO `pilots` (`pilotID`, `pilotName`, `pilotAddress`, `pilotPhone`) VALUES
(1000, 'Charlie Brown', '677 Silly Circle', '9998217432'),
(1001, 'Linus', '9211 Blankey Ave', '1123357766'),
(1002, 'Sally Brown', '677 Silly Circle', '9995539999');

-- --------------------------------------------------------

--
-- Table structure for table `pilot_flights`
--

CREATE TABLE `pilot_flights` (
  `pilotFlightID` int(11) NOT NULL,
  `pilotID` int(11) NOT NULL,
  `flightID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pilot_flights`
--

INSERT INTO `pilot_flights` (`pilotFlightID`, `pilotID`, `flightID`) VALUES
(1, 1000, 1),
(2, 1001, 2),
(3, 1002, 3),
(4, 1000, 4),
(5, 1001, 5),
(6, 1002, 6),
(7, 1000, 7),
(8, 1001, 8),
(9, 1002, 9),
(10, 1000, 10),
(11, 1001, 11),
(12, 1002, 12),
(13, 1000, 13),
(14, 1001, 14),
(15, 1002, 15),
(16, 1000, 16),
(17, 1001, 17),
(18, 1002, 18),
(19, 1000, 19),
(20, 1001, 20),
(21, 1002, 21),
(22, 1000, 22),
(23, 1001, 23),
(24, 1002, 24),
(25, 1000, 25),
(26, 1001, 26),
(27, 1002, 27),
(28, 1000, 28);

-- --------------------------------------------------------

--
-- Table structure for table `reservations`
--

CREATE TABLE `reservations` (
  `reservationID` int(11) NOT NULL,
  `seat_type` varchar(5) NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT 0,
  `flightID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `reservations`
--

INSERT INTO `reservations` (`reservationID`, `seat_type`, `deleted`, `flightID`) VALUES
(1, 'F', 0, 12);

-- --------------------------------------------------------

--
-- Table structure for table `reservation_seats`
--

CREATE TABLE `reservation_seats` (
  `reservationSeatID` int(11) NOT NULL,
  `reservationID` int(11) NOT NULL,
  `seatNumber` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`custID`);

--
-- Indexes for table `customer_reservations`
--
ALTER TABLE `customer_reservations`
  ADD PRIMARY KEY (`customerReservationID`),
  ADD KEY `custID` (`custID`),
  ADD KEY `reservationID` (`reservationID`);

--
-- Indexes for table `flights`
--
ALTER TABLE `flights`
  ADD PRIMARY KEY (`flightID`);

--
-- Indexes for table `flight_seats`
--
ALTER TABLE `flight_seats`
  ADD PRIMARY KEY (`flightSeatID`),
  ADD KEY `flightID` (`flightID`);

--
-- Indexes for table `pilots`
--
ALTER TABLE `pilots`
  ADD PRIMARY KEY (`pilotID`);

--
-- Indexes for table `pilot_flights`
--
ALTER TABLE `pilot_flights`
  ADD PRIMARY KEY (`pilotFlightID`),
  ADD KEY `pilotID` (`pilotID`),
  ADD KEY `flightID` (`flightID`);

--
-- Indexes for table `reservations`
--
ALTER TABLE `reservations`
  ADD PRIMARY KEY (`reservationID`),
  ADD KEY `flightID` (`flightID`);

--
-- Indexes for table `reservation_seats`
--
ALTER TABLE `reservation_seats`
  ADD PRIMARY KEY (`reservationSeatID`),
  ADD KEY `reservationID` (`reservationID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `custID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `customer_reservations`
--
ALTER TABLE `customer_reservations`
  MODIFY `customerReservationID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `flights`
--
ALTER TABLE `flights`
  MODIFY `flightID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `flight_seats`
--
ALTER TABLE `flight_seats`
  MODIFY `flightSeatID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=337;

--
-- AUTO_INCREMENT for table `pilot_flights`
--
ALTER TABLE `pilot_flights`
  MODIFY `pilotFlightID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `reservations`
--
ALTER TABLE `reservations`
  MODIFY `reservationID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `reservation_seats`
--
ALTER TABLE `reservation_seats`
  MODIFY `reservationSeatID` int(11) NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `customer_reservations`
--
ALTER TABLE `customer_reservations`
  ADD CONSTRAINT `customer_reservations_ibfk_1` FOREIGN KEY (`custID`) REFERENCES `customers` (`custID`),
  ADD CONSTRAINT `customer_reservations_ibfk_2` FOREIGN KEY (`reservationID`) REFERENCES `reservations` (`reservationID`);

--
-- Constraints for table `flight_seats`
--
ALTER TABLE `flight_seats`
  ADD CONSTRAINT `flight_seats_ibfk_1` FOREIGN KEY (`flightID`) REFERENCES `flights` (`flightID`);

--
-- Constraints for table `pilot_flights`
--
ALTER TABLE `pilot_flights`
  ADD CONSTRAINT `pilot_flights_ibfk_1` FOREIGN KEY (`pilotID`) REFERENCES `pilots` (`pilotID`),
  ADD CONSTRAINT `pilot_flights_ibfk_2` FOREIGN KEY (`flightID`) REFERENCES `flights` (`flightID`);

--
-- Constraints for table `reservations`
--
ALTER TABLE `reservations`
  ADD CONSTRAINT `reservations_ibfk_1` FOREIGN KEY (`flightID`) REFERENCES `flights` (`flightID`);

--
-- Constraints for table `reservation_seats`
--
ALTER TABLE `reservation_seats`
  ADD CONSTRAINT `reservation_seats_ibfk_1` FOREIGN KEY (`reservationID`) REFERENCES `reservations` (`reservationID`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
