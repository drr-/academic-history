package checkpointThree;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;
import checkpointThree.JDBCConnection;

public class EAMethodsDB {

	static Connection conn = null;
	static Statement stmt = null;
	static CallableStatement call = null;
	private static Scanner scan = new Scanner(System.in);

	public static Connection createConnection() {

		String user = "itp220";
		String pass = "itp220";
		String name = "eagles";
		String driver = "com.mysql.jdbc.Driver";
		String url = "jdbc:mysql://localhost:3306/" + name;

		System.out.println(driver);
		System.out.println(url);

		try {
			Class.forName(driver).newInstance();
			conn = DriverManager.getConnection(url, user, pass);
			System.out.println("Connection really is from : " + conn.getClass().getName());
			System.out.println("Connection successful!");

		} catch (Exception e) {
			e.printStackTrace();
		}
		return conn;
	}

	public static void closeConnection() {
		if (conn != null) {
			try {
				conn.close();
				conn = null;
				// stmt.close();
				System.out.println("The connection was successfully closed");
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	public static void checkConnect() {
		if (conn == null) {
			conn = createConnection();
		}
		if (stmt == null) {
			try {
				stmt = conn.createStatement();
			} catch (SQLException e) {
				System.out.println("Cannot create the statement");
			}

		}
	}

	public void printCustomers() {

		checkConnect();
		String stored = "{call print_cust ()}";

		try {
			call = conn.prepareCall(stored);
			ResultSet rs = call.executeQuery();

			System.out.println("Customer Information:");
			System.out.println();

			while (rs.next()) {

				int custID = rs.getInt(1);
				String name = rs.getString(2);
				String address = rs.getString(3);
				String phone = rs.getString(4);

				System.out.println(
						"Customer ID: " + custID + "  Name: " + name + "   Address: " + address + "   Phone: " + phone);
			}

		}

		catch (SQLException e) {
			System.out.println("SQL Exception");
			e.printStackTrace();
		}

	}

	public void printPilots() {

		checkConnect();
		String stored = "{call print_pilots ()}";

		try {
			call = conn.prepareCall(stored);
			ResultSet rs = call.executeQuery();

			System.out.println("Pilot Information:");
			System.out.println();

			while (rs.next()) {

				int pilotID = rs.getInt(1);
				String pilotName = rs.getString(2);
				String pilotAddress = rs.getString(3);
				String pilotPhone = rs.getString(4);

				System.out.println("Pilot ID: " + pilotID + "  Name: " + pilotName + "   Address: " + pilotAddress
						+ "   Phone: " + pilotPhone);
			}

		}

		catch (SQLException e) {
			System.out.println("SQL Exception");
			e.printStackTrace();
		}

	}

	public void findCustInfo() {
		checkConnect();

		Scanner scan = new Scanner(System.in);

		System.out.println("Please enter a customer ID:");
		int id = scan.nextInt();

		String query = "SELECT * FROM customers WHERE customers.custID = " + id;
		try {
			stmt = conn.createStatement();

			ResultSet rs = stmt.executeQuery(query);
			System.out.println("Customer Information:");
			System.out.println();

			boolean found = false;
			while (rs.next()) {

				int custID = rs.getInt(1);
				String name = rs.getString(2);
				String address = rs.getString(3);
				String phone = rs.getString(4);

				if (custID == id)
					;
				System.out.println(
						"Customer ID: " + custID + "  Name: " + name + "   Address: " + address + "   Phone: " + phone);
				found = true;
			}

			if (!found) {
				System.out.println("That customer was not found");

			}

		}

		catch (SQLException e) {
			System.out.println("SQL Exception");
			e.printStackTrace();
		}

	}

	public void addNewCust() {
		
		checkConnect();

		String yOrN = "y";

		while (yOrN.equals("y")) {
			Scanner scan = new Scanner(System.in);
			System.out.println("Customer's name?");
			String name = scan.nextLine();
			System.out.println("Customer's address?");
			String address = scan.nextLine();
			System.out.println("Customer's phone number?");
			String phone = scan.nextLine();

			String queryAddCustomer = "INSERT INTO customers (name, address, phone) VALUES ('" + name + "','" + address
					+ "','" + phone + "')";

			try {
				stmt = conn.createStatement();
				stmt.executeUpdate(queryAddCustomer);
				System.out.println("Customer added!");
			} catch (SQLException e) {
				System.out.println("SQL Exception");
				e.printStackTrace();
			}

			System.out.println("More Customers? (y/n)");
			yOrN = scan.next();

		}

	}

	public void printPilotSchedule() {
		
		checkConnect();

		Scanner scan = new Scanner(System.in);

		System.out.println("Please enter a pilot ID:");
		int pId = scan.nextInt();

		String query = "SELECT flights.flightCode, pilot_flights.pilotID, pilots.pilotName" 
				+ " FROM flights\r\n" 
				+ " INNER JOIN pilot_flights on flights.flightID = pilot_flights.flightID" 
				+ " INNER JOIN pilots on pilot_flights.pilotID = pilots.pilotID" 
				+ " WHERE pilots.pilotID = " + pId;
		try {
			stmt = conn.createStatement();

			ResultSet rs = stmt.executeQuery(query);
			System.out.println("Pilot Schedule");
			System.out.println();

			boolean found = false;
			while (rs.next()) {

						
				found = true;
			}

			if (!found) {
				System.out.println("That pilot was not found");

			}

		}

		catch (SQLException e) {
			System.out.println("SQL Exception");
			e.printStackTrace();
		}

	
		
	}

}