package checkpointThree;


import java.sql.Connection;
import java.util.Scanner;



public class EADriverDB {

	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);
		EAMethodsDB bm = new EAMethodsDB();

		Connection conn;

		boolean more = true;
		while (more) {
			menu();
			System.out.println("Choice:");
			int ans = scan.nextInt();
			if (ans == 0)
				conn = bm.createConnection();
			
			else if (ans == 1)
				bm.findCustInfo();
			else if (ans==2)
				bm.addNewCust();
			else if (ans==3)
				bm.printPilotSchedule();
			else if (ans == 13)
				bm.printCustomers();
			else if (ans == 14)
				bm.printPilots();
			
			else {
				System.out.println("BYE!!!");
				more = false;
				System.exit(0);
			}

		}

	}

	public static void menu() {
		System.out.println("Choose one:");
		System.out.println("   1. Find Customer information");
		System.out.println("   2. Add New Customer");
		System.out.println("   13. Print out all of the Customer information");
		System.out.println("   14. Print out all of the Pilot information");
		
		System.out.println("   999. Exit");
	}
}
