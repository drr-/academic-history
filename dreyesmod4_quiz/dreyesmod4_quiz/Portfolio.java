package dreyesmod4_quiz;

import java.time.LocalDate;
import java.util.Scanner;

public class Portfolio {
	private Stack<Stock> stocks;
	private double capGains;
	
	public Portfolio() {
		super();
	}
	
	public Portfolio(Stack<Stock> stocks, double capGains) {
		super();
		this.stocks = stocks;
		this.capGains = capGains;
	}
	
	public String toString() {
		stocks.print();
		return " Current capital gains: " + capGains + "\n";
	}	
	
	
	public void buy() {
		Scanner scan = new Scanner(System.in);
		System.out.println("Year:");
		int y = scan.nextInt();
		System.out.println("Month:");
		int m = scan.nextInt();
		System.out.println("Day:");
		int d = scan.nextInt();
		System.out.println("Number of shares:");
		double num = scan.nextDouble();
		System.out.println("Price per share:");
		double cost = scan.nextDouble();
		Stock st = new Stock (LocalDate.of(y, m, d),num,cost);
		stocks.push(st);
	}
	
	public void sell(double numShares, double cost) {
		Stack<Stock> thoseSold = new Stack<>();
		double amtCollected = numShares * cost;
		System.out.println("The sale of " + numShares + " @$" + cost + " gives you $" + amtCollected + " cash");
		double basis = 0;
		double sharesLeftToSell = numShares;
		
		Scanner scan = new Scanner(System.in);
		
		thoseSold.push(new Stock(LocalDate.of(2023,4,12),numShares,cost));
		
		
		
		while (sharesLeftToSell > 0) {
			Stock reko = getStocks().peek();
			
			if (reko.getNumShares() > sharesLeftToSell) {
				reko.setNumShares(reko.getNumShares()-sharesLeftToSell);
				sharesLeftToSell = 0;
			} else {
				getStocks().pop();
				sharesLeftToSell = (sharesLeftToSell-reko.getNumShares());
			}
		}
		
		
		
		capGains = capGains + (amtCollected - basis);
		
		System.out.println("You sold the following stocks:");
		thoseSold.print();
	}
	public Stack<Stock> getStocks() {
		return stocks;
	}
	public void setStocks(Stack<Stock> stocks) {
		this.stocks = stocks;
	}
	public double getCapGains() {
		return capGains;
	}
	public void setCapGains(double capGains) {
		this.capGains = capGains;
	}
	
	
	
	


}
