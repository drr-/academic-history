package dreyesmod6_csc223;

import java.util.Scanner;

public class SubstringRec {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);	
		System.out.println("What is the string?");
		String s = scan.nextLine();
		System.out.println("What substring are you looking for?");
		String sub = scan.nextLine();
		boolean found = find(s,sub);
		if (found)
			System.out.println("It was found!");
		else
			System.out.println("It was not found");

	}
	
	public static boolean find2(String text, String target)
	{
	 //-----------Start below here. To do: approximate lines of code = 4
	 // 1. base case: null 
	 if (text == null || target == null) {return false;}  // added target null check       
	 //2. base case: target too long 
	 if (target.length() > text.length()) {return false;}
	 //3. base case: same length 
	 if (text.length() == target.length()) {return text.equals(target);}
	 //4. base case: startsWith  OR 5. recursive case
	 return text.startsWith(target) || find(text.substring(1) , target);
	}
	
	
	public static boolean find(String text, String target)
	{
		if (target.length()>text.length()) {
			//System.out.println("The target cannot be longer than the text");
			return false;
		}
		else if	 (text.startsWith(target))
			return true;
		else
			return find(text.substring(1) , target);
	}
}