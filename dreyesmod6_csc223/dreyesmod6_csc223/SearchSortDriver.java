package dreyesmod6_csc223;

import java.awt.FileDialog;
import java.awt.Frame;
import java.io.File;
import java.io.FileNotFoundException;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;


public class SearchSortDriver {
	static ArrayList bacon = new ArrayList<>();
	static Scanner scan = new Scanner(System.in);

	public static void main(String[] args) {
		
				
		int[] data = new int[10]; // create array size 10
		
		
		
		boolean more = true;
		while(more) {
			menu();
			System.out.println("Choice:");
			int choice = scan.nextInt();
			
			if (choice ==1) {
				prime(data);
			}
			else if (choice ==2) {
				print(data);
			}
			else if (choice ==3) {
				printAll(data);
			}
			else if (choice ==4) {
				System.out.print("What is the name?");
				String bbb = scan.next();
				System.out.println("What is the year?");
				int num = scan.nextInt();
				int loc = LinearSearch.linearSearch(bacon, bbb, num) ;
				if (loc>=0)
					System.out.println("The value was found at location " + loc);
				else 
					System.out.println("The value is not in the array");
			}
			else if (choice==5) {
				System.out.print("What is the name?");
				String bbb = scan.next();
				System.out.println("What is the year?");
				int num = scan.nextInt();
				int loc = BinarySearch.binarySearch(bacon, bbb, num) ;
				if (loc>=0)
					System.out.println("The value was found at location " + loc);
				else 
					System.out.println("The value is not in the array");
/*				
				System.out.println("What value are you searching for?");
				int num = scan.nextInt();
				int loc = BinarySearch.binarySearch(data, num) ;
				if (loc>=0)
					System.out.println("The value was found at location " + loc);
				else 
					System.out.println("The value is not in the array");
*/
			}
			else if (choice==6) {
				System.out.println("What value are you searching for?");
/*				int num = scan.nextInt();
				int loc = BinarySearchRecursive.recursiveBinarySearch(data,0,data.length, num) ;
				if (loc>=0)
					System.out.println("The value was found at location " + loc);
				else 
					System.out.println("The value is not in the array");
*/
			}
			else if (choice == 7) {
//				MergeSort.mergeSort(data);
			}
			
			else if (choice == 8) {
//				InsertionSort.insertionSort(data);
			}
			else if (choice == 9) {
//				SelectionSort.selectionSort(data);
			}
			else if (choice == 10) {
				ArrayList<Integer> list = new ArrayList<Integer>();
				for (int i=0;i<data.length;i++)
					list.add(data[i]);
				Collections.shuffle(list);
				for (int i=0;i<data.length;i++)
					data[i] = list.get(i);
					
				
			}
			else if (choice == 72) {
				String[] sN = sortSort(bacon);
			}
			else {
				System.out.println("BYE!!");
				System.exit(0);
			}

		}
	}
	
	private static String[] sortSort(ArrayList bacon2) {
		
		ArrayList<String> names = new ArrayList<>();
		
		for (int f = 0; f < bacon.size(); f++) {
			
			String[] A = (String[])bacon.get(f);
			
			String NE = A[0]+"+"+A[3];
			
			names.add(NE);
		}
		
		Collections.sort(names);
		
		String[] newArray = names.toArray(new String[0]);

		for (int f = 0; f < newArray.length -1; f++) {
			//System.out.println(newArray[f].getBytes());
			
			String s = newArray[f];
			StringBuilder buf = new StringBuilder();
			for (int i = 0; i < s.length(); i++ ) {
			    int characterASCII = s.charAt(i);
			    // Automatic type casting as 'char' is a type of 'int'
			    buf.append(Integer.toBinaryString(characterASCII));
			    // or:
			    // buf.append(characterASCII, 2);
			    // or - custom method:
			    // buf.append(toBinary(characterASCII));
			}
			System.out.println("answer : " + buf.toString());

			
		}
		
		return newArray;
	}

	public static void prime(int[] values) {
		SecureRandom generator = new SecureRandom();
		for (int i = 0; i < values.length; i++) // populate array
	         values[i] = 10 + generator.nextInt(90);
		
        //ArrayList<Employee> emp;
        Frame f = new Frame();
		// decide from where to read the file
		FileDialog foBox = new FileDialog(f, "Pick location for reading your file", FileDialog.LOAD);
		System.out.println("The dialog box will appear behind Eclipse.  " + 
		      "\n   Choose where you would like to read from.");
		foBox.setVisible(true);
		// get the absolute path to the file
		String foName = foBox.getFile();
		String dirPath = foBox.getDirectory();

		// create a file instance for the absolute path
		File inFile = new File(dirPath + foName);
		//File inFile = new File("/home/user/QubesIncoming/College/employeeData.csv");
		if (!inFile.exists()) {
			System.out.println("That file does not exist");
			System.exit(0);
		}
        Scanner employeeFile = null;
		try {
			employeeFile = new Scanner(inFile);
//			employeeFile.useDelimiter(",");
		} catch (FileNotFoundException e) {
			
			e.printStackTrace();
		}
            //emp = new ArrayList<Employee>();
			int eaSports = 0;
            while (employeeFile.hasNextLine())
            {
            	if (eaSports == 0) {
            		eaSports++;
            		continue;
            	}
            	
            	String ILikeBase = employeeFile.next();
            	String[] ea = ILikeBase.split(",");
            	
//            	System.out.println(ea[0] + ea[1] + ea[2] + ea[3] + "-\n");

               employeeFile.nextLine();
               bacon.add(ea);
            }
       
      System.out.println("The data was read from the file into the array list");
      employeeFile.close();    

	}
	
	public static void print(int[] values) {
		
		System.out.println("Enter Year: ");
		int WotYear = scan.nextInt();
		
		ArrayList<String> names = new ArrayList<>();
		
		for (int f = 0; f < bacon.size(); f++) {
			
			String[] A = (String[])bacon.get(f);
			
			if (Integer.valueOf(A[3]) == WotYear) {
				names.add(A[0]);
			}
		}
		
		Collections.sort(names);
		
		System.out.println("");
		for (int f = 0; f < names.size(); f++) {
			System.out.println(names.get(f));
		}
		
	}
	
public static void printAll(int[] values) {
		for (int f = 0; f < bacon.size(); f++) {
			
			String[] A = (String[])bacon.get(f);
			
			System.out.println(A[0]);
		}
		
	}
	
	
	public static void menu()
    {
        System.out.println("\n1.  Prime the collection with new data"); 
        System.out.println("2.  Print out all employees boorn in a given year");
        System.out.println("3.  Print out the collection");
        System.out.println("4.  Do a generic linear search for an item");
        System.out.println("5.  Do a generic non-recursion based binary search for an item");
        System.out.println("    NOTE!! Data must be sorted first before choosing 4");
        System.out.println("6.  Do a generic recursion based binary search for an item");
        System.out.println("    NOTE!! Data must be sorted first before choosing 5");
        System.out.println("7.  Merge sort the collection");
        System.out.println("8.  Insertion sort the collection");
        System.out.println("9.  Selection sort the collection");
        System.out.println("10.  Randomly shuffle the data in the list");
        System.out.println("11.  exit\n"); 
    }

}
