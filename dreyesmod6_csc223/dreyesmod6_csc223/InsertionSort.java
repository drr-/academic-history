package dreyesmod6_csc223;

import java.util.ArrayList;

public class InsertionSort {
    // sort array using insertion sort
    public static void insertionSort(ArrayList<String> array) {
        // loop over data.length - 1 elements           
        for (int next = 1; next < array.size(); next++) {
            String insert = array.get(next); // value to insert
            int moveItem = next; // location to place element

            // search for place to put current element             
            while (moveItem > 0 && array.get(moveItem - 1).compareTo(insert) > 0) {
                // shift element right one slot         
                array.set(moveItem, array.get(moveItem - 1));
                moveItem--;                             
            } 

            array.set(moveItem, insert); // place inserted element    
            printPass(array, next, moveItem); // output pass of algorithm
        }                                             
   }

   // print a pass of the algorithm
    public static void printPass(ArrayList<String> array, int pass, int index) {
        System.out.printf("after pass %2d: ", pass);

        // output elements till swapped item
        for (int i = 0; i < index; i++)
            System.out.printf("%-7s  ", array.get(i));

        System.out.printf("%-7s* ", array.get(index)); // indicate swap

        // finish outputting array
        for (int i = index + 1; i < array.size(); i++)
            System.out.printf("%-7s  ", array.get(i));

        System.out.printf("%n               "); // for alignment

        // indicate amount of array that�s sorted
        for(int i = 0; i <= pass; i++)
            System.out.print("-------  ");
        System.out.println();
    } 
}