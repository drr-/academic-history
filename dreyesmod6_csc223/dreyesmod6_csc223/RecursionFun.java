package dreyesmod6_csc223;

import java.util.Scanner;

public class RecursionFun {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		System.out.println("Give me a string:");
		String value = scan.nextLine();
		char[] chars = value.toCharArray();
		int choice = 0;
		while (choice !=9) {
			choice = menu();
			
			if (choice==1)
				reverse(chars);
			else if (choice ==2)
				findSubString(value);
			else if (choice==3)
				countChar(chars);
			else {
				System.out.println("Thanks for using my program");
				System.exit(0);
			}				
		}
	}
	
	public static int menu() {
		Scanner scan = new Scanner(System.in);
		System.out.println("MENU:  (must use recursion for the solutions!)");
		System.out.println("1.  Reverse the String");
		System.out.println("2.  Look for a substring in the string");
		System.out.println("3.  Count how many times a character occurs in the string");
		System.out.println("9.  End");
		System.out.println("CHOICE:");
		int ans = scan.nextInt();
		return ans;
	}
	
	public static void reverse(char[] chars) {

		  stringReverseHelper(chars, 0);
		      System.out.println();
	  }
	
	private static void stringReverseHelper(char[] array, int index)
	   {
	      // reached beginning of string
	      if (index == array.length)
	         return;

	      stringReverseHelper(array, index + 1);
	      System.out.print(array[index]);
	   } 	   

	
	public static boolean find(String text, String target)
	{
		if (target.length()>text.length()) {
			System.out.println("The target cannot be longer than the text");
			return false;
		}
		else	 
			return text.startsWith(target) || find(text.substring(1) , target);
	}
	
	public static void findSubString(String s) {
		Scanner scan = new Scanner(System.in);		
		System.out.println("What substring are you looking for?");
		String sub = scan.nextLine();
		boolean found = find(s,sub);
		if (found)
			System.out.println("It was found!");
		else
			System.out.println("It was not found");
		
		
	}
	
	public static void countChar(char[] chars) {
		Scanner scan = new Scanner(System.in);
		
		System.out.println("What character are you looking for?");
		char which =scan.nextLine().charAt(0);
		System.out.println("The character " + which + " appears " +
                charCount(chars, 0, which) + " times.");
	}
	
	public static int charCount(char[] array, int start, char ch)
	{
		int numChars = 0;		
		if (start == (array.length))
			return 0;
		else
		{
			if (array[start] == ch)
				numChars = 1;
			return numChars + charCount(array, start + 1, ch);
		}
	}

	
}
