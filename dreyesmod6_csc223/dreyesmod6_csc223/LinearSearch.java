package dreyesmod6_csc223;
//LinearSearch.java
//Sequentially searching an array for an item.
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class LinearSearch
{
    

// perform a linear search on the data              
public static int linearSearch(ArrayList bacon, String bane, int searchKey)       
{                      
	for (int f = 0; f < bacon.size(); f++) {
		
		String[] A = (String[])bacon.get(f);
		
		if ((Integer.valueOf(A[3]) == searchKey) && (A[0].equals(bane))) {
			return f;
		}
	}
	
  return -1; // integer was not found
} // end method linearSearch          

/*
public static void main(String[] args)
{
  Scanner input = new Scanner(System.in);
  SecureRandom generator = new SecureRandom();

  int[] data = new int[10]; // create array

  for (int i = 0; i < data.length; i++) // populate array
     data[i] = 10 + generator.nextInt(90);

  System.out.printf("%s%n%n", Arrays.toString(data)); // display array

  // get input from user
  System.out.print("Please enter an integer value (-1 to quit): ");
  int searchInt = input.nextInt(); 

  // repeatedly input an integer; -1 terminates the program
  while (searchInt != -1)
  {
     int position = linearSearch(data, "e", searchInt); // perform search

     if (position == -1) // not found
        System.out.printf("%d was not found%n%n", searchInt); 
     else // found
        System.out.printf("%d was found in position %d%n%n", 
           searchInt, position);

     // get input from user
     System.out.print("Please enter an integer value (-1 to quit): ");
     searchInt = input.nextInt();
  } 
} // end main

*/

} // end class LinearSearch

