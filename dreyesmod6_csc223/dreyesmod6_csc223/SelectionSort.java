package dreyesmod6_csc223;

import java.util.ArrayList;

public class SelectionSort {
    // sort array using selection sort
    public static void selectionSort(ArrayList<String> array) {
        // loop over data.length - 1 elements      
        for (int i = 0; i < array.size() - 1; i++) {
            int smallest = i; // first index of remaining array

            // loop to find index of smallest element              
            for (int index = i + 1; index < array.size(); index++)
                if (array.get(index).compareTo(array.get(smallest)) < 0)             
                    smallest = index;                                

            swap(array, i, smallest); // swap smallest element into position
            printPass(array, i + 1, smallest); // output pass of algorithm  
        }                                          
    }                                    

   // helper method to swap values in two elements
    private static void swap(ArrayList<String> array, int first, int second) {
        String temporary = array.get(first); // store first in temporary
        array.set(first, array.get(second)); // replace first with second
        array.set(second, temporary); // put temporary in second
    } 

   // print a pass of the algorithm
    private static void printPass(ArrayList<String> array, int pass, int index) {
        System.out.printf("after pass %2d: ", pass);

        // output elements till selected item
        for (int i = 0; i < index; i++)
            System.out.printf("%-7s  ", array.get(i));

        System.out.printf("%-7s* ", array.get(index)); // indicate swap

        // finish outputting array
        for (int i = index + 1; i < array.size(); i++)
            System.out.printf("%-7s  ", array.get(i));

        System.out.printf("%n               "); // for alignment

        // indicate amount of array that�s sorted
        for (int j = 0; j < pass; j++)
            System.out.print("-------  ");
        System.out.println(); 
    } 
}