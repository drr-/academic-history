package dreyesmod7_lab_csc223;

import java.awt.FileDialog;
import java.awt.Frame;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.NoSuchElementException;
import java.util.Scanner;

import java.util.*;

public class War_Peace {
	
	public static Scanner data = null;
	public static Scanner scan = new Scanner(System.in);
	public static ArrayList<String> original = new ArrayList<>();
	public static ArrayList<String> clean = new ArrayList<>();
	public static ArrayList<String> stevenUniverse = new ArrayList<>();
	
	public static String pickLocRead() {
		Frame f = new Frame();
		// decide from where to read the file
		FileDialog fiBox = new FileDialog(f, "Pick location for reading your file", FileDialog.LOAD);
		System.out.println("The dialog box might appear behind Eclipse.  " + 
			      "\n   Choose where you would like to read for your data.");
		fiBox.setVisible(true);
		// get the absolute path to the file
		String fiName = fiBox.getFile();
		String dirPath = fiBox.getDirectory();

		// create a file instance for the absolute path
		String name = dirPath + fiName;
		return name;
	}
	
	public static void openFile() {
		try {
			File file = new File(pickLocRead());
			//File file = new File("/home/user/sample.txt");
			data = new Scanner(file);
			//data.useDelimiter(" - ");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	public static void readRecords(ArrayList<String> disYoYo) {
		try {
			while (data.hasNextLine()) {
				
				disYoYo.add(data.nextLine());
				
			}
			closeFile();
		} catch (NoSuchElementException elementException) {
			System.err.println("File improperly formed. Terminating.");
		} catch (IllegalStateException stateException) {
			System.err.println("Error reading from file. Terminating.");
		}
	   }
	
	public static void closeFile() {
		if (data != null)
			data.close();
	}
	
	

	public static void main(String[] args) {
		boolean more = true;
		while(more) {
			menu();
			System.out.println("Choice:");
			int ans = scan.nextInt();
			if (ans == 1)
				prog_loader();
			else if (ans == 2)
				prog_sanitize(original);
			else if (ans==3)
				prog_wordle(clean);
			else if (ans == 4)
				prog_myIdeas(clean);
			else if (ans == 5)
				prog_williamOsman(clean);
			else if (ans == 6)
				prog_googling(clean);
			else if (ans == 7)
				prog_ewMathGoshDarnItImAlreadyTakingAClassOnItAhhhhhh(clean);
			else if (ans == 8)
				prog_IActuallyThoughtWeWereGoingToUseRealEncryptionForASecIPersonallyUseAES256GCM(original);
			else if (ans == 9)
				prog_bacon();
			else {
				System.out.println("BYE!!!");
				more = false;
				System.exit(0);
			}
				
			
		}
		
		

	}
	
	private static String randBrandFan() {
	    String alphabet = "AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz";

	    List<Character> characters = new ArrayList<Character>();
        for(char c:alphabet.toCharArray()){
            characters.add(c);
        }
        StringBuilder output = new StringBuilder(alphabet.length());
        while(characters.size()!=0){
            int randPicker = (int)(Math.random()*characters.size());
            output.append(characters.remove(randPicker));
        }
        
        return output.toString();
	}
	
	
	private static void prog_bacon() {
		System.out.print("Enter Key: ");
		String fortyTwo = scan.next();
		
		String[] keyArray = fortyTwo.split("");
		
		String[] charzar = {"a", "b", "c", "d", "e", "f", "h", "j", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"};
		
		HashMap<String, String> mapping = new HashMap<String, String>();
		
		for (int i = 0; i < charzar.length; i++) {
			mapping.put(keyArray[i], charzar[i]);
		}
		
		if (stevenUniverse.size() == 0) {
			openFile();
			readRecords(stevenUniverse);
		}
		
		
		for (int x = 0; x < stevenUniverse.size(); x++) {
			String[] tmpString = stevenUniverse.get(x).split("");
			
			//System.out.println("Enc: " + stevenUniverse.get(x));
			
			String[] fuckoff = new String[tmpString.length];
			for (int y = 0; y < tmpString.length; y++) {
				if (tmpString[y].matches("[a-zA-Z]")) {
					fuckoff[y] = mapping.get(tmpString[y]);
					//System.out.print(mapping.get(tmpString[y]));
				} else {
					fuckoff[y] = tmpString[y];
				}
				//System.out.println("");
			}
			
			
			
			StringBuilder builder = new StringBuilder();
			for (String value : fuckoff) {
			    builder.append(value);
			}
			String disString2 = builder.toString();
			
			if (x <= 30) {
				System.out.println(disString2);
			}
		}
		
	}

	private static void prog_IActuallyThoughtWeWereGoingToUseRealEncryptionForASecIPersonallyUseAES256GCM(ArrayList<String> data) {
		File outFile = new File(pickLocRead());
		//File outFile = new File("/home/user/outputhere.txt");
		
		PrintWriter saveHere = null;
		try {
			saveHere = new PrintWriter(outFile);
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		}
		
		
		
		String key = randBrandFan();
		String[] keyArray = key.split("");
		
		String[] charzar = {"a", "b", "c", "d", "e", "f", "h", "j", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"};
		
		HashMap<String, String> mapping = new HashMap<String, String>();
		
		for (int i = 0; i < charzar.length; i++) {
			mapping.put(charzar[i], keyArray[i]);
			
			//System.out.println(charzar[i] + " - " + keyArray[i]);
		}
		//System.out.println(mapping.get("c"));
		
		System.out.println("Key: " + key + "");
		
		for (int x = 0; x < data.size(); x++) {
			String[] tmpString = data.get(x).split("");
			
			String[] noMoSo = new String[tmpString.length];
			for (int y = 0; y < tmpString.length; y++) {
				if (tmpString[y].matches("[a-zA-Z]")) {
					noMoSo[y] = mapping.get(tmpString[y]);
				} else {
					noMoSo[y] = tmpString[y];
				}
			}
			
			
			
			StringBuilder builder = new StringBuilder();
			for (String value : noMoSo) {
			    builder.append(value);
			}
			String disString = builder.toString();
			
			stevenUniverse.add(disString);
			saveHere.println(disString);
			
			if (x <= 30) {
				System.out.println(disString);
			}
		}
		
		saveHere.close();
	}

	private static void prog_ewMathGoshDarnItImAlreadyTakingAClassOnItAhhhhhh(ArrayList<String> data) {
		ArrayList<String> tmp = new ArrayList<>();
		
		for (int x = 0; x < data.size(); x++) {
			String[] tmpString = data.get(x).split(" ");
			
			for (int y = 0; y < tmpString.length; y++) {
				int found = -1;
				
				for (int s = 0; s < tmp.size(); s++) {
					String[] merk = tmp.get(s).split("-");
					
					if (tmpString[y].equals(merk[1])) {
						found = s;
					}
				}
				
				if (found != -1) {
					String[] smerk = tmp.get(found).split("-");
					int New = Integer.valueOf(smerk[0]);
					New++;
					
					String dontBeEvil = String.valueOf(New);
					
					StringBuilder sb = new StringBuilder();
				    while (sb.length() < 3 - dontBeEvil.length()) {
				        sb.append('0');
				    }
				    sb.append(dontBeEvil);
					
					String hahahaYeahRight = sb.toString();
					
					tmp.set(found, hahahaYeahRight+"-"+smerk[1]);
				} else {
					tmp.add("001-"+tmpString[y]);
				}
				
				
			}
		}
		
		Collections.sort(tmp);
		
		System.out.println("TOP TEN USED WORDS!!!!!!!!!!!!!!!!");
		
		int places = 1;
		for (int x = tmp.size(); x > tmp.size()-10; x--) {
			String[] yes = tmp.get(x-1).split("-");
			System.out.println(places + ": the word '" + yes[1] + "'   \twas used \t" + Integer.valueOf(yes[0]) + " times!");
			places++;
		}
		
		System.out.println("\nWoah, yall, thats crazy! so many words :O\nThank you for watching my yt video :)\nplz remember to subscribe. B)");
	}

	private static void prog_googling(ArrayList<String> data) {
		System.out.print("Enter word to find: ");
		String www = scan.next();
		
		int found = 0;
		for (int x = 0; x < data.size(); x++) {
			String[] tmpString = data.get(x).split(" ");
			
			for (int y = 0; y < tmpString.length; y++) {
				if (tmpString[y].equals(www)) {
					found++;
				}
			}
		}
		
		System.out.println("Word: '" + www + "' was found " + found + " times");
		
	}

	private static void prog_williamOsman(ArrayList<String> data) {
		ArrayList<Integer> tmp = new ArrayList<>();
		
		for (int x = 0; x < data.size(); x++) {
			String[] tmpString = data.get(x).split(" ");
			
			for (int y = 0; y < tmpString.length; y++) {
				
				if (tmp.size() < tmpString[y].length()) {
					int NumBun = tmpString[y].length()-tmp.size();
					
					for (int z = 0; z < NumBun; z++) {
						tmp.add(0);
					}
				}
				
				int awdNum = tmp.get(tmpString[y].length()-1);
				awdNum++;
				
				//System.out.println("Word: " + tmpString[y] + " - " + tmpString[y].length() + " - ");
				
				tmp.set(tmpString[y].length()-1, awdNum);
			}
		}
		
		
		for (int x = 0; x < tmp.size(); x++) {
			int eae = x+1;
			System.out.println(eae + " character words - " + tmp.get(x));
		}
	}

	private static void prog_myIdeas(ArrayList<String> data) {
		ArrayList<String> tmp = new ArrayList<>();
		
		for (int x = 0; x < data.size(); x++) {
			String[] tmpString = data.get(x).split(" ");
			
			for (int y = 0; y < tmpString.length; y++) {
				int r = tmp.indexOf(tmpString[y]);
				
				if (r == -1) {
					tmp.add(tmpString[y]);
				}
			}
		}
		
		int ae = 0;
		for (int x = 0; x < tmp.size(); x++) {
			//System.out.println(tmp.get(x));
			ae++;
		}
		
		System.out.println("Number of unique words: " + ae);
	}

	private static void prog_wordle(ArrayList<String> data) {
		int MoneyMoneyBunny = 0;
		
		for (int x = 0; x < data.size(); x++) {
			String[] tmp = data.get(x).split(" ");
			MoneyMoneyBunny += tmp.length;
		}
		
		System.out.println("Number of words: " + MoneyMoneyBunny + "\n");
	}

	private static void prog_sanitize(ArrayList<String> data) {
		for (int x = 0; x < data.size(); x++) {
			String tmp = data.get(x);
			String tmpClean = tmp.replaceAll("[^a-zA-Z\\ ]", "").toLowerCase();
			clean.add(tmpClean);
			
			//System.out.println(x + "\t" + "'" + tmp + "'\n\t\t" + "'" + tmpClean + "'");
		}
	}

	private static void prog_loader() {
		openFile();
		readRecords(original);
		
	}

	public static void placeholder() {}
	
	public static void menu() {
		System.out.println("\nChoose one:");
		System.out.println("   1. read in the file (user picks location)");
		System.out.println("   2. remove the punctuation and make it case insensitive");
		System.out.println("   3. Determine the number of words in the book. Print the number.");
		System.out.println("   4. Determine the number of unique words in the book. Print the number.");
		System.out.println("   5. Print out a table of the number of words (total words – not unique ones) of each length (one\n"
				+ "\tcharacter, two characters, etc). Sorted from shortest to longest.");
		System.out.println("   6. Search for a given word. Determine how many times it appears in the text.");
		System.out.println("   7. Find the top ten words used by Tolstoy.");
		System.out.println("   8. Apply a cipher to the original file and store the encrypted output to a text file. Print out the\n"
				+ "\tfirst 30 lines to the console. See below for more details. User picks the location.");
		System.out.println("   9. Read the encoded file back into memory. Decode the message (ask the user for the file\n"
				+ "\tcontaining the decryption scheme). Print out the first thirty lines of the returned file. It\n"
				+ "\tshould be identical to the first thirty lines of the original file.");
		System.out.println("  10. Exit");
	}

}
