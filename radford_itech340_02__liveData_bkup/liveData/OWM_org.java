package liveData;

import java.io.*;
import java.net.*;
import java.text.SimpleDateFormat;
import java.util.*;
import org.json.*;



public class OWM_org {
	static Scanner scan = new Scanner(System.in);

	static boolean bill = false;
	
	public static String parseTime(String tmpTime) {
		
		
		//long unixSeconds = 1372339860;
		long unixSeconds = Long.parseLong(tmpTime);
		Date date = new java.util.Date(unixSeconds*1000L);
		SimpleDateFormat sdf = new java.text.SimpleDateFormat("HH:mm:ss z");
		//sdf.setTimeZone(java.util.TimeZone.getTimeZone("GMT-4"));
		sdf.setTimeZone(java.util.TimeZone.getTimeZone("GMT-4")); 
		
		String formattedDate = sdf.format(date);
		//System.out.println(formattedDate);
		
		return formattedDate;
	}
	
    public static String parseParms(Map<String, String> params) throws UnsupportedEncodingException {
    	StringBuilder result = new StringBuilder();
    	
    	for (Map.Entry<String, String> entry : params.entrySet()) {
    		result.append(URLEncoder.encode(entry.getKey(), "UTF-8"));
    		result.append("=");
    		result.append(URLEncoder.encode(entry.getValue(), "UTF-8"));
    		result.append("&");
    	}

    	String resultString = result.toString();
    	
    	return resultString.length() > 0 ? resultString.substring(0, resultString.length() - 1) : resultString;
    }
	
	public static StringBuffer getUrl(String zeURL) {

		URL url;
		HttpURLConnection con;
		StringBuffer content = new StringBuffer();
		
		int statusCode = 999;
		
		try {
			url = new URL(zeURL);
			con = (HttpURLConnection) url.openConnection();
			con.setRequestMethod("GET");
			
			statusCode = con.getResponseCode();
			
			Reader streamReader = null;
			
			if (statusCode > 299) {
				streamReader = new InputStreamReader(con.getErrorStream());
			} else {
				streamReader = new InputStreamReader(con.getInputStream());
			}
			
			BufferedReader in = new BufferedReader(streamReader);

			String inputLine;

			while ((inputLine = in.readLine()) != null) {
				content.append(inputLine);
			}

			in.close();
			con.disconnect();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		if (statusCode > 299) {
			try {
				JSONObject jsonObject = parseJson(content);
				if (jsonObject.get("cod").toString().equalsIgnoreCase("404")) {
					System.out.println("Error! That city could not be found");
				}
			} catch (JSONException e) {
				e.printStackTrace();
				return null;
			}
			bill = false;
		} else {
			bill = true;
		}
		
		return content;
		
	}
	
	public static JSONObject parseJson(StringBuffer rawData) {
		
		JSONObject jsonObject;
		try {
			// test data
			//jsonObject = new JSONObject("{\"one\":\"two\"}");
			//System.out.println(jsonObject.getString("one") + " ");
			
			jsonObject = new JSONObject(rawData.toString());
			//System.out.println(jsonObject.getJSONObject("coord").get("lon"));
			
			return jsonObject;
		} catch (JSONException e) {
			e.printStackTrace();
			return null;
		}
		
	}
	
	public static void parseWeatherCurrent(StringBuffer rawData) {
		
		JSONObject jsonObject = parseJson(rawData);
		
		
		if (jsonObject == null) {
			System.out.println("ERROR PARSING DATA!");
		}
		
		
		String cleanDataString = "";
		try {
			//cleanDataString += "\n\t";
			//cleanDataString += "\n\t: " + jsonObject.getJSONObject("").get("");
			
			cleanDataString += "Parsed Data:";
			cleanDataString += "\n\tLocation: " + jsonObject.get("name")+", "+jsonObject.getJSONObject("sys").get("country");
			cleanDataString += "\n\tSummary: \t" + jsonObject.getJSONArray("weather").getJSONObject(0).get("main") + " - " + jsonObject.getJSONArray("weather").getJSONObject(0).get("description");
			cleanDataString += "\n\tWindspeed: \t" + jsonObject.getJSONObject("wind").get("speed");
			cleanDataString += "\n\tTemperature: \t" + jsonObject.getJSONObject("main").get("temp") + "\t( Min: "+jsonObject.getJSONObject("main").get("temp_min") + " / Max: " + jsonObject.getJSONObject("main").get("temp_max");
			cleanDataString += "\n\tHumidity: \t" + jsonObject.getJSONObject("main").get("humidity");
			cleanDataString += "\n\tSunrise: \t"+parseTime(jsonObject.getJSONObject("sys").get("sunrise").toString());
			cleanDataString += "\n\tSunset: \t"+parseTime(jsonObject.getJSONObject("sys").get("sunset").toString());
			
			//System.out.println(jsonObject.getJSONArray("weather").getJSONObject(0).get("main"));
			
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		System.out.println(cleanDataString);
		
	}
	
	public static void parseWeatherFuture(StringBuffer rawData) {
		JSONObject jsonObject = parseJson(rawData);
		
		
		if (jsonObject == null) {
			System.out.println("ERROR PARSING DATA!");
		}
		
		
		try {
			JSONArray jarray = jsonObject.getJSONArray("list");
			
			
			
			//System.out.println(jarray.length()+ "---"+);
			
			for (int x=0; x < jarray.length(); x++) {
				JSONObject tmpDat = (JSONObject) jarray.get(x);
//				System.out.println(
//						x+": \n\tX:"+tmpDat.get("dt")
//						+ "\n\tY:"+tmpDat.getJSONObject("main").get("temp")
//						+ "\n\tMinY: "+tmpDat.getJSONObject("main").get("temp_min")
//						+ "\n\tMaxY: "+tmpDat.getJSONObject("main").get("temp_max")
//					);
				
				System.out.println("series1.add("+
						tmpDat.get("dt")+","+tmpDat.getJSONObject("main").get("temp")+","+tmpDat.getJSONObject("main").get("temp_min")+","+tmpDat.getJSONObject("main").get("temp_max")
					+");");
				
			}
			
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
//		String cleanDataString = "";
//		try {
//			//cleanDataString += "\n\t";
//			//cleanDataString += "\n\t: " + jsonObject.getJSONObject("").get("");
//			
//			cleanDataString += "Parsed Data:";
//			cleanDataString += "\n\tLocation: " + jsonObject.get("name")+", "+jsonObject.getJSONObject("sys").get("country");
//			cleanDataString += "\n\tSummary: \t" + jsonObject.getJSONArray("weather").getJSONObject(0).get("main") + " - " + jsonObject.getJSONArray("weather").getJSONObject(0).get("description");
//			cleanDataString += "\n\tWindspeed: \t" + jsonObject.getJSONObject("wind").get("speed");
//			cleanDataString += "\n\tTemperature: \t" + jsonObject.getJSONObject("main").get("temp") + "\t( Min: "+jsonObject.getJSONObject("main").get("temp_min") + " / Max: " + jsonObject.getJSONObject("main").get("temp_max");
//			cleanDataString += "\n\tHumidity: \t" + jsonObject.getJSONObject("main").get("humidity");
//			cleanDataString += "\n\tSunrise: \t"+parseTime(jsonObject.getJSONObject("sys").get("sunrise").toString());
//			cleanDataString += "\n\tSunset: \t"+parseTime(jsonObject.getJSONObject("sys").get("sunset").toString());
//			
//			//System.out.println(jsonObject.getJSONArray("weather").getJSONObject(0).get("main"));
//			
//		} catch (JSONException e) {
//			e.printStackTrace();
//		}
//		System.out.println(cleanDataString);
		
		
		
	
	}
	
	public static String menu() {
		String which = null;
		System.out.println("\nWeather Grepper:");
		System.out.println("\t 1) Current Forecast (console)");
		System.out.println("\t 2) Future Forecast  (gui)");
		which = scan.nextLine();
		return which;
	}
	
	public static void cleanup() {
		scan.close();
		System.exit(0);
	}
	

	public static void main(String[] args) {
		
		// TEST DATA
		// String strURL = "https://api.openweathermap.org/data/2.5/weather?q=London,UK&units=imperial&appid=8b741bc1024029be46233ee92fff1c9f";
		// StringBuffer rawData = new StringBuffer();
		// rawData.append("{\"coord\":{\"lon\":-79.9414,\"lat\":37.271},\"weather\":[{\"id\":800,\"main\":\"Clear\",\"description\":\"clear sky\",\"icon\":\"01d\"}],\"base\":\"stations\",\"main\":{\"temp\":72.72,\"feels_like\":71.33,\"temp_min\":69.51,\"temp_max\":74.21,\"pressure\":1016,\"humidity\":35},\"visibility\":10000,\"wind\":{\"speed\":5.99,\"deg\":274,\"gust\":10},\"clouds\":{\"all\":0},\"dt\":1699212432,\"sys\":{\"type\":2,\"id\":2031082,\"country\":\"US\",\"sunrise\":1699184888,\"sunset\":1699222712},\"timezone\":-18000,\"id\":4782167,\"name\":\"Roanoke\",\"cod\":200}");
				
		
		
		
		boolean coco = true;
		while (coco) {
			String choice = menu();
			
			String[] cA = choice.split(" ");
			
			if (cA[0].equalsIgnoreCase("1")) {
				System.out.println("Enter location to get weather from (Based on ISO 3166 Country Codes)\n(I.e.: 'London,UK'): ");
				String ccc = scan.nextLine();
				
				ccc.replaceAll("\\s+","%20");

				
				String strURL = "https://api.openweathermap.org/data/2.5/weather?q="+ccc+"&units=imperial&appid=8b741bc1024029be46233ee92fff1c9f";
				
				StringBuffer rawData = getUrl(strURL);
				
				if (bill) { parseWeatherCurrent(rawData); } ;
				
				
			} else if (cA[0].equalsIgnoreCase("2")) {
				StringBuffer rawData = new StringBuffer();
				rawData.append("{\"cod\":\"200\",\"message\":0,\"cnt\":40,\"list\":[{\"dt\":1699239600,\"main\":{\"temp\":53.31,\"feels_like\":51.17,\"temp_min\":48.61,\"temp_max\":53.31,\"pressure\":1020,\"sea_level\":1020,\"grnd_level\":986,\"humidity\":60,\"temp_kf\":2.61},\"weather\":[{\"id\":800,\"main\":\"Clear\",\"description\":\"clear sky\",\"icon\":\"01n\"}],\"clouds\":{\"all\":0},\"wind\":{\"speed\":4.36,\"deg\":296,\"gust\":4.05},\"visibility\":10000,\"pop\":0,\"sys\":{\"pod\":\"n\"},\"dt_txt\":\"2023-11-06 03:00:00\"},{\"dt\":1699250400,\"main\":{\"temp\":48.83,\"feels_like\":48.83,\"temp_min\":45.41,\"temp_max\":48.83,\"pressure\":1021,\"sea_level\":1021,\"grnd_level\":986,\"humidity\":67,\"temp_kf\":1.9},\"weather\":[{\"id\":800,\"main\":\"Clear\",\"description\":\"clear sky\",\"icon\":\"01n\"}],\"clouds\":{\"all\":3},\"wind\":{\"speed\":2.98,\"deg\":296,\"gust\":2.82},\"visibility\":10000,\"pop\":0,\"sys\":{\"pod\":\"n\"},\"dt_txt\":\"2023-11-06 06:00:00\"},{\"dt\":1699261200,\"main\":{\"temp\":43.65,\"feels_like\":43.65,\"temp_min\":43.65,\"temp_max\":43.65,\"pressure\":1021,\"sea_level\":1021,\"grnd_level\":987,\"humidity\":74,\"temp_kf\":0},\"weather\":[{\"id\":800,\"main\":\"Clear\",\"description\":\"clear sky\",\"icon\":\"01n\"}],\"clouds\":{\"all\":9},\"wind\":{\"speed\":2.57,\"deg\":290,\"gust\":2.42},\"visibility\":10000,\"pop\":0,\"sys\":{\"pod\":\"n\"},\"dt_txt\":\"2023-11-06 09:00:00\"},{\"dt\":1699272000,\"main\":{\"temp\":42.85,\"feels_like\":42.85,\"temp_min\":42.85,\"temp_max\":42.85,\"pressure\":1022,\"sea_level\":1022,\"grnd_level\":987,\"humidity\":73,\"temp_kf\":0},\"weather\":[{\"id\":801,\"main\":\"Clouds\",\"description\":\"few clouds\",\"icon\":\"02d\"}],\"clouds\":{\"all\":20},\"wind\":{\"speed\":2.24,\"deg\":294,\"gust\":2.08},\"visibility\":10000,\"pop\":0,\"sys\":{\"pod\":\"d\"},\"dt_txt\":\"2023-11-06 12:00:00\"},{\"dt\":1699282800,\"main\":{\"temp\":58.35,\"feels_like\":55.99,\"temp_min\":58.35,\"temp_max\":58.35,\"pressure\":1021,\"sea_level\":1021,\"grnd_level\":987,\"humidity\":45,\"temp_kf\":0},\"weather\":[{\"id\":802,\"main\":\"Clouds\",\"description\":\"scattered clouds\",\"icon\":\"03d\"}],\"clouds\":{\"all\":26},\"wind\":{\"speed\":2.71,\"deg\":159,\"gust\":4.9},\"visibility\":10000,\"pop\":0,\"sys\":{\"pod\":\"d\"},\"dt_txt\":\"2023-11-06 15:00:00\"},{\"dt\":1699293600,\"main\":{\"temp\":66.78,\"feels_like\":64.94,\"temp_min\":66.78,\"temp_max\":66.78,\"pressure\":1017,\"sea_level\":1017,\"grnd_level\":984,\"humidity\":38,\"temp_kf\":0},\"weather\":[{\"id\":803,\"main\":\"Clouds\",\"description\":\"broken clouds\",\"icon\":\"04d\"}],\"clouds\":{\"all\":58},\"wind\":{\"speed\":5.61,\"deg\":175,\"gust\":12.15},\"visibility\":10000,\"pop\":0,\"sys\":{\"pod\":\"d\"},\"dt_txt\":\"2023-11-06 18:00:00\"},{\"dt\":1699304400,\"main\":{\"temp\":67.39,\"feels_like\":65.8,\"temp_min\":67.39,\"temp_max\":67.39,\"pressure\":1015,\"sea_level\":1015,\"grnd_level\":982,\"humidity\":42,\"temp_kf\":0},\"weather\":[{\"id\":803,\"main\":\"Clouds\",\"description\":\"broken clouds\",\"icon\":\"04d\"}],\"clouds\":{\"all\":69},\"wind\":{\"speed\":6.04,\"deg\":170,\"gust\":12.15},\"visibility\":10000,\"pop\":0,\"sys\":{\"pod\":\"d\"},\"dt_txt\":\"2023-11-06 21:00:00\"},{\"dt\":1699315200,\"main\":{\"temp\":54.86,\"feels_like\":53.06,\"temp_min\":54.86,\"temp_max\":54.86,\"pressure\":1017,\"sea_level\":1017,\"grnd_level\":983,\"humidity\":64,\"temp_kf\":0},\"weather\":[{\"id\":803,\"main\":\"Clouds\",\"description\":\"broken clouds\",\"icon\":\"04n\"}],\"clouds\":{\"all\":67},\"wind\":{\"speed\":3.89,\"deg\":228,\"gust\":4.23},\"visibility\":10000,\"pop\":0,\"sys\":{\"pod\":\"n\"},\"dt_txt\":\"2023-11-07 00:00:00\"},{\"dt\":1699326000,\"main\":{\"temp\":52.77,\"feels_like\":50.9,\"temp_min\":52.77,\"temp_max\":52.77,\"pressure\":1016,\"sea_level\":1016,\"grnd_level\":983,\"humidity\":67,\"temp_kf\":0},\"weather\":[{\"id\":803,\"main\":\"Clouds\",\"description\":\"broken clouds\",\"icon\":\"04n\"}],\"clouds\":{\"all\":58},\"wind\":{\"speed\":3.71,\"deg\":249,\"gust\":4.85},\"visibility\":10000,\"pop\":0,\"sys\":{\"pod\":\"n\"},\"dt_txt\":\"2023-11-07 03:00:00\"},{\"dt\":1699336800,\"main\":{\"temp\":53.31,\"feels_like\":51.4,\"temp_min\":53.31,\"temp_max\":53.31,\"pressure\":1016,\"sea_level\":1016,\"grnd_level\":982,\"humidity\":65,\"temp_kf\":0},\"weather\":[{\"id\":803,\"main\":\"Clouds\",\"description\":\"broken clouds\",\"icon\":\"04n\"}],\"clouds\":{\"all\":79},\"wind\":{\"speed\":3.94,\"deg\":259,\"gust\":5.95},\"visibility\":10000,\"pop\":0,\"sys\":{\"pod\":\"n\"},\"dt_txt\":\"2023-11-07 06:00:00\"},{\"dt\":1699347600,\"main\":{\"temp\":53.17,\"feels_like\":51.33,\"temp_min\":53.17,\"temp_max\":53.17,\"pressure\":1015,\"sea_level\":1015,\"grnd_level\":982,\"humidity\":67,\"temp_kf\":0},\"weather\":[{\"id\":804,\"main\":\"Clouds\",\"description\":\"overcast clouds\",\"icon\":\"04n\"}],\"clouds\":{\"all\":100},\"wind\":{\"speed\":4.23,\"deg\":259,\"gust\":5.93},\"visibility\":10000,\"pop\":0,\"sys\":{\"pod\":\"n\"},\"dt_txt\":\"2023-11-07 09:00:00\"},{\"dt\":1699358400,\"main\":{\"temp\":53.24,\"feels_like\":51.66,\"temp_min\":53.24,\"temp_max\":53.24,\"pressure\":1015,\"sea_level\":1015,\"grnd_level\":982,\"humidity\":72,\"temp_kf\":0},\"weather\":[{\"id\":804,\"main\":\"Clouds\",\"description\":\"overcast clouds\",\"icon\":\"04d\"}],\"clouds\":{\"all\":100},\"wind\":{\"speed\":4.12,\"deg\":253,\"gust\":5.88},\"visibility\":10000,\"pop\":0,\"sys\":{\"pod\":\"d\"},\"dt_txt\":\"2023-11-07 12:00:00\"},{\"dt\":1699369200,\"main\":{\"temp\":67.66,\"feels_like\":66.52,\"temp_min\":67.66,\"temp_max\":67.66,\"pressure\":1015,\"sea_level\":1015,\"grnd_level\":982,\"humidity\":51,\"temp_kf\":0},\"weather\":[{\"id\":804,\"main\":\"Clouds\",\"description\":\"overcast clouds\",\"icon\":\"04d\"}],\"clouds\":{\"all\":99},\"wind\":{\"speed\":7.36,\"deg\":258,\"gust\":19.04},\"visibility\":10000,\"pop\":0,\"sys\":{\"pod\":\"d\"},\"dt_txt\":\"2023-11-07 15:00:00\"},{\"dt\":1699380000,\"main\":{\"temp\":76.19,\"feels_like\":75.49,\"temp_min\":76.19,\"temp_max\":76.19,\"pressure\":1013,\"sea_level\":1013,\"grnd_level\":981,\"humidity\":42,\"temp_kf\":0},\"weather\":[{\"id\":804,\"main\":\"Clouds\",\"description\":\"overcast clouds\",\"icon\":\"04d\"}],\"clouds\":{\"all\":97},\"wind\":{\"speed\":12.19,\"deg\":272,\"gust\":22.24},\"visibility\":10000,\"pop\":0,\"sys\":{\"pod\":\"d\"},\"dt_txt\":\"2023-11-07 18:00:00\"},{\"dt\":1699390800,\"main\":{\"temp\":74.32,\"feels_like\":73.65,\"temp_min\":74.32,\"temp_max\":74.32,\"pressure\":1013,\"sea_level\":1013,\"grnd_level\":980,\"humidity\":47,\"temp_kf\":0},\"weather\":[{\"id\":803,\"main\":\"Clouds\",\"description\":\"broken clouds\",\"icon\":\"04d\"}],\"clouds\":{\"all\":76},\"wind\":{\"speed\":7.87,\"deg\":277,\"gust\":20.22},\"visibility\":10000,\"pop\":0,\"sys\":{\"pod\":\"d\"},\"dt_txt\":\"2023-11-07 21:00:00\"},{\"dt\":1699401600,\"main\":{\"temp\":61.93,\"feels_like\":61.25,\"temp_min\":61.93,\"temp_max\":61.93,\"pressure\":1015,\"sea_level\":1015,\"grnd_level\":982,\"humidity\":73,\"temp_kf\":0},\"weather\":[{\"id\":803,\"main\":\"Clouds\",\"description\":\"broken clouds\",\"icon\":\"04n\"}],\"clouds\":{\"all\":51},\"wind\":{\"speed\":3.91,\"deg\":263,\"gust\":5.23},\"visibility\":10000,\"pop\":0,\"sys\":{\"pod\":\"n\"},\"dt_txt\":\"2023-11-08 00:00:00\"},{\"dt\":1699412400,\"main\":{\"temp\":60.53,\"feels_like\":59.86,\"temp_min\":60.53,\"temp_max\":60.53,\"pressure\":1016,\"sea_level\":1016,\"grnd_level\":982,\"humidity\":76,\"temp_kf\":0},\"weather\":[{\"id\":802,\"main\":\"Clouds\",\"description\":\"scattered clouds\",\"icon\":\"03n\"}],\"clouds\":{\"all\":44},\"wind\":{\"speed\":3.22,\"deg\":281,\"gust\":4.36},\"visibility\":10000,\"pop\":0,\"sys\":{\"pod\":\"n\"},\"dt_txt\":\"2023-11-08 03:00:00\"},{\"dt\":1699423200,\"main\":{\"temp\":57.06,\"feels_like\":56.46,\"temp_min\":57.06,\"temp_max\":57.06,\"pressure\":1016,\"sea_level\":1016,\"grnd_level\":983,\"humidity\":85,\"temp_kf\":0},\"weather\":[{\"id\":802,\"main\":\"Clouds\",\"description\":\"scattered clouds\",\"icon\":\"03n\"}],\"clouds\":{\"all\":37},\"wind\":{\"speed\":3.83,\"deg\":267,\"gust\":4.36},\"visibility\":10000,\"pop\":0,\"sys\":{\"pod\":\"n\"},\"dt_txt\":\"2023-11-08 06:00:00\"},{\"dt\":1699434000,\"main\":{\"temp\":55.15,\"feels_like\":54.55,\"temp_min\":55.15,\"temp_max\":55.15,\"pressure\":1017,\"sea_level\":1017,\"grnd_level\":983,\"humidity\":89,\"temp_kf\":0},\"weather\":[{\"id\":800,\"main\":\"Clear\",\"description\":\"clear sky\",\"icon\":\"01n\"}],\"clouds\":{\"all\":8},\"wind\":{\"speed\":3.69,\"deg\":277,\"gust\":3.98},\"visibility\":10000,\"pop\":0,\"sys\":{\"pod\":\"n\"},\"dt_txt\":\"2023-11-08 09:00:00\"},{\"dt\":1699444800,\"main\":{\"temp\":56.21,\"feels_like\":55.53,\"temp_min\":56.21,\"temp_max\":56.21,\"pressure\":1018,\"sea_level\":1018,\"grnd_level\":985,\"humidity\":85,\"temp_kf\":0},\"weather\":[{\"id\":802,\"main\":\"Clouds\",\"description\":\"scattered clouds\",\"icon\":\"03d\"}],\"clouds\":{\"all\":26},\"wind\":{\"speed\":2.35,\"deg\":271,\"gust\":2.3},\"visibility\":10000,\"pop\":0,\"sys\":{\"pod\":\"d\"},\"dt_txt\":\"2023-11-08 12:00:00\"},{\"dt\":1699455600,\"main\":{\"temp\":70.07,\"feels_like\":69.37,\"temp_min\":70.07,\"temp_max\":70.07,\"pressure\":1019,\"sea_level\":1019,\"grnd_level\":986,\"humidity\":55,\"temp_kf\":0},\"weather\":[{\"id\":801,\"main\":\"Clouds\",\"description\":\"few clouds\",\"icon\":\"02d\"}],\"clouds\":{\"all\":24},\"wind\":{\"speed\":1.14,\"deg\":182,\"gust\":2.91},\"visibility\":10000,\"pop\":0,\"sys\":{\"pod\":\"d\"},\"dt_txt\":\"2023-11-08 15:00:00\"},{\"dt\":1699466400,\"main\":{\"temp\":76.95,\"feels_like\":76.32,\"temp_min\":76.95,\"temp_max\":76.95,\"pressure\":1017,\"sea_level\":1017,\"grnd_level\":984,\"humidity\":42,\"temp_kf\":0},\"weather\":[{\"id\":801,\"main\":\"Clouds\",\"description\":\"few clouds\",\"icon\":\"02d\"}],\"clouds\":{\"all\":16},\"wind\":{\"speed\":4.16,\"deg\":142,\"gust\":8.41},\"visibility\":10000,\"pop\":0,\"sys\":{\"pod\":\"d\"},\"dt_txt\":\"2023-11-08 18:00:00\"},{\"dt\":1699477200,\"main\":{\"temp\":73.65,\"feels_like\":73.08,\"temp_min\":73.65,\"temp_max\":73.65,\"pressure\":1016,\"sea_level\":1016,\"grnd_level\":983,\"humidity\":50,\"temp_kf\":0},\"weather\":[{\"id\":803,\"main\":\"Clouds\",\"description\":\"broken clouds\",\"icon\":\"04d\"}],\"clouds\":{\"all\":65},\"wind\":{\"speed\":6.42,\"deg\":117,\"gust\":12.28},\"visibility\":10000,\"pop\":0,\"sys\":{\"pod\":\"d\"},\"dt_txt\":\"2023-11-08 21:00:00\"},{\"dt\":1699488000,\"main\":{\"temp\":62.19,\"feels_like\":61.68,\"temp_min\":62.19,\"temp_max\":62.19,\"pressure\":1018,\"sea_level\":1018,\"grnd_level\":984,\"humidity\":76,\"temp_kf\":0},\"weather\":[{\"id\":803,\"main\":\"Clouds\",\"description\":\"broken clouds\",\"icon\":\"04n\"}],\"clouds\":{\"all\":60},\"wind\":{\"speed\":3.09,\"deg\":103,\"gust\":4.03},\"visibility\":10000,\"pop\":0,\"sys\":{\"pod\":\"n\"},\"dt_txt\":\"2023-11-09 00:00:00\"},{\"dt\":1699498800,\"main\":{\"temp\":56.95,\"feels_like\":56.71,\"temp_min\":56.95,\"temp_max\":56.95,\"pressure\":1018,\"sea_level\":1018,\"grnd_level\":985,\"humidity\":93,\"temp_kf\":0},\"weather\":[{\"id\":800,\"main\":\"Clear\",\"description\":\"clear sky\",\"icon\":\"01n\"}],\"clouds\":{\"all\":1},\"wind\":{\"speed\":2.37,\"deg\":92,\"gust\":3.8},\"visibility\":10000,\"pop\":0,\"sys\":{\"pod\":\"n\"},\"dt_txt\":\"2023-11-09 03:00:00\"},{\"dt\":1699509600,\"main\":{\"temp\":53.76,\"feels_like\":53.4,\"temp_min\":53.76,\"temp_max\":53.76,\"pressure\":1018,\"sea_level\":1018,\"grnd_level\":984,\"humidity\":97,\"temp_kf\":0},\"weather\":[{\"id\":800,\"main\":\"Clear\",\"description\":\"clear sky\",\"icon\":\"01n\"}],\"clouds\":{\"all\":5},\"wind\":{\"speed\":2.1,\"deg\":97,\"gust\":2.8},\"visibility\":10000,\"pop\":0,\"sys\":{\"pod\":\"n\"},\"dt_txt\":\"2023-11-09 06:00:00\"},{\"dt\":1699520400,\"main\":{\"temp\":53.02,\"feels_like\":52.59,\"temp_min\":53.02,\"temp_max\":53.02,\"pressure\":1017,\"sea_level\":1017,\"grnd_level\":983,\"humidity\":97,\"temp_kf\":0},\"weather\":[{\"id\":802,\"main\":\"Clouds\",\"description\":\"scattered clouds\",\"icon\":\"03n\"}],\"clouds\":{\"all\":48},\"wind\":{\"speed\":1.66,\"deg\":260,\"gust\":2.26},\"visibility\":10000,\"pop\":0,\"sys\":{\"pod\":\"n\"},\"dt_txt\":\"2023-11-09 09:00:00\"},{\"dt\":1699531200,\"main\":{\"temp\":52.36,\"feels_like\":51.57,\"temp_min\":52.36,\"temp_max\":52.36,\"pressure\":1016,\"sea_level\":1016,\"grnd_level\":983,\"humidity\":91,\"temp_kf\":0},\"weather\":[{\"id\":802,\"main\":\"Clouds\",\"description\":\"scattered clouds\",\"icon\":\"03d\"}],\"clouds\":{\"all\":46},\"wind\":{\"speed\":2.84,\"deg\":237,\"gust\":4},\"visibility\":10000,\"pop\":0,\"sys\":{\"pod\":\"d\"},\"dt_txt\":\"2023-11-09 12:00:00\"},{\"dt\":1699542000,\"main\":{\"temp\":70.65,\"feels_like\":69.62,\"temp_min\":70.65,\"temp_max\":70.65,\"pressure\":1015,\"sea_level\":1015,\"grnd_level\":982,\"humidity\":47,\"temp_kf\":0},\"weather\":[{\"id\":803,\"main\":\"Clouds\",\"description\":\"broken clouds\",\"icon\":\"04d\"}],\"clouds\":{\"all\":82},\"wind\":{\"speed\":7.45,\"deg\":245,\"gust\":21.16},\"visibility\":10000,\"pop\":0,\"sys\":{\"pod\":\"d\"},\"dt_txt\":\"2023-11-09 15:00:00\"},{\"dt\":1699552800,\"main\":{\"temp\":78.98,\"feels_like\":78.98,\"temp_min\":78.98,\"temp_max\":78.98,\"pressure\":1012,\"sea_level\":1012,\"grnd_level\":980,\"humidity\":40,\"temp_kf\":0},\"weather\":[{\"id\":803,\"main\":\"Clouds\",\"description\":\"broken clouds\",\"icon\":\"04d\"}],\"clouds\":{\"all\":80},\"wind\":{\"speed\":10.78,\"deg\":256,\"gust\":18.39},\"visibility\":10000,\"pop\":0,\"sys\":{\"pod\":\"d\"},\"dt_txt\":\"2023-11-09 18:00:00\"},{\"dt\":1699563600,\"main\":{\"temp\":75.31,\"feels_like\":74.75,\"temp_min\":75.31,\"temp_max\":75.31,\"pressure\":1012,\"sea_level\":1012,\"grnd_level\":980,\"humidity\":47,\"temp_kf\":0},\"weather\":[{\"id\":804,\"main\":\"Clouds\",\"description\":\"overcast clouds\",\"icon\":\"04d\"}],\"clouds\":{\"all\":97},\"wind\":{\"speed\":8.19,\"deg\":259,\"gust\":16.15},\"visibility\":10000,\"pop\":0,\"sys\":{\"pod\":\"d\"},\"dt_txt\":\"2023-11-09 21:00:00\"},{\"dt\":1699574400,\"main\":{\"temp\":65.12,\"feels_like\":64.49,\"temp_min\":65.12,\"temp_max\":65.12,\"pressure\":1014,\"sea_level\":1014,\"grnd_level\":981,\"humidity\":67,\"temp_kf\":0},\"weather\":[{\"id\":804,\"main\":\"Clouds\",\"description\":\"overcast clouds\",\"icon\":\"04n\"}],\"clouds\":{\"all\":99},\"wind\":{\"speed\":4.21,\"deg\":272,\"gust\":5.88},\"visibility\":10000,\"pop\":0,\"sys\":{\"pod\":\"n\"},\"dt_txt\":\"2023-11-10 00:00:00\"},{\"dt\":1699585200,\"main\":{\"temp\":63.73,\"feels_like\":63.23,\"temp_min\":63.73,\"temp_max\":63.73,\"pressure\":1015,\"sea_level\":1015,\"grnd_level\":982,\"humidity\":73,\"temp_kf\":0},\"weather\":[{\"id\":804,\"main\":\"Clouds\",\"description\":\"overcast clouds\",\"icon\":\"04n\"}],\"clouds\":{\"all\":97},\"wind\":{\"speed\":4.74,\"deg\":295,\"gust\":11.27},\"visibility\":10000,\"pop\":0.08,\"sys\":{\"pod\":\"n\"},\"dt_txt\":\"2023-11-10 03:00:00\"},{\"dt\":1699596000,\"main\":{\"temp\":59.36,\"feels_like\":59,\"temp_min\":59.36,\"temp_max\":59.36,\"pressure\":1014,\"sea_level\":1014,\"grnd_level\":981,\"humidity\":85,\"temp_kf\":0},\"weather\":[{\"id\":804,\"main\":\"Clouds\",\"description\":\"overcast clouds\",\"icon\":\"04n\"}],\"clouds\":{\"all\":96},\"wind\":{\"speed\":4.03,\"deg\":294,\"gust\":5.14},\"visibility\":10000,\"pop\":0.15,\"sys\":{\"pod\":\"n\"},\"dt_txt\":\"2023-11-10 06:00:00\"},{\"dt\":1699606800,\"main\":{\"temp\":59.22,\"feels_like\":58.84,\"temp_min\":59.22,\"temp_max\":59.22,\"pressure\":1013,\"sea_level\":1013,\"grnd_level\":980,\"humidity\":85,\"temp_kf\":0},\"weather\":[{\"id\":804,\"main\":\"Clouds\",\"description\":\"overcast clouds\",\"icon\":\"04n\"}],\"clouds\":{\"all\":99},\"wind\":{\"speed\":2.77,\"deg\":313,\"gust\":3.15},\"visibility\":10000,\"pop\":0,\"sys\":{\"pod\":\"n\"},\"dt_txt\":\"2023-11-10 09:00:00\"},{\"dt\":1699617600,\"main\":{\"temp\":59.76,\"feels_like\":59.43,\"temp_min\":59.76,\"temp_max\":59.76,\"pressure\":1015,\"sea_level\":1015,\"grnd_level\":982,\"humidity\":85,\"temp_kf\":0},\"weather\":[{\"id\":804,\"main\":\"Clouds\",\"description\":\"overcast clouds\",\"icon\":\"04d\"}],\"clouds\":{\"all\":100},\"wind\":{\"speed\":2.24,\"deg\":303,\"gust\":2.26},\"visibility\":10000,\"pop\":0.25,\"sys\":{\"pod\":\"d\"},\"dt_txt\":\"2023-11-10 12:00:00\"},{\"dt\":1699628400,\"main\":{\"temp\":58.57,\"feels_like\":58.64,\"temp_min\":58.57,\"temp_max\":58.57,\"pressure\":1017,\"sea_level\":1017,\"grnd_level\":983,\"humidity\":96,\"temp_kf\":0},\"weather\":[{\"id\":500,\"main\":\"Rain\",\"description\":\"light rain\",\"icon\":\"10d\"}],\"clouds\":{\"all\":100},\"wind\":{\"speed\":0.74,\"deg\":104,\"gust\":1.48},\"visibility\":10000,\"pop\":0.99,\"rain\":{\"3h\":2.25},\"sys\":{\"pod\":\"d\"},\"dt_txt\":\"2023-11-10 15:00:00\"},{\"dt\":1699639200,\"main\":{\"temp\":58.57,\"feels_like\":58.64,\"temp_min\":58.57,\"temp_max\":58.57,\"pressure\":1016,\"sea_level\":1016,\"grnd_level\":982,\"humidity\":96,\"temp_kf\":0},\"weather\":[{\"id\":500,\"main\":\"Rain\",\"description\":\"light rain\",\"icon\":\"10d\"}],\"clouds\":{\"all\":100},\"wind\":{\"speed\":1.14,\"deg\":52,\"gust\":2.66},\"visibility\":6946,\"pop\":0.99,\"rain\":{\"3h\":2.99},\"sys\":{\"pod\":\"d\"},\"dt_txt\":\"2023-11-10 18:00:00\"},{\"dt\":1699650000,\"main\":{\"temp\":55.27,\"feels_like\":55.06,\"temp_min\":55.27,\"temp_max\":55.27,\"pressure\":1017,\"sea_level\":1017,\"grnd_level\":983,\"humidity\":97,\"temp_kf\":0},\"weather\":[{\"id\":501,\"main\":\"Rain\",\"description\":\"moderate rain\",\"icon\":\"10d\"}],\"clouds\":{\"all\":100},\"wind\":{\"speed\":4.21,\"deg\":59,\"gust\":7.14},\"visibility\":3933,\"pop\":1,\"rain\":{\"3h\":6.81},\"sys\":{\"pod\":\"d\"},\"dt_txt\":\"2023-11-10 21:00:00\"},{\"dt\":1699660800,\"main\":{\"temp\":52.72,\"feels_like\":52.25,\"temp_min\":52.72,\"temp_max\":52.72,\"pressure\":1019,\"sea_level\":1019,\"grnd_level\":985,\"humidity\":97,\"temp_kf\":0},\"weather\":[{\"id\":501,\"main\":\"Rain\",\"description\":\"moderate rain\",\"icon\":\"10n\"}],\"clouds\":{\"all\":100},\"wind\":{\"speed\":3.02,\"deg\":113,\"gust\":4.09},\"visibility\":3937,\"pop\":1,\"rain\":{\"3h\":3.93},\"sys\":{\"pod\":\"n\"},\"dt_txt\":\"2023-11-11 00:00:00\"}],\"city\":{\"id\":4782167,\"name\":\"Roanoke\",\"coord\":{\"lat\":37.271,\"lon\":-79.9414},\"country\":\"US\",\"population\":97032,\"timezone\":-18000,\"sunrise\":1699184888,\"sunset\":1699222712}}");
				
				parseWeatherFuture(rawData);
				
			} else {
				coco = false;
				cleanup();
			}
		}

		
		
		
		
		
		

	}

}
