package liveData;


import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import javax.swing.JPanel;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.DeviationRenderer;
import org.jfree.chart.ui.ApplicationFrame;
import org.jfree.chart.ui.RectangleInsets;
import org.jfree.chart.ui.UIUtils;
import org.jfree.data.time.RegularTimePeriod;
import org.jfree.data.time.Week;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.YIntervalSeries;
import org.jfree.data.xy.YIntervalSeriesCollection;

public class slim extends ApplicationFrame {
    public slim(String title) {
        super(title);
        JPanel chartPanel = slim.createDemoPanel();
        chartPanel.setPreferredSize(new Dimension(500, 270));
        this.setContentPane(chartPanel);
    }

    private static XYDataset createDataset() {
        YIntervalSeries series1 = new YIntervalSeries((Comparable)((Object)"Series 1"));
        YIntervalSeries series2 = new YIntervalSeries((Comparable)((Object)"Series 2"));
        RegularTimePeriod t = new Week();
        
        double y1 = 10.0;
        double y2 = 10.0;
//        for (int i = 0; i <= 52; ++i) {
//            double dev1 = 0.05 * (double)i;
//            series1.add(((RegularTimePeriod)t).getFirstMillisecond(), y1, y1 - dev1, y1 + dev1);
//            y1 = y1 + Math.random() - 0.45;
//            double dev2 = 0.07 * (double)i;
//            series2.add(((RegularTimePeriod)t).getFirstMillisecond(), y2, y2 - dev2, y2 + dev2);
//            y2 = y2 + Math.random() - 0.55;
//            t = ((RegularTimePeriod)t).next();
//        }
        
        // x, y, Ylow, Yhigh
        
        
        
//        series1.add(1699239600,53.31,48.61,53.31);
//        series1.add(1699250400,48.83,45.41,48.83);
//        series1.add(1699261200,43.65,43.65,43.65);
//        series1.add(1699272000,42.85,42.85,42.85);
//        series1.add(1699282800,58.35,58.35,58.35);
//        series1.add(1699293600,66.78,66.78,66.78);
//        series1.add(1699304400,67.39,67.39,67.39);
//        series1.add(1699315200,54.86,54.86,54.86);
//        series1.add(1699326000,52.77,52.77,52.77);
//        series1.add(1699336800,53.31,53.31,53.31);
//        series1.add(1699347600,53.17,53.17,53.17);
//        series1.add(1699358400,53.24,53.24,53.24);
//        series1.add(1699369200,67.66,67.66,67.66);

        
        series1.add(169923960, 2, 3, 4);
        series2.add(2, 2, 3, 4);
        series1.add(169925040, 3, 3, 4);
        series2.add(3, 3, 3, 4);
        
        
        System.out.println(((RegularTimePeriod)t).getFirstMillisecond());
        
        //series1.add(1, 2, 3, 4);
        //series2.add(5, 6, 7, 8);
        //series1.add(9, 10, 11, 12);
        //series2.add(13, 14, 15, 16);
        
        
        YIntervalSeriesCollection dataset = new YIntervalSeriesCollection();
        dataset.addSeries(series1);
        dataset.addSeries(series2);
        return dataset;
    }

    private static JFreeChart createChart(XYDataset dataset) {
        JFreeChart chart = ChartFactory.createTimeSeriesChart("Projected Values - Test", "Date", "Index Projection", dataset, true, true, false);
        XYPlot plot = (XYPlot)chart.getPlot();
        plot.setDomainPannable(true);
        plot.setRangePannable(false);
        plot.setInsets(new RectangleInsets(5.0, 5.0, 5.0, 20.0));
        DeviationRenderer renderer = new DeviationRenderer(true, false);
        renderer.setSeriesStroke(0, new BasicStroke(3.0f, 1, 1));
        renderer.setSeriesStroke(0, new BasicStroke(3.0f, 1, 1));
        renderer.setSeriesStroke(1, new BasicStroke(3.0f, 1, 1));
        renderer.setSeriesFillPaint(0, new Color(255, 200, 200));
        renderer.setSeriesFillPaint(1, new Color(200, 200, 255));
        plot.setRenderer(renderer);
        NumberAxis yAxis = (NumberAxis)plot.getRangeAxis();
        yAxis.setAutoRangeIncludesZero(false);
        yAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());
        return chart;
    }

    public static JPanel createDemoPanel() {
        JFreeChart chart = slim.createChart(slim.createDataset());
        return new ChartPanel(chart);
    }

    public static void main(String[] args) {
        slim demo = new slim("JFreeChart: DeviationRendererDemo2.java");
        demo.pack();
        UIUtils.centerFrameOnScreen(demo);
        demo.setVisible(true);
    }
}