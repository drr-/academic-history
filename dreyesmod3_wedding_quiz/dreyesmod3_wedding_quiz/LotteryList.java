package dreyesmod3_wedding_quiz;

import java.util.LinkedList;
import java.util.List;

public class LotteryList {
	private List<String> hopefulss = new LinkedList<>();
	private List<String> eliminated = new LinkedList<>();
	
	int[] steps;
	
	public LotteryList(List<String> suitors, int[] steps2) {
		hopefulss = suitors;
		steps = steps2;
	}

	public void rotateThroughAllSteps() {
		for (int i = steps.length-1; i >= 0; i--) {
			System.out.println(i+2 + "\t- " + hopefulss.toString().replaceAll("[\\[\\]]",""));
			
			if (steps[i] >= hopefulss.size()) {
				int TheStep = (steps[i] % hopefulss.size());
				eliminated.add(hopefulss.get(TheStep));
				hopefulss.remove(TheStep);
			} else {
				eliminated.add(hopefulss.get(steps[i]));
				hopefulss.remove(steps[i]);
			}
		}
	}

	public List<String> getEliminated() {
		return eliminated;
	}

	public List<String> getHopefuls() {
		return hopefulss;
	}
	
	
	
}
