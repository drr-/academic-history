package checkpointTwo;

public class Customer extends Person {

	
	private int custId;
	
	public static int num = 1;
	
	//Empty constructor. Still makes a new custId.
	public Customer() {
		custId = num;
		num++;
	}
	
	// Constructor
	public Customer (String n, String a, String p) {
		super(n, a, p);
		custId = num;
		num++;
	}
	
	// When read from a file
	public Customer (String n, String a, String p, int id) {
		super(n, a, p);
		custId = id;
	}
	
	// Uses Person.toString and adds on the customer id.
	public String toString() {
		return  super.toString() + " and their customer id is: " + custId;
	}
	
	public String toStringF() {
		return super.toStringF() + "|" + custId;
	}

	public int getCustId() {
		return custId;
	}

	public void setCustId(int custId) {
		this.custId = custId;
	}
}
