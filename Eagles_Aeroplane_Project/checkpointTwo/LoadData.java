package checkpointTwo;

import java.text.NumberFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class LoadData {

	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);
		ArrayList<Customer> c = new ArrayList<Customer>();
		ArrayList<Pilot> p = new ArrayList<Pilot>();
		ArrayList<Flight> f = new ArrayList<Flight>();
		ArrayList<Seat> s = new ArrayList<Seat>();
		ArrayList<Reservation> r = new ArrayList<Reservation>();
		ArrayList<Reservation> rd = new ArrayList<Reservation>();

		loadCustomers(c);
		loadPilots(p);
		loadFlights(f, p);

		System.out.println("          Welcome to Eagle Airways\n_____________________________________________");

		int choice = 0;
		Integer custId = null;
		boolean more = true;
		while (more) {
			
			choice = menu();

			if (choice == 1) {
				custId = null;
				do {
					try {
						System.out.println("What is the customer's ID number: ");
						//custId = scan.nextInt();
						String tempId = scan.nextLine();
						
						custId = Integer.parseInt(tempId);
				
						findCustInfo(c, custId);
					} catch (NumberFormatException e) {
						System.out.println("Wrong input try again.");
					}
				} while (custId == null);
			} else if (choice == 2) {
				addNewCust(c);
			} else if (choice == 3) {
				Integer pilotId = null;
				do {
					try {
						System.out.println("Please provide your pilot ID: ");
						String tempPilot = scan.nextLine();
						pilotId = Integer.parseInt(tempPilot);
						
						//int pilotId = scan.nextInt();
						printPilotSchedule(f, pilotId);
					} catch (NumberFormatException e) {
						System.out.println("Wrong input try again.");
					}
				} while (pilotId == null);
			} else if (choice == 4) {
				createReservation(c, f, r);
			} else if (choice == 5) {
				deleteReservation(r, f, rd);

			} else if (choice == 6) {
				findReservationNumber(r);
			} else if (choice == 7) {
				findReservationCust(r, c);
			} else if (choice == 8) {
				printIncomeOneFlight(r, f);
			} else if (choice == 9) {
				printIncomeAllFlights(r);
			} else if (choice == 10) {
				findDeletedReservationNumber(rd);
			} else if (choice == 11) {
				findDeletedReservationCust(c, rd);
			} else if (choice == 12) {
				displaySeatMapForFlight(f);
			}

			else if (choice == 13) {
				printCustomers(c);
			} else if (choice == 14) {
				printPilots(p);
			} else if (choice == 15) {
				printFlights(f, p);
			}

			else {
				System.out.println("Thank You For Choosing Eagle Airways, Have A Nice Day!");
				System.exit(0);
			}
		}

	}

	public static int menu() {
		Scanner scan = new Scanner(System.in);
		System.out.println("\nPlease make a selection from the list below:");
		System.out.println("1. Find Customer Information ");
		System.out.println("2. Add New Customer ");
		System.out.println("3. Print Pilot Schedule ");
		System.out.println("4. Create New Reservation ");
		System.out.println("5. Delete Existing Reservation ");
		System.out.println("6. Find Existing Reservation By Reservation Number");
		System.out.println("7. Find Existing Reservation(s) By Customer Number");
		System.out.println("8. Print Gross Income For A Specific Flight ");
		System.out.println("9. Print Gross Income For All Scheduled Flights ");
		System.out.println("10. Find Deleted Reservation By Reservation Number");
		System.out.println("11. Find Deleted Reservation(s) By Customer Number");
		System.out.println("12. Show Seat Arrangement For A Flight");

		System.out.println("13. Print Current Customers");
		System.out.println("14. Print Current Pilots");
		System.out.println("15. Print Current Flights");
		System.out.println("999. Exit Program ");

		System.out.println("Your choice: ");
		int value = scan.nextInt();
		return value;
	}

	public static void findCustInfo(ArrayList<Customer> c, int custNum) {
		boolean foundCust = false;
		for (int i = 0; i < c.size(); i++) {
			if (c.get(i).getCustId() == custNum) {
				System.out.println(c.get(i).toString());
				foundCust = true;
			}
		}
		if (!foundCust) {
			System.out.println("Didn't find a customer with that ID.");
		}
	}

	public static void addNewCust(ArrayList<Customer> c) {
		Scanner scan = new Scanner(System.in);
		System.out.println("What is this customer's full name (first and last): ");
		String custName = scan.nextLine();

		boolean custFound = false;
		Customer ourCust = new Customer();
		for (Customer cust : c) {
			if (cust.getName().equalsIgnoreCase(custName)) {
				ourCust = cust;
				custFound = true;
				System.out.println("This customer already exists!");
				System.out.println("Customer ID: " + cust.getCustId() + " | " + cust.getName() + " | " + cust.getPhone() + " | " +
				cust.getAddress());
			}
			
		}
		if (!custFound) {
			String phone = null;
			
			System.out.println("What is this customer's address: ");
			String address = scan.nextLine();
			
			boolean pMatch = false;
			while (!pMatch) {
				System.out.println("What is this customer's phone number? ");
				System.out.println("Example '(000)-123-4567': ");
				phone = scan.nextLine();
				Pattern pattern = Pattern.compile("\\([0-9]{3}\\)-[0-9]{3}-[0-9]{4}");
				Matcher match = pattern.matcher(phone);
				if (match.matches()) {
					pMatch = true;
				} else {
					System.out.println("Phone number does not match format");
				}
			}
			
			ourCust.setName(custName);
			ourCust.setAddress(address);
			ourCust.setPhone(phone);
			c.add(ourCust);
			System.out.println("Added " + custName + "!");
		}
	}

	public static void printPilotSchedule(ArrayList<Flight> f, int pilotId) {
		boolean foundPilot = false;
		for (int i = 0; i < f.size(); i++) {
			if (f.get(i).getPilot().getPilotId() == (pilotId)) {
				foundPilot = true;
				System.out.println(f.get(i).toString());
			}
		}
		if (!foundPilot) {
			System.out.println("Didn't find a pilot with that ID.");
		}
	}

	public static void createReservation(ArrayList<Customer> c, ArrayList<Flight> f, ArrayList<Reservation> r) {
		Scanner scan = new Scanner(System.in);
		System.out.println("Please enter your first and last name:");
		String custName = scan.nextLine();

		boolean custFound = false;
		Customer ourCust = new Customer();
		for (Customer cust : c) {
			if (cust.getName().equalsIgnoreCase(custName)) {
				ourCust = cust;
				custFound = true;
			}
		}
		if (!custFound) {
			System.out.println("What is this customer's address: ");
			String address = scan.nextLine();
			System.out.println("What is this customer's phone number: ");
			String phone = scan.nextLine();
			ourCust.setName(custName);
			ourCust.setAddress(address);
			ourCust.setPhone(phone);
			c.add(ourCust);
		}
		boolean valid = false;
		System.out.println("What is your destination (R = Roanoke, P = Pheonix): ");
		String direction = "";
		while (!valid) {
			String destination = scan.nextLine();
			if (destination.equalsIgnoreCase("R")) {
				valid = true;
				direction = "PR";
			} else if (destination.equalsIgnoreCase("P")) {
				valid = true;
				direction = "RP";
			} else {
				System.out.println("Please enter a valid destination for your reservation");
			}
		}
		valid = false;
		int date = 0;
		System.out.println("Enter a number between 12 and 18 for your reservation date in November");
		while (!valid) {
			try {
				date = scan.nextInt();
				if (date >= 12 && date <= 18) {
					valid = true;
				} else {
					System.out.println("Please enter a valid date between 12 and 18");
				}
			} catch (InputMismatchException e) {
				System.out.println("Please enter numbers only.");
				scan.nextLine();
			}
		}
		scan.nextLine();
		String time = "";
		valid = false;
		System.out.println("Do you want the AM flight or the PM flight?");
		while (!valid) {
			time = scan.nextLine();
			if (time.equalsIgnoreCase("AM") || time.equalsIgnoreCase("PM")) {
				valid = true;
			} else {
				System.out.println("Please choose either AM or PM for your flight time");
			}
		}
		String flightCode = Integer.toString(date) + direction + time;
		flightCode = flightCode.toUpperCase();
		Flight selectedFlight = new Flight();

		ArrayList<Seat> availableEcon = new ArrayList<Seat>();
		ArrayList<Seat> availableFirstClass = new ArrayList<Seat>();
		for (Flight fl : f) {
			if (fl.getFlightCode().contains(flightCode)) {
				System.out.println(fl.seatsAvailable());
				availableEcon = fl.economySeatsAvailable();
				availableFirstClass = fl.firstClassSeatsAvailable();
				selectedFlight = fl;
			}
		}

		valid = false;
		String section = "";
		System.out.println("Which section for your seats? E for economy." + " F for FirstClass.");
		while (!valid) {
			section = scan.nextLine();
			if (section.equalsIgnoreCase("E") || section.equalsIgnoreCase("F")) {
				if (section.equalsIgnoreCase("E") && availableEcon.size() == 0) {
					System.out.println("There are no seats in economy.");
				} else if (section.equalsIgnoreCase("F") && availableFirstClass.size() == 0) {
					System.out.println("There are no seats in first class.");
				} else
					valid = true;
			} else {
				System.out.println("Please enter a valid section");
			}
		}

		valid = false;
		int numSeats = 0;
		System.out.println("How many seats would you like to reserve?");
		while (!valid) {
			try {
				numSeats = scan.nextInt();
				if (numSeats > 0) {
					if (numSeats <= availableFirstClass.size() && section.equalsIgnoreCase("F"))
						valid = true;
					else if (numSeats <= availableEcon.size() && section.equalsIgnoreCase("E")) {
						valid = true;
					} else {
						System.out.println("Please enter a valid number of seats");
					}
				} else {
					System.out.println("Please enter a valid number of seats");
				}
			} catch (InputMismatchException e) {
				System.out.println("Please enter numbers only.");
				scan.nextLine();
			}
		}
		scan.nextLine();
		ArrayList<Seat> seatsSelected = new ArrayList<Seat>();
		if (section.equalsIgnoreCase("E")) {
			for (int i = 0; i < numSeats; i++) {
				seatsSelected.add(availableEcon.get(i));
				// System.out.println(seatsSelected.toString());
			}
		} else {
			for (int i = 0; i < numSeats; i++) {
				seatsSelected.add(availableFirstClass.get(i));
				// System.out.println(seatsSelected.toString());
			}
		}
		addReservation(selectedFlight, ourCust, seatsSelected, r);
	}

	public static void addReservation(Flight f, Customer c, ArrayList<Seat> s, ArrayList<Reservation> r) {
		for (Seat st : s) {
			st.setStatus("R");
		}
		Reservation res = new Reservation(f, c, s);
		System.out.println(res.toString());
		r.add(res);
	}

	public static void deleteReservation(ArrayList<Reservation> r, ArrayList<Flight> f, ArrayList<Reservation> rd) {
		// System.out.println(r.toString());
		Scanner scan = new Scanner(System.in);
		Reservation rFound = new Reservation();
		ArrayList<Seat> seats = new ArrayList<>();
		int reservationID = 0;
		System.out.println("Please enter the reservation number:");
		try {
			reservationID = scan.nextInt();
		} catch (InputMismatchException e) {
			System.err.println("Please only enter numbers");
			scan.nextLine();
		}
		Boolean found = false;

		for (Reservation res : r) {
			if (res.getReservationID() == reservationID) {
				found = true;
				rFound = res;
				seats = res.getSeats();
				System.out.println("I have found that reservation.");
			}
		}
		if (found) {
			for (Flight flight : f) {
				if (flight.getFlightCode() == rFound.getFlight().getFlightCode()) {
					System.out.println(flight.seats());

					for (int row = 0; row < flight.seatMap.length; row++) {
						for (int col = 0; col < flight.seatMap[row].length; col++) {
							for (Seat st : seats) {
								Seat fs = flight.seatMap[row][col];
								if (fs != null && fs.getSeatNum() == st.getSeatNum()) {
									fs.setStatus("A");
								}
							}
						}
					}
					// System.out.println(flight.seats());
					System.out.println("The reservation has been deleted");
				}
			}
			rd.add(rFound);
			r.remove(rFound);
			System.out.println(rFound.deletedToString());

		} else {
			System.out.println("I could not find that reservation.");
		}
	}

	public static void findReservationNumber(ArrayList<Reservation> r) {
		// System.out.println(r.toString());
		Scanner scan = new Scanner(System.in);
		int reservationID = 0;
		System.out.println("Please enter the reservation number:");
		try {
			reservationID = scan.nextInt();
		} catch (InputMismatchException e) {
			System.err.println("Please only enter numbers");
			scan.nextLine();
		}
		Boolean found = false;

		for (Reservation res : r) {
			if (res.getReservationID() == reservationID) {
				found = true;
				System.out.println("I have found that reservation.");
				System.out.println(res.toString());
			}
		}
		if (!found) {
			System.out.println("I could not find that reservation number.");
		}
	}

	public static void findReservationCust(ArrayList<Reservation> r, ArrayList<Customer> c) {
		// for (int i = 0; i < c.size(); i++) {
		// System.out.println(c.get(i).toString());
		// }
		Scanner scan = new Scanner(System.in);
		int custID = 0;
		Customer customer;
		int numRes = 0;
		System.out.println("Please enter the customer's id number:");
		try {
			custID = scan.nextInt();
		} catch (InputMismatchException e) {
			System.err.println("Please only enter numbers");
			scan.nextLine();
		}
		Boolean found = false;
		for (Customer cust : c) {
			if (cust.getCustId() == custID) {
				found = true;
				System.out.println("I have found that customer.");
				customer = cust;
				for (int i = 0; i < r.size(); i++) {
					if (r.get(i).getCust() == customer) {
						numRes++;
						System.out.println(r.get(i).toString());
					}
				}
			}
		}
		if (!found) {
			System.out.println("I could not find that customer's number.");
		}
		if (numRes == 0) {
			System.out.println("I could not find any reservations for that customer.");
		}
	}

	public static void printIncomeOneFlight(ArrayList<Reservation> r, ArrayList<Flight> f) {
		NumberFormat nf = NumberFormat.getCurrencyInstance();
		Scanner scan = new Scanner(System.in);
		System.out.println("Please enter a valid flight code");
		String flightCode = scan.nextLine();
		flightCode.toUpperCase();
		double income = 0;
		boolean found = false;
		for (int j = 0; j < r.size(); j++) {
			if (r.get(j).getFlight().getFlightCode().contains(flightCode)) {
				found = true;
				Reservation res = r.get(j);
				for (int k = 0; k < res.getSeats().size(); k++) {
					if (res.getSeats().get(k).getStatus().equals("R")) {
						income = income + res.getSeats().get(k).getCost();
					}
				}
			}
		}

		if (!found) {
			System.out.println("I could not find a flight with that flight code");
		} else {
			System.out.println("The total income for this single flight is " + nf.format(income));
		}
	}

	public static void printIncomeAllFlights(ArrayList<Reservation> r) {
		NumberFormat nf = NumberFormat.getCurrencyInstance();
		double totalIncome = 0;

		for (int i = 0; i < r.size(); i++) {
			for (int j = 0; j < r.get(i).getSeats().size(); j++) {
				if (r.get(i).getSeats().get(j).getStatus().equals("R")) {
					totalIncome = totalIncome + r.get(i).getSeats().get(j).getCost();
				}
			}
		}
		System.out.println("The total income for all flights currently reserved is " + nf.format(totalIncome));
	}

	public static void findDeletedReservationNumber(ArrayList<Reservation> rd) {
		Scanner scan = new Scanner(System.in);
		System.out.println("Please enter the reservation id of the deleted reservation you wish to search for");
		int resId = 0;
		try {
			resId = scan.nextInt();
		} catch (InputMismatchException e) {
			System.err.println("Please only enter numbers");
			scan.nextLine();
		}

		boolean found = false;
		for (int i = 0; i < rd.size(); i++) {
			if (rd.get(i).getReservationID() == resId) {
				found = true;
				System.out.println(rd.get(i).toString());
			}
		}
		if (!found) {
			System.out.println("I could not find a deleted reservation with that reservation id");
		}
	}

	public static void findDeletedReservationCust(ArrayList<Customer> c, ArrayList<Reservation> rd) {
		Scanner scan = new Scanner(System.in);
		System.out.println("Please enter the customer's id number");
		int custID = 0;
		try {
			custID = scan.nextInt();
		} catch (InputMismatchException e) {
			System.err.println("Please only enter numbers");
			scan.nextLine();
		}
		boolean foundCust = false;
		boolean foundRes = false;
		for (int i = 0; i < c.size(); i++) {
			if (c.get(i).getCustId() == custID) {
				foundCust = true;
			}
		}
		for (int j = 0; j < rd.size(); j++) {
			if (rd.get(j).getCust().getCustId() == custID) {
				foundRes = true;
				System.out.println(rd.get(j).toString());
			}
		}
		if (!foundCust) {
			System.out.println("I could not find a customer with that ID");
		} else if (!foundRes) {
			System.out.println("I could not find a deleted reservation for that customer");
		}
	}

	public static void displaySeatMapForFlight(ArrayList<Flight> f) {
		Scanner scan = new Scanner(System.in);
		System.out.println("Please enter a valid flight code");
		String flightCode = scan.nextLine();
		flightCode.toUpperCase();

		boolean found = false;
		for (Flight flight : f) {
			if (flight.flightCode.contains(flightCode)) {
				found = true;
				System.out.println(flight.seats());
			}
		}
		if (!found) {
			System.out.println("I could not find a flight with that flight code");
		}
	}

	public static void printCustomers(ArrayList<Customer> c) {
		System.out.println("Current Customer List:");
		System.out.println();
		for (int i = 0; i < c.size(); i++)
			System.out.println("Customer ID: " + c.get(i).getCustId() + " | " + c.get(i).getName() + " | "
					+ c.get(i).getPhone() + " | " + c.get(i).getAddress());
	}

	public static void printPilots(ArrayList<Pilot> p) {
		System.out.println("Current Pilot List:");
		System.out.println();
		for (int i = 0; i < p.size(); i++)
			System.out.println("Pilot ID: " + p.get(i).getPilotId() + " | " + p.get(i).getName() + " | "
					+ p.get(i).getPhone() + " |  " + p.get(i).getAddress());
	}

	public static void printFlights(ArrayList<Flight> flights, ArrayList<Pilot> p) {
		System.out.println("Current Flight Schedule:");
		System.out.println();
		for (int i = 0; i < flights.size(); i++)
			for (int j = 0; i < p.size(); i++)
				System.out.println("Flight ID: " + flights.get(i).getFlightCode() + " | " + flights.get(i).getDate()
						+ " | " + flights.get(i).getRoute() + " | " + flights.get(i).getTime() + " | "
						+ flights.get(i).getPilot().getName());
	}

	public static void loadCustomers(ArrayList<Customer> c) {
		c.add(new Customer("Donald Duck", "1134 DisneyLand Road", "(540)-337-2281"));
		c.add(new Customer("Mickey Mouse", "741 Mouse Lane", "(791)-173-4877"));
		c.add(new Customer("Minnie Mouse", "742 Mouse Lane", "(791)-672-8990"));
		c.add(new Customer("Goofy", "532 Silly Circle", "(999)-999-0110"));
	}

	public static void loadPilots(ArrayList<Pilot> p) {
		p.add(new Pilot("Charlie Brown", "677 Silly Circle", "(999)-821-7432"));
		p.add(new Pilot("Linus", "9211 Blankey Ave", "(112)-335-7766"));
		p.add(new Pilot("Sally Brown", "677 Silly Circle", "(999)-553-9999"));
	}

	public static void loadFlights(ArrayList<Flight> flights, ArrayList<Pilot> p) {
		Seat[][] seatMap = { { null, null, null, new Seat(5), new Seat(9) },
				{ new Seat(1), new Seat(3), null, new Seat(6), new Seat(10) }, { null, null, null, null, null }, // sets
																													// the
																													// aisle
				{ new Seat(2), new Seat(4), null, new Seat(7), new Seat(11) },
				{ null, null, null, new Seat(8), new Seat(12) } };
		Seat[][] seatMap2 = { { null, null, null, new Seat(5), new Seat(9) },
				{ new Seat(1), new Seat(3), null, new Seat(6), new Seat(10) }, { null, null, null, null, null }, // sets
																													// the
																													// aisle
				{ new Seat(2), new Seat(4), null, new Seat(7), new Seat(11) },
				{ null, null, null, new Seat(8), new Seat(12) } };
		Seat[][] seatMap3 = { { null, null, null, new Seat(5), new Seat(9) },
				{ new Seat(1), new Seat(3), null, new Seat(6), new Seat(10) }, { null, null, null, null, null }, // sets
																													// the
																													// aisle
				{ new Seat(2), new Seat(4), null, new Seat(7), new Seat(11) },
				{ null, null, null, new Seat(8), new Seat(12) } };
		Seat[][] seatMap4 = { { null, null, null, new Seat(5), new Seat(9) },
				{ new Seat(1), new Seat(3), null, new Seat(6), new Seat(10) }, { null, null, null, null, null }, // sets
																													// the
																													// aisle
				{ new Seat(2), new Seat(4), null, new Seat(7), new Seat(11) },
				{ null, null, null, new Seat(8), new Seat(12) } };

		Seat[][] seatMap5 = { { null, null, null, new Seat(5), new Seat(9) },
				{ new Seat(1), new Seat(3), null, new Seat(6), new Seat(10) }, { null, null, null, null, null }, // sets
																													// the
																													// aisle
				{ new Seat(2), new Seat(4), null, new Seat(7), new Seat(11) },
				{ null, null, null, new Seat(8), new Seat(12) } };
		Seat[][] seatMap6 = { { null, null, null, new Seat(5), new Seat(9) },
				{ new Seat(1), new Seat(3), null, new Seat(6), new Seat(10) }, { null, null, null, null, null }, // sets
																													// the
																													// aisle
				{ new Seat(2), new Seat(4), null, new Seat(7), new Seat(11) },
				{ null, null, null, new Seat(8), new Seat(12) } };
		Seat[][] seatMap7 = { { null, null, null, new Seat(5), new Seat(9) },
				{ new Seat(1), new Seat(3), null, new Seat(6), new Seat(10) }, { null, null, null, null, null }, // sets
																													// the
																													// aisle
				{ new Seat(2), new Seat(4), null, new Seat(7), new Seat(11) },
				{ null, null, null, new Seat(8), new Seat(12) } };
		Seat[][] seatMap8 = { { null, null, null, new Seat(5), new Seat(9) },
				{ new Seat(1), new Seat(3), null, new Seat(6), new Seat(10) }, { null, null, null, null, null }, // sets
																													// the
																													// aisle
				{ new Seat(2), new Seat(4), null, new Seat(7), new Seat(11) },
				{ null, null, null, new Seat(8), new Seat(12) } };
		Seat[][] seatMap9 = { { null, null, null, new Seat(5), new Seat(9) },
				{ new Seat(1), new Seat(3), null, new Seat(6), new Seat(10) }, { null, null, null, null, null }, // sets
																													// the
																													// aisle
				{ new Seat(2), new Seat(4), null, new Seat(7), new Seat(11) },
				{ null, null, null, new Seat(8), new Seat(12) } };
		Seat[][] seatMap10 = { { null, null, null, new Seat(5), new Seat(9) },
				{ new Seat(1), new Seat(3), null, new Seat(6), new Seat(10) }, { null, null, null, null, null }, // sets
																													// the
																													// aisle
				{ new Seat(2), new Seat(4), null, new Seat(7), new Seat(11) },
				{ null, null, null, new Seat(8), new Seat(12) } };
		Seat[][] seatMap11 = { { null, null, null, new Seat(5), new Seat(9) },
				{ new Seat(1), new Seat(3), null, new Seat(6), new Seat(10) }, { null, null, null, null, null }, // sets
																													// the
																													// aisle
				{ new Seat(2), new Seat(4), null, new Seat(7), new Seat(11) },
				{ null, null, null, new Seat(8), new Seat(12) } };
		Seat[][] seatMap12 = { { null, null, null, new Seat(5), new Seat(9) },
				{ new Seat(1), new Seat(3), null, new Seat(6), new Seat(10) }, { null, null, null, null, null }, // sets
																													// the
																													// aisle
				{ new Seat(2), new Seat(4), null, new Seat(7), new Seat(11) },
				{ null, null, null, new Seat(8), new Seat(12) } };
		Seat[][] seatMap13 = { { null, null, null, new Seat(5), new Seat(9) },
				{ new Seat(1), new Seat(3), null, new Seat(6), new Seat(10) }, { null, null, null, null, null }, // sets
																													// the
																													// aisle
				{ new Seat(2), new Seat(4), null, new Seat(7), new Seat(11) },
				{ null, null, null, new Seat(8), new Seat(12) } };
		Seat[][] seatMap14 = { { null, null, null, new Seat(5), new Seat(9) },
				{ new Seat(1), new Seat(3), null, new Seat(6), new Seat(10) }, { null, null, null, null, null }, // sets
																													// the
																													// aisle
				{ new Seat(2), new Seat(4), null, new Seat(7), new Seat(11) },
				{ null, null, null, new Seat(8), new Seat(12) } };
		Seat[][] seatMap15 = { { null, null, null, new Seat(5), new Seat(9) },
				{ new Seat(1), new Seat(3), null, new Seat(6), new Seat(10) }, { null, null, null, null, null }, // sets
																													// the
																													// aisle
				{ new Seat(2), new Seat(4), null, new Seat(7), new Seat(11) },
				{ null, null, null, new Seat(8), new Seat(12) } };
		Seat[][] seatMap16 = { { null, null, null, new Seat(5), new Seat(9) },
				{ new Seat(1), new Seat(3), null, new Seat(6), new Seat(10) }, { null, null, null, null, null }, // sets
																													// the
																													// aisle
				{ new Seat(2), new Seat(4), null, new Seat(7), new Seat(11) },
				{ null, null, null, new Seat(8), new Seat(12) } };
		Seat[][] seatMap17 = { { null, null, null, new Seat(5), new Seat(9) },
				{ new Seat(1), new Seat(3), null, new Seat(6), new Seat(10) }, { null, null, null, null, null }, // sets
																													// the
																													// aisle
				{ new Seat(2), new Seat(4), null, new Seat(7), new Seat(11) },
				{ null, null, null, new Seat(8), new Seat(12) } };
		Seat[][] seatMap18 = { { null, null, null, new Seat(5), new Seat(9) },
				{ new Seat(1), new Seat(3), null, new Seat(6), new Seat(10) }, { null, null, null, null, null }, // sets
																													// the
																													// aisle
				{ new Seat(2), new Seat(4), null, new Seat(7), new Seat(11) },
				{ null, null, null, new Seat(8), new Seat(12) } };
		Seat[][] seatMap19 = { { null, null, null, new Seat(5), new Seat(9) },
				{ new Seat(1), new Seat(3), null, new Seat(6), new Seat(10) }, { null, null, null, null, null }, // sets
																													// the
																													// aisle
				{ new Seat(2), new Seat(4), null, new Seat(7), new Seat(11) },
				{ null, null, null, new Seat(8), new Seat(12) } };
		Seat[][] seatMap20 = { { null, null, null, new Seat(5), new Seat(9) },
				{ new Seat(1), new Seat(3), null, new Seat(6), new Seat(10) }, { null, null, null, null, null }, // sets
																													// the
																													// aisle
				{ new Seat(2), new Seat(4), null, new Seat(7), new Seat(11) },
				{ null, null, null, new Seat(8), new Seat(12) } };
		Seat[][] seatMap21 = { { null, null, null, new Seat(5), new Seat(9) },
				{ new Seat(1), new Seat(3), null, new Seat(6), new Seat(10) }, { null, null, null, null, null }, // sets
																													// the
																													// aisle
				{ new Seat(2), new Seat(4), null, new Seat(7), new Seat(11) },
				{ null, null, null, new Seat(8), new Seat(12) } };
		Seat[][] seatMap22 = { { null, null, null, new Seat(5), new Seat(9) },
				{ new Seat(1), new Seat(3), null, new Seat(6), new Seat(10) }, { null, null, null, null, null }, // sets
																													// the
																													// aisle
				{ new Seat(2), new Seat(4), null, new Seat(7), new Seat(11) },
				{ null, null, null, new Seat(8), new Seat(12) } };
		Seat[][] seatMap23 = { { null, null, null, new Seat(5), new Seat(9) },
				{ new Seat(1), new Seat(3), null, new Seat(6), new Seat(10) }, { null, null, null, null, null }, // sets
																													// the
																													// aisle
				{ new Seat(2), new Seat(4), null, new Seat(7), new Seat(11) },
				{ null, null, null, new Seat(8), new Seat(12) } };
		Seat[][] seatMap24 = { { null, null, null, new Seat(5), new Seat(9) },
				{ new Seat(1), new Seat(3), null, new Seat(6), new Seat(10) }, { null, null, null, null, null }, // sets
																													// the
																													// aisle
				{ new Seat(2), new Seat(4), null, new Seat(7), new Seat(11) },
				{ null, null, null, new Seat(8), new Seat(12) } };
		Seat[][] seatMap25 = { { null, null, null, new Seat(5), new Seat(9) },
				{ new Seat(1), new Seat(3), null, new Seat(6), new Seat(10) }, { null, null, null, null, null }, // sets
																													// the
																													// aisle
				{ new Seat(2), new Seat(4), null, new Seat(7), new Seat(11) },
				{ null, null, null, new Seat(8), new Seat(12) } };
		Seat[][] seatMap26 = { { null, null, null, new Seat(5), new Seat(9) },
				{ new Seat(1), new Seat(3), null, new Seat(6), new Seat(10) }, { null, null, null, null, null }, // sets
																													// the
																													// aisle
				{ new Seat(2), new Seat(4), null, new Seat(7), new Seat(11) },
				{ null, null, null, new Seat(8), new Seat(12) } };
		Seat[][] seatMap27 = { { null, null, null, new Seat(5), new Seat(9) },
				{ new Seat(1), new Seat(3), null, new Seat(6), new Seat(10) }, { null, null, null, null, null }, // sets
																													// the
																													// aisle
				{ new Seat(2), new Seat(4), null, new Seat(7), new Seat(11) },
				{ null, null, null, new Seat(8), new Seat(12) } };
		Seat[][] seatMap28 = { { null, null, null, new Seat(5), new Seat(9) },
				{ new Seat(1), new Seat(3), null, new Seat(6), new Seat(10) }, { null, null, null, null, null }, // sets
																													// the
																													// aisle
				{ new Seat(2), new Seat(4), null, new Seat(7), new Seat(11) },
				{ null, null, null, new Seat(8), new Seat(12) } };

		flights.add(new Flight("12RPAMA", 12, "RP", "AM", p.get(0), seatMap));
		flights.add(new Flight("12PRAMB", 12, "PR", "AM", p.get(1), seatMap2));
		flights.add(new Flight("12RPPMB", 12, "RP", "PM", p.get(2), seatMap3));
		flights.add(new Flight("12PRPMA", 12, "PR", "PM", p.get(0), seatMap4));
		// DAY 2
		flights.add(new Flight("13RPAMA", 13, "RP", "AM", p.get(1), seatMap5));
		flights.add(new Flight("13PRAMB", 13, "PR", "AM", p.get(2), seatMap6));
		flights.add(new Flight("13RPPMB", 13, "RP", "PM", p.get(0), seatMap7));
		flights.add(new Flight("13PRPMA", 13, "PR", "PM", p.get(1), seatMap8));
		// DAY 3
		flights.add(new Flight("14RPAMA", 14, "RP", "AM", p.get(2), seatMap9));
		flights.add(new Flight("14PRAMB", 14, "PR", "AM", p.get(0), seatMap10));
		flights.add(new Flight("14RPPMB", 14, "RP", "PM", p.get(1), seatMap11));
		flights.add(new Flight("14PRPMA", 14, "PR", "PM", p.get(2), seatMap12));
		// DAY 4
		flights.add(new Flight("15RPAMA", 15, "RP", "AM", p.get(0), seatMap13));
		flights.add(new Flight("15PRAMB", 15, "PR", "AM", p.get(1), seatMap14));
		flights.add(new Flight("15RPPMB", 15, "RP", "PM", p.get(2), seatMap15));
		flights.add(new Flight("15PRPMA", 15, "PR", "PM", p.get(0), seatMap16));
		// DAY 5
		flights.add(new Flight("16RPAMA", 16, "RP", "AM", p.get(1), seatMap17));
		flights.add(new Flight("16PRAMB", 16, "PR", "AM", p.get(2), seatMap18));
		flights.add(new Flight("16RPPMB", 16, "RP", "PM", p.get(0), seatMap19));
		flights.add(new Flight("16PRPMA", 16, "PR", "PM", p.get(1), seatMap20));
		// DAY 6
		flights.add(new Flight("17RPAMA", 17, "RP", "AM", p.get(2), seatMap21));
		flights.add(new Flight("17PRAMB", 17, "PR", "AM", p.get(0), seatMap22));
		flights.add(new Flight("17RPPMB", 17, "RP", "PM", p.get(1), seatMap23));
		flights.add(new Flight("17PRPMA", 17, "PR", "PM", p.get(2), seatMap24));
		// DAY 7
		flights.add(new Flight("18RPAMA", 18, "RP", "AM", p.get(0), seatMap25));
		flights.add(new Flight("18PRAMB", 18, "PR", "AM", p.get(1), seatMap26));
		flights.add(new Flight("18RPPMB", 18, "RP", "PM", p.get(2), seatMap27));
		flights.add(new Flight("18PRPMA", 18, "PR", "PM", p.get(0), seatMap28));
	}

}
