package checkpointTwo;

import java.util.ArrayList;

public class Reservation {
	
	private int reservationID;
	public static int idNum = 100;
	private Flight flight;
	private Customer cust;
	private ArrayList<Seat> seats;
	
	
	public Reservation() {
		reservationID = idNum;
		idNum++;
	}
	
	public Reservation( Flight f, Customer c, ArrayList<Seat> s) {
		reservationID = idNum;
		idNum++;
		flight = f;
		cust = c;
		seats = s;
	}
	
	//ADD flight stuff
	public String toString() {
		String seatsList = "";
		
		for (Seat s:seats) {
			if (!seatsList.equals("")){
				seatsList = seatsList + ", ";
			}
			seatsList = seatsList + s.getSeatNum();
		}
		
		return cust.getName() + " has a reservation for flight " + flight.getFlightCode() + 
				" and their seat numbers will be " + seatsList + ". The reservation ID is " + reservationID;
	}
	
	public String deletedToString() {
		String seatsList = "";
		
		for (Seat s:seats) {
			if (!seatsList.equals("")){
				seatsList = seatsList + ", ";
			}
			seatsList = seatsList + s.getSeatNum();
		}
		
		return cust.getName() + " had a reservation for flight " + flight.getFlightCode() + 
				" and their seat numbers were " + seatsList + ". The reservation ID is " + reservationID;
	}

	public int getReservationID() {
		return reservationID;
	}

	public void setReservationID(int reservationID) {
		this.reservationID = reservationID;
	}

	public Flight getFlight() {
		return flight;
	}

	public void setFlight(Flight flight) {
		this.flight = flight;
	}

	public Customer getCust() {
		return cust;
	}

	public void setCust(Customer cust) {
		this.cust = cust;
	}

	public ArrayList<Seat> getSeats() {
		return seats;
	}

	public void setSeats(ArrayList<Seat> seats) {
		this.seats = seats;
	}

}
