package checkpointTwo;

public class Pilot extends Person {
	
	private int pilotId;
	
	public static int num = 1000;
	
	//Empty constructor. Still makes a new pilotId.
	public Pilot() {
		pilotId = num;
		num++;
	}
	
	// Constructor
	public Pilot (String n, String a, String p) {
		super(n, a, p);
		pilotId = num;
		num++;
	}
	
	// When read from a file
	public Pilot (String n, String a, String p, int id) {
		super(n, a, p);
		pilotId = id;
	}
	
	// Uses Person.toString and adds on the pilot id.
	public String toString() {
		return  super.toString() + " and their pilot id is: " + pilotId;
	}
	
	public String toStringF() {
		return super.toStringF() + "|" + pilotId;
	}

	public int getPilotId() {
		return pilotId;
	}

	public void setPilotId(int pilotId) {
		this.pilotId = pilotId;
	}

}
