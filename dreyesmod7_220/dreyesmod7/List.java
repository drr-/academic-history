package dreyesmod7;

import java.util.NoSuchElementException;


class ListNode<E> {
	E data; 
	ListNode<E> nextNode;
	
	ListNode(String string) {
		this(string, null);
	}

	ListNode(String string, ListNode<E> node) {
		data = (E) string;
		nextNode = node;
	}

	E getData() {
		return data;
	}

	ListNode<E> getNext() {
		return nextNode;
	}
} 


public class List<E> {
	private ListNode<E> firstNode;
	private ListNode<E> lastNode; 
	private String name; 

	public List() {
		this("list");
	}

	public List(String listName) {
		name = listName;
		firstNode = lastNode = null;
	} 

	public void insertAtFront(E insertItem) {
		if (isEmpty()) { 
			firstNode = lastNode = new ListNode<E>((String) insertItem);
		} 
		else { 
			firstNode = new ListNode<E>((String) insertItem, firstNode);
		} 
	} 

	public void insertAtBack(String string) {
		if (isEmpty()) { 
			firstNode = lastNode = new ListNode<E>(string);
		} 
		else { 
			lastNode = lastNode.nextNode = new ListNode<E>(string);
		} 
	} 

	public E removeFromFront() throws NoSuchElementException {
		if (isEmpty()) { 
			throw new NoSuchElementException(name + " is empty");
		}

		E removedItem = firstNode.data; 

		if (firstNode == lastNode) {
			firstNode = lastNode = null;
		}
		else {
			firstNode = firstNode.nextNode;
		}

		return removedItem; 
	} 

	public E removeFromBack() throws NoSuchElementException {
		if (isEmpty()) { 
			throw new NoSuchElementException(name + " is empty");
		}

		E removedItem = lastNode.data; 

		if (firstNode == lastNode) {
			firstNode = lastNode = null;
		}
		else { 
			ListNode<E> current = firstNode;

			while (current.nextNode != lastNode) {
				current = current.nextNode;
			}
	
			lastNode = current; 
			current.nextNode = null;
		} 

		return removedItem; 
	} 

	public boolean isEmpty() {
		return firstNode == null;
	}

	public void print() {
		if (isEmpty()) {
			System.out.printf("Empty %s%n", name);
			return;
		} 

		System.out.printf("The %s is: ", name);
		ListNode<E> current = firstNode;

		while (current != null) {
			System.out.printf("%s ", current.data);
			current = current.nextNode;
		} 

		System.out.println();
	}
	
	public String upNext(int x) {
		ListNode<E> c = firstNode;
		
		if (x == 1)
			return (String) c.nextNode.data;
		else
			return (String) c.data;
	}
} 
																									