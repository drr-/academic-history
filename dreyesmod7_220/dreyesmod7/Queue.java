package dreyesmod7;

import java.util.NoSuchElementException;
import java.util.Scanner;

public class Queue<E> {
	static Scanner scan = new Scanner(System.in);
	
	private List<E> queueList;

	public Queue() {
		queueList = new List<E>("queue");
	}

	public void enqueue(String string) {
		queueList.insertAtBack(string);
	}

	public String dequeue() throws NoSuchElementException {
		return (String) queueList.removeFromFront(); 
	}

	public boolean isEmpty() {
		return queueList.isEmpty();
	}
	
	public void print() {queueList.print();}
   
	public static String menu() {
		String which = null;
		System.out.println("\nEnter one of the following commands:");
		System.out.println("\t PRINT (to print the queues)");
		System.out.println("\t TAKEOFF flightID (which enters the flightID plane into the takeoff queue)");
		System.out.println("\t LAND flightID (which enters the flightID into the pattern for the landing queue)");
		System.out.println("\t NEXT (enables the take off or landing -- landing gets priority). Print the action that occurred (takeoff or land) and the flight symbol. ");
		System.out.println("\t END\nChoice: ");
		which = scan.nextLine();
		return which;
	}
	
	public String upNext(int x) {
		return queueList.upNext(x);
	}
	
	public static void cleanup() {
		scan.close();
		System.exit(0);
	}
}


