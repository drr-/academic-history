package dreyesmod7;

import java.awt.FileDialog;
import java.awt.Frame;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.NoSuchElementException;
import java.util.Scanner;

public class NestedDelimeters {
	
	public static Scanner data = null;
	Scanner scan = new Scanner(System.in);
	
	
	
	public static void openFile() {
		try {
			File file = new File(pickLocRead());
			//File file = new File("{$test_vm}/home/user/test_File");
			data = new Scanner(file);
			
			//data.useDelimiter(" - ");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	public static String pickLocRead() {
		Frame f = new Frame();
		// decide from where to read the file
		FileDialog fiBox = new FileDialog(f, "Pick location for reading your file", FileDialog.LOAD);
		System.out.println("The dialog box might appear behind Eclipse.  " + 
			      "\n   Choose where you would like to read for your data.");
		fiBox.setVisible(true);
		// get the absolute path to the file
		String fiName = fiBox.getFile();
		String dirPath = fiBox.getDirectory();

		// create a file instance for the absolute path
		String name = dirPath + fiName;
		return name;
	}
	
	
	public static ArrayList<String> loadData() {
		// { ( [ ] ) }
		
		openFile();
		
		ArrayList<String> sArray = new ArrayList<String>();
		
		try {
			
			while (data.hasNext()) {
				String temp = data.next();
				char[] tempCArray = temp.toCharArray();
				
				//System.out.println("'" + temp +"'");
				
				for (int i = 0; i < tempCArray.length; i++) {
					String tempMagix = "" + tempCArray[i];
					sArray.add(tempMagix);
					//System.out.println(tempCArray[i]);
				}
			}
			
		} catch (NoSuchElementException elementException) {
			System.err.println("File improperly formed. Terminating.");
		} catch (IllegalStateException stateException) {
			System.err.println("Error reading from file. Terminating.");
		}
		
		return sArray;
	}
	
	public static void validate(ArrayList<String> x) {
		int ValCurly = 0;
		int ValPara = 0;
		int ValBrac = 0;
		
		String Error = null;
		
		System.out.println("Processing the file:");
		
		for (int i = 0; i < x.size(); i++) {
			if (Error == null) {
				switch (x.get(i)) {
				case "{": 
					System.out.println("Pushed {");
					ValCurly++;
					break;
				case "(": 
					System.out.println("Pushed (");
					ValPara++;
					break;
				case "[": 
					System.out.println("Pushed [");
					ValBrac++;
					break;
				case "]": 
					ValBrac--;
					if (ValBrac < 0) {
						Error = "]";
						break;
					} else
						System.out.println("Popped [ \n");
					break;
				case ")": 
					ValPara--;
					if (ValPara < 0) {
						Error = ")";
						break;
					} else
						System.out.println("Popped ( \n");
					break;
				case "}": 
					ValCurly--;
					if (ValCurly < 0) {
						Error = "}";
					} else
						System.out.println("Popped { \n");
					break;
				//default: 
					//System.out.println(x.get(i));
				}
			}
			//System.out.println(ValCurly + " - " + ValPara + " + " + ValBrac);
		}
		
		if ((ValCurly == 0) && (ValPara == 0) && (ValBrac == 0)) {
			System.out.println("Successful");
		} else {
			int t = 0;
			
			if (Error == null) {
				for (Integer arg : Arrays.asList(ValCurly, ValPara, ValBrac)) {
					t++;
					if ((arg != 0) && (t == 1))
						Error = "}";
					if ((arg != 0) && (t == 2))
						Error = ")";
					if ((arg != 0) && (t == 3))
						Error = "]";
				}
			}
			
			System.out.println("Unmatched deliminator " + Error);
			System.out.println("^._.^");
		}
	}

	public static void main(String[] args) {
		ArrayList<String> ArrayData = loadData();
		validate(ArrayData);
	}

}
